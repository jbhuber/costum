# COSTUM YOUR COMMUNECTER
This module permits to modulate and use communecter as a specific application like organized your group, exchange ressources localy or/and map wealthy of a territory or even on a thematic.<br/>
It give you multiple choices:<br/>
	- Configuration which application you want<br/>
	- Set your own style and graphical chart<br/>
	- Have a unique interface<br/>
	- Give the message define by your projects<br/>
Only one rule :**Be open and you will openly surprised**<br/>

Enjoy and contribute to the OCDB (open and collective database)

# ROADMAP

The conception of costum based on of communecter's parameters is a basic version. Also we need contribution and improve each part of settings we can do with community to fit actors expectations or local group

	
	[] Automatic settings of your json costum
	[] Instance of costum on other server
	[] Implement activityPub
	[] Decentralized using of costum and communecter

# How does it work

### Initialize a costum
**Follow this few steps in order to prepare your environement**<br/>
1- Create on communecter an entity who will define your slug, the name of your costum, the meta data<br/>
	- It could be a project or an organization<br/>
	- Don't forget the profil image for a better render<br/>
	**[!!Slug : have to be the name of each files like home or folder you will create]**<br/>
2- Create in the module all folder and file you will call in the json<br/>
	- images folder in costum/assets/images/{slug}/<br/>
	- font folder in costum/assets/font/{slug}/ (if you need to call specific font files)<br/>
	- ico folder in costum/assets/ico/{slug}/favicon.ico<br/>
	- Your home page /costum/views/custom/home/{slug}.php<br/>
3- Construct your json costum [follow the list of params under this section]<br/>
	- Stock your json costum on folder costum/data/{slug}.json<br/>
	- Put your json in your dataBase in the collection "costum"<br/>
4- Try your local url : "localhost/costum/co/index/id/{slug}"<br/>

### Construct your json

[get json example](/data/collectifEssArrageois.json)

**List of json params**

| <span style="color:blue">**htmlConstruct**</span> | Value in the json refering to the html structure of the costum. It will filters and modify the entry @htmlConstruct in the params.json of communecter |
| ------ | ------ |
| appRendering | string 'vertical' or 'horizontal' configure your app with the app menu on the left or the top |
| header | object configuration of the costum's header   |
| header.**menuTop** | set all entry in nav menu left and nav menu right<br/>You can rename and remove button you don't want to use|
| header.menuTop.**navLeft** | - logo : can set (int)height params <br/>-searchBar : boolean to have search bar in menu<br/>-useFilter : boolean with scopeFilter & showFilter button|
| header.menuTop.**navRight** | Set 2 entry when user is connected [co] & disconnected [disco] with a list of settings :<br/>- dropdown.languages [co]<br/>- dropdown.statistics [co]<br/>- dropdown.documentations [co]<br/>- dropdown.donate [co]<br/>- dropdown.admin [co]<br/>- dropdown.settings [co]<br/>- dropdown.logout [co]<br/>- dropdown.statistics [co]<br/>- userProfil.img [co]<br/>- userProfil.name [co]<br/>- networkFloop [co]<br/>- notifications [co]<br/>- dda [co]<br/>- chat [co]<br/>- home [co]<br/>- app [co]<br/>- languages [disco]<br/>- login [disco] |
| subMenu | correspond maily to the app menu on top for horizontal disposition and on the left for vertical rendering : <br/>- app, _boolean_<br/>- button, _boolean_    |
| footer | [**Must be refactor**] corresponds currently to the button of creation of element and donation in co: <br/>- add, _boolean_<br/>- donate, _boolean_    |
| adminPanel | permits to configure menu and access for administrator of the costum site: <br/>- add : get button acces to dynform, _boolean_<br/>- statistic, specific stats for custom admin _boolean_<br/>- directory, lists of persons registration on the costum _boolean_<br/>- reference, specific functionnality for costum working on slug point in order to show data from common open database in the costum _object_ with array initType|
| directory | is a object of settings in order to render the directory render like in app #search, #event, etc <br/><br/>- viewMode, _string_ "block" or "list"<br/><br/>- header, _object_ to set button in directory header:<br/> -- header.map _boolean_ to show hide button<br/>
-- header.viewMode _boolean_ to show/hide buttons<br/>-- header.add _boolean_ to show/hide button<br/><br/>- results, _object_ to set type and defaultImg:<br/> - results.proposals.defaultImg _string_ given url of custom defautl image<br/><br/>- footer, _object_ to set button in directory footer:<br/> -- footer.add _boolean_ to show hide button<br/>|
| element | object configuration of element configuration   |
| element.**initType** | will initialized view started in element page (details or newspaper or gallery|
| element.**menuLeft** | Object of configuration for menu left of element<br>- detail _boolean_<br/>- gallery _boolean_<br>- community _boolean_<br>- agenda _boolean_<br>- projects _boolean_<br>- classifieds _boolean_<br>- cv _boolean_<br>- collection _boolean_|
| element.**menuTop** | Object of configuration for menu on top of element<br>- news _boolean_<br/>- cospace _boolean_<br>- chat _boolean_<br>- share _boolean_<br>- params _object_<br>-- params.history _boolean_<br>- params.slug _boolean_<br>- params.delete _boolean_|
| **redirect** | Set the initial view when redirection on costum is mandatory:<br>- redirect.logged _string_ [welcome, search, home, etc]<br/>- redirect.unlogged _string_ [welcome, search, home, etc] |
| **app** | Set the app object who is used app.#dda, app.#search, app.#live, app.#agenda, app.#annonces with few params:<br>- subdomainName _string_<br/>- placeholderMainSearch _string_<br/>- icon _string_ |
<br/><br/>        
| <span style="color:blue">**headerParams**</span> | object of params for elements available in communecter who will permits to influence object |
| ------ | ------ |
| proposals | set object : <br/>- color _string_<br/>- name _string_<br/>- label _string_ |
| organizations | idem |
| projects | idem |
| etc | etc |
<br/><br/>        
| <span style="color:blue">**add**</span> | define which element could be created in the costum |
| ------ | ------ |
| proposals | _boolean_ |
| organizations | idem |
| projects | idem |
| events | idem |
| etc | etc |
<br/><br/>        
|<span style="color:blue">**css**</span> | object to set lot of style on dom element of the costum  allow a panel of style update [background / border / borderBottom / box / boxShadow / fontSize / color / paddingTop / height / width / top / bottom / right / left / borderWidth / borderColor / borderRadius / lineHeight / padding / display] |
| ------ | ------ |
| font | style of font expect an object with url value _string_ font.url |
| loader | first view when refresh<br/>- loader.ring1 : first ring around logo<br/>- loader.ring2 : second ring around logo |
| progress | refers to the bar on the top when loading view<br/>- progress.value<br/>- progress.bar|
| menuTop | the top menu and can influence its children button with the entry object<br/>- menuTop.button<br/>- menuTop.scopeBtn<br/>- menuTop.filterBtn<br/>- menuTop.badge<br/>- menuTop.connectBtn|
| menuApp | the menu containing the app button (left or top) <br/>- menuApp.button<br/>- menuApp.button.hover|
| button | List of buttom [to refactor] <br/>- footer.add<br/>- footer.toolbarAdds|
| color | set the panel of color initialized in co2-color.css [purple, orange, red, dark, yellow, vine, brown, green, green-k, etc] expect string with color encode|
<br/><br/>        
|<span style="color:blue">**filters**</span> | is object of params in order to forced filters in app and directly filtering by scope or context |
| ------ | ------ |
| app | _object_ who will forced an app filtering with type [all / events / classifieds / proposals / classifieds]: <br/> filters.app.classifieds.types getting array with value only "ressources" will show the module with only ressources part|
| sourceKey | _boolean_ to get only data in the context of costum |
| scopes | _object_ with entry cities or zones to be focus on specific geographical area |
<br/>

# Features 

- build you own `personnalised interface`
- a costum can be used and shared `as a template`, all configs are added onto an element that points to a give costum template 
```
http://127.0.0.1/ph/costum/co/index/slug/cocampagne
```
- use Poi.cms elements from the parent element inside à costum as CMS editing
- template blocks can be reused 
	+ costum/views/tpls/wizard use it in a costum 
	```
<div class="col-xs-12 margin-top-20">
<?php 
$params = array(
    "poiList"=>$poiList,
    "listSteps" => array("one","two","three","four","five","six"),
    "listDone" => array("one","two")
);
echo $this->renderPartial("../tpls/wizard",$params,true); ?>
</div>
	```

# TODO

