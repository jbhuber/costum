
<style type="text/css">
	.tagBarLinks {list-style:none; width: 100%; margin: 0 auto;}
	.tagBarLinks li{ 
		margin: 10px; 
		border: 2px solid <?php echo (isset($borderColor)) ? $borderColor: "white"; ?>; border-radius: 10px; display: inline-block;}
	.tagBarLinks li i{margin:15px;}
	.tagBarLinks li img{width:53px;border-radius: 10px; }
</style>
<div class="col-xs-12 no-padding text-center" style="background-color: <?php echo (isset($bgColor)) ? $bgColor: "#1F2532"; ?>">
	<ul class="tagBarLinks ">
		<?php foreach ($list as $key => $v) { 
			$lbl = "no lbl";
			if(isset($v["icon"])){
				$color = ( isset($v["color"]) ) ? $v["color"] : "white" ;
				$lbl = '<i class="fa fa-2x '.$v["icon"].'" style="color:'.$color.'"></i>';
			}
			else if(isset($v["img"]))
				$lbl = '<img src="'.$v["img"].'"></i>';

			$link = (isset($v["link"])) ? $v["link"] : "" ;
			?>

			<li><a href="<?php echo $link; ?>"><?php echo $lbl; ?></a></li>
		<?php } ?>
	</ul>
</div>