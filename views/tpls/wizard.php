<div id="wizard" class="swMain">


    <style type="text/css">
        .swMain ul li > a.done .stepNumber {
            border-color: <?php echo ( @Yii::app()->session["costum"]["colors"]["pink"] ) ? Yii::app()->session["costum"]["colors"]["pink"] : "#354C57"; ?>;
            background-color: <?php echo ( @Yii::app()->session["costum"]["colors"]["pink"] ) ? Yii::app()->session["costum"]["colors"]["pink"] : "#354C57"; ?>; 
        }

        swMain > ul li > a.selected .stepDesc, .swMain li > a.done .stepDesc {
         color: <?php echo ( @Yii::app()->session["costum"]["colors"]["pink"] ) ? Yii::app()->session["costum"]["colors"]["pink"] : "#ffffff"; ?>;  
         font-weight: bolder; 
        }

        .swMain > ul li > a.selected::before, .swMain li > a.done::before{
          border-color: <?php echo ( @Yii::app()->session["costum"]["colors"]["pink"] ) ? Yii::app()->session["costum"]["colors"]["pink"] : "#354C57"; ?>;      
        }
    </style>


    <ul id="wizardLinks">
        
        <?php
        var_dump($poiList); 
        foreach ($listSteps as $k => $v) {
            $n = "todo";
            $p = null;

            if( count(Poi::getPoiByTag($poiList, "step".$k ) ) != 0 ) { 
                $p = Poi::getPoiByTag($poiList, "step".$k )[0];
                $n =  $p["name"];
            }
            echo "<li>";
                $d = ( isset($p) && !in_array("inactif", $p["tags"]) ) ? 'class="done"' : '';
                echo '<a onclick="showStep(\'#'.$v.'\')" href="javascript:;" '.$d.' >';
                echo '<div class="stepNumber">'.$k.'</div>';
                echo '<span class="stepDesc">'.$n.'</span></a>';
            echo "</li>";    
        }
        ?>

    </ul>
    

    <?php  
    foreach ($listSteps as $k => $v) {
    ?>
    <div id='<?php echo $v ?>' class='col-sm-offset-1 col-sm-10 sectionStep hide'>
        <?php 
        if( count(Poi::getPoiByTag($poiList,"step".$k))!=0 ){
            $p = Poi::getPoiByTag($poiList,"step".$k)[0];
            echo '<h1 class="text-red">'.@$p["name"].'</h1>';
            echo "<div class='markdown'>".@$p["description"]."</div>";
        } 
        else { ?>
        TEXT TODO <br/>
        as POI type cms + tag : step<?php echo $k ?>
        <script type="text/javascript">
           var dynFormCostum = {
                
                "onload" : {
                    "actions" : {
                        "setTitle" : "Gestion de Contenu libre",
                        "html" : {
                            "infocustom" : "<br/>Gérez votre contenu vous meme"
                        },
                        "presetValue" : {
                            "type" : "cms"
                        },
                        "hide" : {
                            "locationlocation" : 1,
                            "breadcrumbcustom" : 1,
                            "urlsarray" : 1,
                        }
                    }
                }
            }

        </script>
        <?php if(Authorisation::canEdit(Yii::app()->session["userId"] , Yii::app()->session["costum"]["contextId"], Element::getControlerByCollection(Yii::app()->session["costum"]["contextType"]) ) ){ ?>
        <br><a href="javascript:;" onclick="dyFObj.openForm('poi',null,{tags:'step1',type:'cms'},null,dynFormCostum)"  class="btn btn-xs btn-primary">create</a>
        <?php } }?>
    </div>
    <?php } ?>


    <script type="text/javascript">
        function showStep(id){
            $(".sectionStep").addClass("hide");
            $(id).removeClass("hide");
        }
    </script>


</div>