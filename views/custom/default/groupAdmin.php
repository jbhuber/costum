<?php
$cssAnsScriptFilesModule = array(
    '/plugins/jquery-simplePagination/jquery.simplePagination.js',
	'/plugins/jquery-simplePagination/simplePagination.css'
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getRequest()->getBaseUrl(true));

$layoutPath = 'webroot.themes.'.Yii::app()->theme->name.'.views.layouts.';

$memberId = Yii::app()->session["userId"];
$memberType = Person::COLLECTION;
$tags = array();
$scopes = array(
	"codeInsee"=>array(),
	"codePostal"=>array(),
	"region"=>array(),
);
?>
<style type="text/css">
.simple-pagination li a, .simple-pagination li span {
    border: none;
    box-shadow: none !important;
    background: none !important;
    color: #2C3E50 !important;
    font-size: 16px !important;
    font-weight: 500;
}
.simple-pagination li.active span{
	color: #d9534f !important;
    font-size: 24px !important;	
}
</style>
<div class="panel panel-white col-lg-offset-1 col-lg-10 col-xs-12 no-padding">
	<div class="col-md-12 col-sm-12 col-xs-12 text-center">
		<div id="" class="" style="width:80%;  display: -webkit-inline-box;">
	    	
	    	<!-- <button class="btn btn-default hidden-xs menu-btn-start-search-admin btn-directory-type">
	    		<i class="fa fa-search"></i>
	    	</button> -->
	    </div>
    </div>
	<div class="panel-heading border-light padding-10">
		<h4 class="panel-title padding-10">Filtered by types : </h4>
		<div class="panel-heading border-light padding-10">
			<a href="javascript:;" onclick="showType('line')" class="btn btn-xs btn-default">Tous</a>
				
			<?php foreach ($typeDirectory as $value) { ?>
				<a href="javascript:;" onclick="showType('<?php echo $value ?>')" class="filter<?php echo $value ?> btn btn-xs btn-default"> <?php echo $value ?> <span class="badge badge-warning countPeople" id="count<?php echo $value ?>"> <?php echo @$results["count"][$value] ?></span></a>
			<?php } ?>
		</div>
		<div class="panel-heading border-light padding-10">
			<input type="text" class="form-control col-xs-12" id="search" placeholder="search by name">
		</div>
	</div>
	<div class="pageTable col-md-12 col-sm-12 col-xs-12 padding-20 text-center"></div>
		<div class="panel-body">
		<div>	
			<table class="table table-striped table-bordered table-hover  directoryTable" id="panelAdmin">
				<thead>
					<tr id="headerTable">
						<th>Type</th>
						<th>Name</th>
						<th>Email</th>
						
					</tr>
				</thead>
				<tbody class="directoryLines">
					
				</tbody>
			</table>
		</div>
	</div>
	<div class="pageTable col-md-12 col-sm-12 col-xs-12 padding-20"></div>
</div>
<script type="text/javascript">
var openingFilter = "<?php echo ( isset($_GET['type']) ) ? $_GET['type'] : '' ?>";
var directoryTable = null;
var contextMap = {
	"tags" : <?php echo json_encode($tags) ?>,
	"scopes" : <?php echo json_encode($scopes) ?>,
};
var results = <?php echo json_encode($results) ?>;
var initType = <?php echo json_encode($typeDirectory) ?>;
var betaTest= "<?php echo @Yii::app()->params['betaTest'] ?>";
var isCostumAdmin= <?php echo json_encode(@Yii::app()->session['isCostumAdmin']) ?>;
var isSuperAdmin= <?php echo json_encode(@Yii::app()->session['userIsAdmin']) ?>;
var icons = {
	organizations : "fa-group",
};
var searchAdmin={
	text:null,
	page:"",
	type:initType[0]
};



var adminObj = {
	paramsCostum : costum.htmlConstruct.adminPanel.groupe,
	getElt : function(id, type){
		mylog.log("getElt", id, type,results);
		return results[type][id] ;
	},
	setElt : function(elt, id, type){
		mylog.log("getElt", id, type,results);
		results[type][id] = elt;
	},
	values : {
		validated : function(e, id, type){
			//mylog.log("values validated", e, id, type);
			var isValidated=( typeof e.source != "undefined" && typeof e.source.toBeValidated != "undefined" && typeof e.source.toBeValidated[costum.slug] != "undefined" && e.source.toBeValidated[costum.slug] == true) ? false : true;
			var str = "";
			if(!isValidated)
				str = "<span class='badge bg-orange'><i class='fa fa-check'></i> En attente de validation</span>";
			else
				str = "<span class='badge bg-green-k'><i class='fa fa-check'></i> Groupe validé</span>";
			return str;
		}
	},
	actions : {

		init : function(e, id, type){
			//mylog.log("adminObj init", e, id, type);
			var str = "";
			if( Object.keys(adminObj.paramsCostum.actions).length == 1 ) {
				 str = adminObj.actions.get(e, id, type, false);
			}else{
				 str = '<a href="#" data-toggle="dropdown" class="btn btn-danger dropdown-toggle btn-sm">'+
							'<i class="fa fa-cog"></i> <span class="caret"></span>'+
						'</a>'+
						'<ul class="dropdown-menu pull-right dropdown-dark" role="menu" style="background: #d9534f;">'+
							adminObj.actions.get(e, id, type, true)+
						'</ul>';
			}
			//mylog.log("adminObj get init ", str);
			return str ;
		},
		get : function(e, id, type, multiple){
			//mylog.log("adminObj get", e, id, type, multiple);
			var str = "";
			$.each(adminObj.paramsCostum.table, function(key, value){

				if(multiple === true)
					str += "<li>";
				mylog.log("adminObj get", key, value);
				if(key == "validated" && value === true){
					str += adminObj.actions.validated(e, id, type);	
				} else if(key == "remove" && value === true){
					str += adminObj.actions.validated(e, id, type);	
				}

				if(multiple === true)
					str += "</li>";
			});
			//mylog.log("adminObj get str ", str);
			return str ;
		},
		validated : function(e, id, type){
			//mylog.log("adminObj validated", e, id, type);
			var str = "" ;
			var isValidated=( typeof e.source != "undefined" && typeof e.source.toBeValidated != "undefined" && typeof e.source.toBeValidated[costum.slug] != "undefined" && e.source.toBeValidated[costum.slug] == true) ? false : true;
			if(!isValidated){
				str ='<button data-id="'+id+'" data-type="'+type+'" data-valid="true" class="margin-right-5 validateSourceBtn btn bg-green-k text-white"><i class="fa fa-check"></i> Valider le groupe</button>';
			} else {
				str ='<button data-id="'+id+'" data-type="'+type+'" data-valid="false" class="margin-right-5 validateSourceBtn btn bg-red-k text-white"><i class="fa fa-trash"></i> Enlever la validation du groupe</button>';
			}
			return str ;
		}
	},
	bindAdminBtnEvents : function(){
		//mylog.log("validateSourceBtn");
		$(".validateSourceBtn").off().on("click", function(){
			$(this).find("i").removeClass("fa-check").addClass("fa-spinner fa-spin");
			var thisObj=$(this);
			var params={
				id:$(this).data("id"),
				type:$(this).data("type"),
				valid:$(this).data("valid")
			};
			mylog.log("adminObj params", params);

			$.ajax({
				type: "POST",
				url: baseUrl+"/costum/default/validategroup",
				data : params
			})
			.done(function (data){
				if ( data && data.result ) {
					//mylog.log("validateSourceBtn data", data, params.id, params.type);

					var elt = adminObj.getElt(params.id, params.type) ;
					if(typeof data.elt.source != "undefined"){
						elt.source = data.elt.source ;
					}
					adminObj.setElt(elt, params.id, params.type) ;

					thisObj.replaceWith( adminObj.actions.validated( elt, params.id, params.type) );
					$("#"+params.type+params.id+" .validated").html( adminObj.values.validated( elt, params.id, params.type));
					adminObj.bindAdminBtnEvents();
					if(params.valid === true)
						toastr.success("L'élément est validée");
					else 
						toastr.success("L'élément n'est plus validée");
					
				} else {
					toastr.error("Un problème est survenu lors de la validation");
				}
			});
		});
	},
	initTable : function(e){
		//mylog.log("adminObj initTable ", e);
		var str = "" ;
		if(	typeof adminObj.paramsCostum.table != "undefined" && 
			adminObj.paramsCostum.table != null ){
			$.each(adminObj.paramsCostum.table, function(key, value){
				if(key == "pdf" && value === true){
					str +="<th>Pdf</th>";
				} else if(key == "validated" && value === true){
					str +="<th>Status</th>";	
				}
			});
			
		}

		if(	typeof adminObj.paramsCostum.table != "undefined" && 
			adminObj.paramsCostum.actions != null ) {
			str +="<th>Actions</th>";
		}
		$("#headerTable").append(str) ;
	},
	columTable : function(e, id, type){
		mylog.log("adminObj columTable ", e);
		var str = "" ;
		if(	typeof adminObj.paramsCostum.table != "undefined" && 
			adminObj.paramsCostum.table != null ){
			$.each(adminObj.paramsCostum.table, function(key, value){
				str += '<td class="center '+key+'">';
				if(key == "pdf" && value === true){
					str+='<a href="'+baseUrl+'/co2/export/pdfelement/id/'+id+'/type/'+type+'/" data-id="'+id+'" data-type="'+type+'" class="margin-right-5" target="_blank"> <i class="fa fa-2x fa-file-pdf-o text-red" ></i> </a> ';
				
				} else if(key == "validated" && value === true) {
					str += adminObj.values.validated(e, id, type);
				}
				str += '</td>';
			});
		}
		return str ;
	}
};

jQuery(document).ready(function() {
	setTitle("Espace administrateur : Répertoire","cog");
	initKInterface();
	initViewTable(results);
	$("#input-search-table").keyup(function(e){
		if(e.keyCode == 13){
			searchAdmin.page=0;
			searchAdmin.text = $(this).val();
			if(searchAdmin.text=="")
				searchAdmin.text=true;
			startAdminSearch();
			// Init of search for count
			if(searchAdmin.text===true)
				searchAdmin.text=null;
		}
    });
	var counttotal = 0 ;
    $.each(results.count, function(key, values){
		counttotal += values;
	});
    //initPageTable(counttotal);
    adminObj.initTable();

    $("#search").on("keyup", function() {
	    var value = $(this).val().toLowerCase();
	    $("#panelAdmin tr.line").filter( function() {
	    	$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
	    	//countLine();
	    });
	});

});

function initPageTable(number){
	numberPage=(number/100);
	//alert("numberPage "+numberPage);
	$('.pageTable').pagination({
		items: numberPage,
		itemOnPage: 5,
		currentPage: 1,
		hrefTextPrefix:"?page=",
		cssStyle: 'light-theme',
		onInit: function () {
		},
		onPageClick: function (page, evt) {
			searchAdmin.page=(page-1);
			startAdminSearch();
		}
    });
}
function initViewTable(data){
	$('#panelAdmin .directoryLines').html("");
	console.log("valuesInit",data);
	$.each(data,function(type,list){
		$.each(list, function(key, values){
			entry=buildDirectoryLine( values, type, type, icons[type]);
			$("#panelAdmin .directoryLines").append(entry);
		});
	});
	adminObj.bindAdminBtnEvents();
	}
function refreshCountBadgeAdmin(count){
	$.each(count, function(e,v){
		$("#count"+e).text(v);
	});
}
function startAdminSearch(initPage){

    $('#panelAdmin .directoryLines').html("Recherche en cours. Merci de patienter quelques instants...");

    $.ajax({ 
        type: "POST",
        url: baseUrl+"/costum/pacte/groupadmin/tpl/json",
        data: searchAdmin,
        dataType: "json",
        success:function(data) { 
	          initViewTable(data.results);
	          adminObj.bindAdminBtnEvents();
	          if(initPage)
	          	initPageTable(data.results.count[searchAdmin.type]);
        },
        error:function(xhr, status, error){
            $("#searchResults").html("erreur");
        },
        statusCode:{
                404: function(){
                    $("#searchResults").html("not found");
            }
        }
    });
}

function buildDirectoryLine( e, collection, type, icon/* tags, scopes*/ ){
		strHTML="";
		if(typeof e._id =="undefined" || ((typeof e.name == "undefined" || e.name == "") && (e.text == "undefined" || e.text == "")) )
			return strHTML;
		actions = "";
		classes = "";
		id = e._id.$id;
		var status=[];
		/* **************************************
		* ADMIN STUFF
		***************************************** */
		if(userId != "" 
			|| isCostumAdmin || isSuperAdmin){
			
		}
		
		/* **************************************
		* TYPE + ICON
		***************************************** */
	strHTML += '<tr id="'+type+id+'" class="'+type+' line">'+
		'<td class="'+collection+'Line '+classes+'">'+
			'<a href="#page.type.'+type+'.id.'+id+'" class="lbh" target="_blank">';
				if (e && typeof e.profilThumbImageUrl != "undefined" && e.profilThumbImageUrl!="")
					strHTML += '<img width="50" height="50" alt="image" class="img-circle" src="'+baseUrl+e.profilThumbImageUrl+'">'+e.type;
				else 
					strHTML += '<i class="fa '+icon+' fa-2x"></i> '+type;
			strHTML += '</a>';
		strHTML += '</td>';
		
		/* **************************************
		* NAME
		***************************************** */
		if(typeof e.name != "undefined")
			title=e.name;
		else if(typeof e.text != "undefined")
			title=e.text;
		strHTML += '<td><a href="#page.type.'+type+'.id.'+id+'" class="lbh" target="_blank">'+title+'</a></td>';
		
		/* **************************************
		* EMAIL for admin use only
		***************************************** */
		strHTML += '<td>'+e.email+'</td>';

		
		strHTML += adminObj.columTable(e, id, type);

		/* **************************************
		* ACTIONS
		***************************************** */
		strHTML += '<td class="center">'+ 
						'<div class="btn-group">'+
						adminObj.actions.init(e, id, type)+
						'</div>'+
					'</td>';
	
	strHTML += '</tr>';
	return strHTML;
}

function showType (type) {
	mylog.log("showType", type);
	$(".line").hide();
	$("."+type).show();
	//countLine();
}
</script>