<?php $cssJS = array(
    '/plugins/jQuery-Knob/js/jquery.knob.js',
    '/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js',
    // SHOWDOWN
    '/plugins/showdown/showdown.min.js',
    // MARKDOWN
    '/plugins/to-markdown/to-markdown.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->request->baseUrl);


if(Yii::app()->session["costum"]["contextType"] && Yii::app()->session["costum"]["contextId"]){
    $el = Element::getByTypeAndId(Yii::app()->session["costum"]["contextType"], Yii::app()->session["costum"]["contextId"] );

    $poiList = PHDB::find(Poi::COLLECTION, 
                    array( "parent.".Yii::app()->session["costum"]["contextId"] => array('$exists'=>1), 
                           "parent.".Yii::app()->session["costum"]["contextId"].".type"=>Yii::app()->session["costum"]["contextType"],
                           "type"=>"cms") );

}

?>


<div class="pageContent">


<style type="text/css">
    @font-face{
        font-family: "montserrat";
         src: url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/co/montserrat-regular.woff") format("woff"),
         url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/co/montserrat-regular.ttf") format("ttf")
    }.mst{font-family: 'montserrat'!important;}

    @font-face{
        font-family: "CoveredByYourGrace";
        src: url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/co/CoveredByYourGrace.ttf")
    }.cbyg{font-family: 'CoveredByYourGrace'!important;}
    
    
  #customHeader{
    margin-top: 0px;
  }
  #costumBanner{
   /* max-height: 375px; */
  }
  #costumBanner h1{
    position: absolute;
    color: white;
    background-color: rgba(0,0,0,0.4);
    font-size: 29px;
    bottom: 0px;
    padding: 20px;
  }
  #costumBanner h1 span{
    color: #eeeeee;
    font-style: italic;
  }
  #costumBanner img{
    min-width: 100%;
  }
  .btn-main-menu{
    background: <?php echo Yii::app()->session["costum"]["colors"]["pink"]; ?>;
    border-radius: 10px;
    padding: 10px !important;
    color: white;
    cursor: pointer;
    border:3px solid transparent;
    font-size: 1.5em
    /*min-height:100px;*/
  }
  .btn-main-menuW{
    background: white;
    color: <?php echo Yii::app()->session["costum"]["colors"]["pink"]; ?>;
    border:none;
    cursor:text ;
  }
  .btn-main-menu:hover{
    border:2px solid <?php echo Yii::app()->session["costum"]["colors"]["pink"]; ?>;
    background-color: white;
    color: <?php echo Yii::app()->session["costum"]["colors"]["pink"]; ?>;
  }
  .btn-main-menuW:hover{
    border:none;
  }
  @media screen and (min-width: 450px) and (max-width: 1024px) {
    .logoDescription{
      width: 60%;
      margin:auto;
    }
  }

  @media (max-width: 1024px){
    #customHeader{
      margin-top: -1px;
    }
  }
  @media (max-width: 768px){
    h1, h2, h3, h4, h5, h6 {
        display: block;
        font-size: 1.9em;
    }
  }

</style>

<?php 
if(!@$el["costum"] && false){
    echo $this->renderPartial("costum.views.tpls.acceptAndAdmin",
                        array("el"  => $el,
                              "tpl" => "cocampagne" ),true ); 
}
?>  

<div class="col-xs-12 no-padding" id="customHeader" style="background-color: white">
  <div id="costumBanner" class="col-xs-12 col-sm-12 col-md-12 no-padding">
 <!--  <h1>L'entraide<br/><span class="small">Une interface numérique pour échanger</span></h1>-->
    
    <?php 
    $banner = Yii::app()->getModule("co2")->assetsUrl."/images/banniere-Campagne-Acoeur.jpg";
    if(@Yii::app()->session["costum"]["metaImg"]){
    	//ex this.profilBannerUrl
		if(substr_count(Yii::app()->session["costum"]["metaImg"], 'this.') > 0 && isset($el)){
			$field = explode(".", Yii::app()->session["costum"]["metaImg"]);
			if( isset( $el[ $field[1] ] ) )
			  	$banner = Yii::app()->getRequest()->getBaseUrl(true).$el[$field[1]] ;
		}
		else if(strrpos(Yii::app()->session["costum"]["metaImg"], "http" ) === false && strrpos(Yii::app()->session["costum"]["metaImg"], "/upload/" ) === false ) {
			$banner = Yii::app()->getModule("co2")->getAssetsUrl().Yii::app()->session["costum"]["metaImg"] ;
        }
		else 
			$banner = Yii::app()->session["costum"]["metaImg"];
    }
    ?>
    <img class="img-responsive"  style="margin:auto;background-color: black;" src='<?php echo $banner ?>'/> 

  </div>
  <!--<div class="col-md-12 col-lg-12 col-sm-12 imageSection no-padding" 
     style=" position:relative;">-->
  <div class="col-sm-12 col-md-12 col-xs-12 no-padding" style="background-color:<?php echo Yii::app()->session["costum"]["colors"]["grey"]; ?>; max-width:100%; float:left;">

    <div class="col-xs-12 no-padding" style="margin-top:100px;"> 


      <div class="col-xs-12 no-padding">
        <div class="col-md-12 col-sm-12 col-xs-12 padding-20" style="padding-left:100px;background-color: <?php echo Yii::app()->session["costum"]["colors"]["grey"]; ?>; ">
          <div class="col-xs-12 col-sm-10 col-sm-offset-1 pull-left padding-20 shadow2" style="font-family: montserrat; margin-top:-200px;background-color: #fff;font-size: 14px;z-index: 5;">
            <div class="col-xs-12  ourvalues" style="text-align:center;">
	            <h2 class="mst col-xs-12 text-center">
                <br>CRÉONS L'UTOPIE
                </h2>

                <p class="mst" style="color:<?php echo Yii::app()->session["costum"]["colors"]["dark"]; ?>">
                  En co-construisant un réseau social libre et local :<br>
                  humain, commun et libre.<br/>
                 <br/>
                </p>

                <h2 class="mst" style="color:<?php echo Yii::app()->session["costum"]["colors"]["pink"]; ?>" >JE CRÉE L'UTOPIE</h2>
	                
	            
	            <br/>
              	<div class="col-xs-12" style="margin-bottom:40px;">
	              	<div class="col-xs-12 col-sm-4">
                        <?php if( Person::logguedAndValid() ){ ?>
	                	<span class="col-xs-12 text-center btn-main-menu btn-main-menuW ">Je suis Connecté !! <br>et je veux l'utopie</span>
                        <?php } else { ?>
                        <button  data-toggle="modal" data-target="#modalRegister" class="col-xs-12 btn-main-menu text-center" styl >En m'inscrivant</button>
                        <?php } ?>
		            </div>

		            <div class="col-xs-12 col-sm-4">
	                	<a href="#porterPacte" class="col-xs-12 btn-main-menu btn-main-menuW text-red" style="font-size: 1.5em; padding-top: 20px;"  >En partageant <br>la campagne</a>
		            </div>

		            <div class="col-xs-12 col-sm-4">
	                	<a href="https://www.helloasso.com/associations/open-atlas/collectes/communecter/don" target="_blank" class="col-xs-12 btn-main-menu">En donnant</a>
		            </div>
             	</div>

            </div>
          </div>

        </div><br/>

      </div>

		
        <style type="text/css">
        	.monTitle{
        		border-top: 1px dashed <?php echo Yii::app()->session["costum"]["colors"]["pink"]; ?>; 
        		border-bottom: 1px dashed <?php echo Yii::app()->session["costum"]["colors"]["pink"]; ?>;
                margin-top: -20px;
        	}
        </style>
    	<div class="col-xs-12 no-padding "><br/>
    		<div class="col-md-12 col-sm-12 col-xs-12 " style="background-color: white; ">
    		  <h1 class="margin-top-20 monTitle  text-red padding-20 text-center cbyg" >à Chacun son pourquoi</h1>
    		</div>

            <div class="row margin-top-20  padding-20" style="background-color: white; ">

                <div class="col-xs-12">
                    <div class="col-xs-8 padding-20">
                        <style>
                        .embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto; } 
                        .embed-container iframe, 
                        .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                        </style>
                        <div id="video_container" class='embed-container' style="cursor:pointer;background-color: #666;max-height: 300px">
                            <a id="videoImg">
                                <img class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->assetsUrl."/images/co/videoImg.png" ?>">
                            </a>
                        </div>
                        
                    </div>
                    <div class="col-xs-4 padding-20 margin-top-20 Montserrat" style="font-size:1.2em ">
                        Parce que Communecter est bâti par une communauté d’individus qui ont tous leur motivation propre. <br/><br/>
                        Parce que tous ont leurs raisons de vouloir le meilleur pour les communs, le libre et les territoires. <br/><br/>
                        Oui mais ça veut dire quoi tout ça ? On vous répond : 1 question nouvelle par semaine, 1 point de vue à la fois. 

                    </div>  
                </div>

                <div class="col-xs-12 margin-top-20">
                    <?php 
                    $params = array(
                        "poiList"=>$poiList,
                        "listSteps" => array("one","two","three","four","five","six"),
                        "el" => $el
                    );
                    echo $this->renderPartial("costum.views.tpls.wizard",$params,true); ?>
                </div>
                  
            </div>

            
            <div id="newsstream">
                
                <div class="col-md-12 col-sm-12 col-xs-12 padding-20" style="background-color: white; ">
                    <h1 class="monTitle  text-red padding-20 text-center" >fil d'actu</h1>
                </div>

            </div>

        </div>
    </div>

    <style type="text/css">
        .borderInv{
            border-bottom: 1px dashed white;
        }
        .numVal{font-size: 30px}
                
    </style>

    <div class="col-xs-12 no-padding">
        <div class="col-md-12 col-sm-12 col-xs-12 padding-20" style="background-color: <?php echo Yii::app()->session["costum"]["colors"]["pink"]; ?>; ">
            <h1 class="borderInv  text-white padding-20 text-center cbyg" >Amis du COEUR, ils voient le monde autrement !<br/><br/>
            </h1>
            
            <div class="row">

                <div class="col-xs-12 borderInv ">
                    
                    <div class="col-sm-offset-1 col-sm-2 col-xs-12 Montserrat text-center text-white" style="padding:40px" >
                        <span class="numVal"><?php echo PHDB::count(Person::COLLECTION); ?></span><br>
                        <span class="text-small">CITOYENS</span>
                    </div>  

                    <div class="col-sm-2 col-xs-12 Montserrat text-center text-white " style="padding:40px">
                        <span class="numVal"><?php echo PHDB::count(Organization::COLLECTION,array("type"=>Organization::TYPE_NGO)); ?></span><br>
                        <span>ASSOCIATIONS</span>
                    </div>

                    <div class="col-sm-2 col-xs-12 Montserrat text-center text-white" style="padding:40px">
                        <span class="numVal"><?php echo PHDB::count(Organization::COLLECTION,array("type"=>Organization::TYPE_BUSINESS)); ?></span><br>
                        <span>ENTREPRISES</span>
                    </div>

                    <div class="col-sm-2 col-xs-12 Montserrat text-center text-white" style="padding:40px">
                        <span class="numVal"><?php echo PHDB::count(Organization::COLLECTION,array("type"=>Organization::TYPE_GROUP)); ?></span><br>
                        <span>GROUPES</span>
                    </div>

                    <div class="col-sm-2 col-xs-12 Montserrat text-center text-white" style="padding:40px">
                        <span class="numVal"><?php echo PHDB::count(Organization::COLLECTION,array("type"=>Organization::TYPE_GOV)); ?></span><br>
                        <span>SERVICES PUBLICS</span>
                    </div>  

                </div>
                <div class="col-xs-12 borderInv padding-20">
                    
                    <div class=" margin-top-20 col-xs-4 Montserrat text-center text-white" style="padding:40px">
                        <span class="numVal">157</span><br/>
                        <span>DONATEURS<br></span>
                    </div>  

                    <div class="col-xs-4 Montserrat margin-top-20 text-center text-white" style="padding:40px">
                        <span class="numVal">89 €</span><br/>
                        <span>DONS / MOIS<br></span>
                    </div>
                    
                    <div class="col-xs-4 Montserrat margin-top-20 text-center text-white" style="padding:40px">
                        <span class="numVal">709 €</span><br/>
                        <span>DONS CUMULÉS<br></span>
                    </div>

                    <div class="col-xs-12 Montserrat text-center text-white" >
                        <br/><a href="https://www.helloasso.com/associations/open-atlas/collectes/communecter/don" target="_blank" class="btn-main-menu btn-main-menuW">Je co-construit en donnant</a><br/><br/>
                        <p style="font-size:1.8em;padding:40px">
                        En donnant je participe au financement de fonctionnalités, <br/>
                        et je vote pour celles qui seront développées.<br/>
                        </p>
                    </div>


                </div>
            </div>
        </div>

  </div>




  <style type="text/css">
            .monTitle{
                border-top: 1px dashed <?php echo Yii::app()->session["costum"]["colors"]["pink"]; ?>; 
                border-bottom: 1px dashed <?php echo Yii::app()->session["costum"]["colors"]["pink"]; ?>;
                margin-top: -20px;
            }
        </style>
        <div class="col-xs-12 padding-0 bg-white">
            <div class="col-md-12 col-sm-12 col-xs-12 padding-top-20 margin-top-20" style="background-color: white; ">
              <h1 class="monTitle  text-red padding-20 text-center cbyg uppercase" >à Quoi Servent ces dons</h1>
            </div>

            <div class="row margin-top-20  padding-0">

                <div class="col-xs-12">
                    
                    <div class="col-xs-12 col-sm-10 col-sm-offset-1 padding-20 margin-top-20 Montserrat text-center" style="font-size:1.2em;background-color: white; ">

                        Nous souhaitons créer un modèle économique de co-investissement des utilisateurs.<br/> 
                        En contrepartie de vos dons vous avez droit de proposer et voter pour les nouvelles fonctionnalités que nous développerons.<br/><br/>
                        
                        <h4>L’équipe qui travaille pour vous :</h4>
                        4 prestataires (développeurs et animation),<br/>
                        5 salariés (graphistes, community manager, administration)<br/> 
                        et plus d’une vingtaine de bénévoles<br/>
                        <br/>
                        <h4>Coûts de fonctionnement, quelques exemples :</h4>
                        Hébergement de Communecter : 2000€/mois<br/>
                        Une nouvelle fonctionnalité développée : 2600€<br/>
                        Etude ergonomique : 5 000€<br/><br/><br/>

                        <div class="col-xs-12 text-center">
                        <a href="https://www.helloasso.com/associations/open-atlas/collectes/communecter/don" target="_blank" class="btn-main-menu btn btn-danger"  >En Savoir Plus</a>
                        <br/><br/>
                    </div>  
                </div>
                
                <div class="col-xs-12 padding-0 text-center">
                    <a href="#dda" class="lbh"><img class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->assetsUrl."/images/co/BandeauVote.jpg" ?>"></a>
                </div>

                <div class="col-xs-12 padding-0 text-center">
                    <img class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->assetsUrl."/images/co/BanniereMerci.jpg" ?>">
                </div>

               
                  
            </div>

        </div>


    </div>


     
    </div>
  </div>
</div>

<script type="text/javascript">
  jQuery(document).ready(function() {
    urlNews = "/news/co/index/type/"+costum.contextType+"/id/"+costum.contextId+"/formCreate/false/nbCol/2/scroll/false";

    ajaxPost("#newsstream",baseUrl+"/"+urlNews,{search:true, formCreate:false, scroll:false}, function(news){}, "html"); 

    $.each($(".markdown"), function(k,v){
        descHtml = dataHelper.markdownToHtml($(v).html()); 
        $(v).html(descHtml);
    });

    $('#videoImg').click(function(e) {
        e.preventDefault();
      $('#video_container').html('<iframe  id="player1" src="https://player.vimeo.com/video/293105010?autoplay=1"  frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>');
      
    });

// var d = new Date();
// var timecount2 = d.getTime();
// alert(timecount2-timecount);
    contextData = {
        id : "<?php echo Yii::app()->session["costum"]["contextId"] ?>",
        type : "<?php echo Element::getControlerByCollection(Yii::app()->session["costum"]["contextType"]) ?>"
    };
    
});

</script>

