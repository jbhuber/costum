<?php 

$hashTagsExcluded=array("chiffre","citation");

$carousel_indicators_color="#C9462C";

$cssAnsScriptFilesTheme = array(
        // SHOWDOWN
        '/plugins/showdown/showdown.min.js',
        //MARKDOWN
        '/plugins/to-markdown/to-markdown.js',              
    );
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);

$chiffre=Poi::getPoiByWhereSortAndLimit(array("tags"=>array("chiffre"), "source.key"=>"journalInsoumisChambery"),array("updated"=>-1), 1, 0);

$citation=Poi::getPoiByWhereSortAndLimit(array("tags"=>array("citation"), "source.key"=>"journalInsoumisChambery"),array("updated"=>-1), 1, 0);
    
$evenements=Event::getEventByWhereSortAndLimit(array( "source.key"=>"journalInsoumisChambery"),array("startDate" => -1),3,0);

//$articles_une=Poi::getPoiByWhereSortAndLimit(array("rank"=>"true"),array("updated"=>-1), 3, 0);
$articles_une=Poi::getPoiByWhereSortAndLimit(array("rank"=>"true","notags"=>$hashTagsExcluded, "source.key"=>"journalInsoumisChambery"),array("updated"=>-1), 3, 0);
//$articles=Poi::getPoiByWhereSortAndLimit(array('$or'=>array(array("rank"=>array('$exists'=>false)),array("rank"=>"false"))),array("updated"=>-1), 5, 0); 

$articles=Poi::getPoiByWhereSortAndLimit(array('$and'=>array(array("notags"=>$hashTagsExcluded, "source.key"=>"journalInsoumisChambery"))),array("updated"=>-1), 10, 0); 


$nb_articles=0;
foreach ($articles as $key => $value) {
  if($nb_articles>6){
    unset($articles[$key]);
  } else {
    foreach ($articles_une as $key_une => $value_une) {
      if(strcmp($key, $key_une)===0){
        unset($articles[$key]);
        break;
      }
    }
    if(isset($articles[$key])){
      $nb_articles=$nb_articles+1;
    }
  }
}

?>

<div id="a2k_page" class="pageContent w-100 mx-auto">

  <div id="a2k_main_conteneur" class="w-100">
    <div class="w-100 mw1000 mx-auto">
      <div id="a2k_left-column" class="col-xs-12 col-sm-6 col-md-8 right-separator pl-0 pr-0">
        <div class="bloc_rubrique w-100 p-30">
          <div class="titre_rubrique w-100 b maj">
             A la une
          </div>         
          <div id="une_container" class="contenu_rubrique w-100">  
            <?php
            $this->renderPartial('co2.views.pod.sliderGeneric',array("idCarousel"=>"articleCarousel","nbItem"=>count($articles_une),"carousel_indicators_color"=>"#C9462C")); 
            ?>
          </div>       
        </div>
        <div class="bloc_rubrique w-100 top-separator my-0">
          <div class="col-xs-12 col-sm-6 right-separator p-30">
            <div class="titre_rubrique w-100 b maj">
               Chiffre du jour
            </div>
            <div id="chiffre_container" class="contenu_rubrique w-100">
              
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 left-separator p-30" style="margin-left : -2px;">
            <div class="titre_rubrique w-100 b maj">
               Citation du jour
            </div>
            <div id="citation_container" class="contenu_rubrique w-100">
              
            </div>
          </div>
        </div>
        <div class="bloc_rubrique w-100 top-separator p-30">
          <div class="titre_rubrique w-100 b maj">
             Agenda
          </div>
          <div id="agenda_container" class="contenu_rubrique w-100">
            <?php
            $this->renderPartial('co2.views.pod.sliderGeneric',array("idCarousel"=>"agendaCarousel","nbItem"=>count($evenements),"carousel_indicators_color"=>"#C9462C")); 
            ?>
          </div>
        </div>
      </div>
      <div id="a2k_right-column" class="d-none d-sm-block col-sm-6 col-md-4 left-separator pl-0 pr-0" style="margin-left : -2px;">
        <div class="bloc_rubrique w-100 p-30">
          <div class="titre_rubrique w-100 b maj">
             Derniers articles
          </div>
          <div id="last_container" class="contenu_rubrique w-100">
          </div>
        </div>
      </div>
      <div id="a2k_full-column" class="col-xs-12 top-separator p-30">
        <div class="titre_rubrique w-100 b maj">
          Organisations amies
        </div>
        <div id="friend_container" class="contenu_rubrique mx-auto">
          <div class="col-xs-4 col-sm-3 col-md-2 friend" onclick="window.open('http://franceinsoumise.fr','_blank')">
            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/journalInsoumisChambery/logo-fi.png" alt="La France Insoumise">
          </div>
          <div class="col-xs-4 col-sm-3 col-md-2 friend" onclick="window.open('http://www.heuredupeuple.fr','_blank')">
            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/journalInsoumisChambery/logo-hdp.png" alt="L'Heure du Peuple">
          </div>
          <div class="col-xs-4 col-sm-3 col-md-2 friend" onclick="window.open('http://avenirencommun.fr','_blank')">
            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/journalInsoumisChambery/logo-aec.png" alt="L'Avenir en Commun">
          </div>        
          <div class="col-xs-4 col-sm-3 col-md-2 friend" onclick="window.open('http://www.lepartidegauche.fr','_blank')">
            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/journalInsoumisChambery/logo-pdg.png" alt="Le Parti de Gauche">
          </div>
          <div class="col-xs-4 col-sm-3 col-md-2 friend" onclick="window.open('http://www.fakirpresse.info','_blank')">
            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/journalInsoumisChambery/logo-fp.png" alt="Fakir Presse">
          </div>
        </div>
      </div> 
    </div>
  </div>
  <div id="a2k_footer" class="w-100">
      Contact - Mentions Légales
  </div>
</div>

<script type="text/javascript">

  var A2K_articles_une = new CO3_Article(null,<?php echo json_encode($articles_une); ?>);
  var A2K_articles = new CO3_Article(null,<?php echo json_encode($articles); ?>);
  var A2K_evenements = new CO3_Event(null,<?php echo json_encode($evenements); ?>);
  var A2K_chiffre = new CO3_ArticleBadge(null,<?php echo json_encode($chiffre); ?>);
  var A2K_citation = new CO3_ArticleBadge(null,<?php echo json_encode($citation); ?>);

  jQuery(document).ready(function() {
        setTitle("Journal de la FI Chambéry");
        
        $("#articleCarousel .carousel-inner").html(A2K_articles_une.SetCarousable().SetDisplayImg("full").RenderHtml());

        $("#last_container").html(A2K_articles.SetDisplayImg("none").RenderHtml());
        
        $("#chiffre_container").html(A2K_chiffre.RenderHtml());
        $("#citation_container").html(A2K_citation.RenderHtml());
  
        $("#agendaCarousel .carousel-inner").html(A2K_evenements.SetCarousable().RenderHtml());
            
        jsOnLoad();        
        setTimeout(function() {jsOnLoad();}, 1000); 
        setTimeout(function() {jsOnLoad();}, 5000);    // TODO arriver à choper le window on load !!!!
  });

  function jsOnLoad(){
    CO3_Article.forceListImRatio("#articleCarousel .container-img-profil",16/9,CO3_Article.carouselGetActiveWidth("#articleCarousel"));
    start_articleCarousel();
    //forceListImRatio("#last_container .container-img-profil",16/9);
    CO3_Event.forceListImRatio("#agendaCarousel .container-img-profil",1,CO3_Event.carouselGetActiveWidth("#agendaCarousel"));
    start_agendaCarousel();
  }

  $(window).resize(function() {
    CO3_Article.forceListImRatio("#articleCarousel .container-img-profil",16/9,CO3_Article.carouselGetActiveWidth("#articleCarousel"));
    //forceListImRatio("#last_container .container-img-profil",16/9);
    CO3_Event.forceListImRatio("#agendaCarousel .container-img-profil",1,CO3_Event.carouselGetActiveWidth("#agendaCarousel"));    
  });

  //directory.checkImage=function(){return null;};

</script>


