<!--
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
-->

<div class="w-100 header-costum h150">
	<a href="#welcome" class="lbh">
		<div class="w-100 mw1000 mx-auto">
			<img src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/journalInsoumisChambery/banner_jic.png" class="mh150 img-responsive mx-auto"/>
		</div>
	</a>
</div>
<div id="a2k_nav_rubrique" class="w-100">
    <div class="w-100 mw1000 mx-auto">
      <ul class="nav justify-content-center nav-pills"> 
       		<li class="nav-item">
	          <a class="nav-link lbh" href="#">Accueil</a>
	        </li>
	        <li class="nav-item">
	          <a class="nav-link lbh" href="#article?tags=grand%20chambery">Grand Chambéry</a>
	        </li>
	        <li class="nav-item">
	          <a class="nav-link lbh" href="#article?tags=grand%20dossier">Grands Dossiers</a>
	        </li>
	        <li class="nav-item">
	          <a class="nav-link lbh" href="#article?tags=actu%20politique">Actus Politiques</a>
	        </li>
	        <li class="nav-item">
	          <a class="nav-link lbh" href="#article?tags=sujet%20debat">Sujets Débat</a>
	        </li>
    	
      </ul>
    </div>
</div>