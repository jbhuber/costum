<footer class="text-center col-xs-12 pull-left no-padding bg-purple">
     <div class="">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 text-center col-footer col-footer-step">
                    <!--<h5><i class="fa fa-info-circle hidden-xs"></i> <?php echo Yii::t("home", "General information"); ?></h5>-->
                    <a href="https://transition-citoyenne.org/mentions-legales/" target="_blank" class="text-white"><?php echo Yii::t("home","Mentions légales") ?></a><br><br>
                    <!--<a href="mailto:pacte-local-subscribe@listes.transition-citoyenne.org" target="_blank"  class="text-white"><?php echo Yii::t("common","S'abonner à la newsletter") ?></a><br><br>-->
                    <a href="https://www.helloasso.com/associations/collectif-pour-une-transition-citoyenne/formulaires/2" target="_blank" class="text-white"><?php echo Yii::t("home","Faire un don") ?></a><br><br>

                    
                    <a href="https://nextcloud.transition-citoyenne.org/index.php/s/9rDBAorzmSErfzg" target="_blank" class=" hidden-xs"><?php echo Yii::t("home","Devenir bénévole") ?></a>
                   <!-- <a href="https://github.com/pixelhumain" target="_blank" 
                        class=" hidden-xs">
                        <i class="fa fa-github fa-2x bg-white img-circle padding-5 margin-5"></i>
                    </a>-->
                    <!--<a href="https://www.infomaniak.com/fr" target="_blank" class="">
                        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/img/logo-infomaniak.png" height=20 style="margin-top: -10px;border-radius: 3px;">
                    </a>-->
                    
                    
                </div>
                <!--<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 col-footer-ph">
                    <span class="font-blackoutT text-yellow-PH" style="font-size:20px;">by!!</span> 
                    <a href="https://www.communecter.org/#@pixelhumain" target="_blank">
                        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/img/LOGO_PIXEL_HUMAIN.png" height=70>
                    </a><br><br>

                    <a href="https://github.com/pixelhumain/co2" target="_blank" class=" hidden-xs">
                        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/img/CO2r.png" height=30>
                    </a>


                    
                </div>-->
                <div class="col-xs-12 col-sm-6 col-footer">
                    <span style="font-size:20px;"><a href="mailto:pacte@transition-citoyenne.org" style="color: #FFF !important;">pacte@transition-citoyenne.org</a></span>
                    <ul class="list-inline">
                        <li>
                            <a href="https://www.facebook.com/PacteTransition/" target="_blank" 
                               class="btn-social btn-outline">
                                <i class="fa fa-fw fa-facebook"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com/PacteTransition" target="_blank" 
                               class="btn-social btn-outline">
                                <i class="fa fa-fw fa-twitter"></i>
                            </a>
                        </li>
                     </ul>
                    <span class="">Powered by <a href="https://gitlab.adullact.net/pixelhumain" target="_blank"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/img/LOGO_PIXEL_HUMAIN.png" height=50></a></span>
                </div>
            </div>
        </div>
    </div>
</footer>
