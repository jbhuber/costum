<style type="text/css">
    footer{margin-top:0px;}
    #sub-doc-page{margin-top: 0px !important;}
    .content-input-scope-pacte #input-sec-search .shadow-input-header .input-global-search{
        border: 2px solid #5b2649;
        color: #5b2649;
        font-size: 20px;

    }
</style>
<div id="sub-doc-page">

    <div id="start" class="section-home section-home-video">
        <div class="col-xs-12 content-video-home no-padding">
          <div class="col-xs-12 no-padding container-video text-center" style="max-height: 450px;overflow-y: hidden;">
            <img class="img-responsive start-img" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/siteDuPactePourLaTransition/sengaer_bandeau.jpg' style="margin:auto;">
          </div>
        </div>
    </div>
    <div class="col-xs-12 section-separtor no-padding">
      <div class="col-xs-4 bg-orange"></div>
      <div class="col-xs-4 bg-blue"></div>
      <div class="col-xs-4 bg-orange"></div>
    </div>
    <div class="col-xs-12 no-padding"> 
      <div class="col-xs-12 col-sm-10 col-sm-offset-1  padding-20 text-center" style="font-size: 22px;margin-top: 20px;">
      Vous souhaitez contribuer à <span class="text-purple bold">la transformation</span> de <span class="text-orange">votre commune</span> ?<br/>
        Rejoignez un <span class="text-purple">collectif</span> <span class="text-orange">local</span> pour agir concrètement dans <span class="text-orange">votre territoire</span> et auprès de <span class="text-purple">vos candidat.es</span>.<br/><br/>
      </div>
        
    </div>
    <!--<div class="col-xs-12 support-section section-home col-md-10 col-md-offset-1 margin-bottom-20">
        <div class="col-xs-12 header-section margin-bottom-20">
            <h3 class="title-section col-sm-812"><i class="fa fa-hands"></i> >Vous souhaitez contribuer à la transformation de votre commune ?</h3>
            <hr>
        </div>
        <div class="col-xs-12">
            <span class="col-xs-12 text-left text-explain">
                Rejoignez un collectif local pour agir concrètement dans votre territoire et auprès de vos candidat.es.
            </span>
            <div class="col-xs-12 text-right margin-top-50">
                <a href="mailto:pacte@transitioncitoyenne.org" target="_blank" class="btn-main-menu btn-redirect-home">
                    <i class="fa fa-download"></i> La charte
                </a>
            </div> 
        </div>
    </div>-->
    <div id="search" class="section-home col-xs-12 padding-20" style="margin-top:0px;color:white; padding-bottom: 40px;margin-bottom: 40px;">
        <div class="col-xs-10 col-xs-offset-1 col-md-8 col-md-offset-2 header-section">
            <h3 class="text-center text-purple">Que se passe-t-il dans votre commune ?</h3>
        </div>
        <div class="col-xs-12">
              <div class="col-xs-12 text-center content-input-scope-pacte"></div>
        </div>
    </div>
    <div class="col-xs-12 section-separtor no-padding">
        <div class="col-xs-4 bg-orange"></div>
        <div class="col-xs-4 bg-blue"></div>
        <div class="col-xs-4 bg-orange"></div>
    </div>
   
</div>
<script type="text/javascript">
  
    jQuery(document).ready(function() {
        pacte.initScopeObj();
    //Login.runRegisterValidator();
});
</script>