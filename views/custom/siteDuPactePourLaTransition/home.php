<?php 
$cssAnsScriptFilesTheme = array(
        // SHOWDOWN
        '/plugins/showdown/showdown.min.js',
        //MARKDOWN
        '/plugins/to-markdown/to-markdown.js',              
    );
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);
$articles_une=Poi::getPoiByWhereSortAndLimit(array("rank"=>"true", "source.key"=>"siteDuPactePourLaTransition"),array("updated"=>-1), 3, 0);
?>
<style type="text/css">
	
</style>
<div class="col-xs-12 no-padding">
	<div id="start" class="section-home section-home-video">
		<div class="col-xs-12 content-video-home no-padding">
			<div class="col-xs-12 no-padding container-video">
				<!--<a href="javascript:;" id="launchVideo">
				<i class="fa fa-play-circle"></i>
				</a>-->
				<img class="img-responsive start-img" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/pactePourLaTransition/banner_ctc.png'>
			</div>
		</div>

		<!--<div id="indicators-container" class="col-md-4 col-md-offset-0 col-sm-10 col-sm-offset-1 col-xs-12 text-purple">
			<h4 class="text-center">Construire les communes de demain</h4>
			<div class="col-xs-12 text-center">
				<a href="#mesures" class="lbh-menu-app">
					<span class="stats-number text-purple">12</span>
					<span class="stats-label text-orange">Pactes</span><br/>
					<span class="explain">Retrouvez tous les pactes signés</span>
				</a>
			</div>
			<div class="col-xs-12 text-center">
				<a href="#search" class="lbh-menu-app">
					<span class="stats-number text-purple">21</span>
					<span class="stats-label text-orange">Groupes</span><br/>
					<span class="explain">Ils s'organisent dans les communes</span>
				</a>
			</div>
			<div class="col-xs-12">
				<a href="#toolkit" class="col-xs-10 col-xs-offset-1 margin-top-20 lbh-menu-app btn bg-purple btn-redirect-home" style="font-size: 25px;"><i class="fa fa-plus-circle"></i> Tout savoir</a>
			</div>
		</div>-->
	</div>
	<div class="col-xs-12 section-separtor no-padding">
		<div class="col-xs-4 bg-orange"></div>
		<div class="col-xs-4 bg-blue"></div>
		<div class="col-xs-4 bg-orange"></div>
	</div>
	<div class="col-md-12 col-sm-12 col-xs-12 padding-20 section-home" style="padding-left:100px; margin-top:0px;background-color: #f6f6f6;">
        <div class="col-xs-12 col-sm-10 col-sm-offset-1 pull-left padding-20 shadow2" style="margin-top: -19px;margin-bottom: -18px;background-color: #fff;font-size: 14px;z-index: 5;">
        <h3 class="col-xs-12 text-center">
                <small style="text-align: left">
                <!--<link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>-->
                  <bloquote style="/*font: 10 25px/1 'Pacifico', Helvetica, sans-serif;*/
  color: #2b2b2b;
  text-shadow: 1px 1px 0px rgba(0,0,0,0.1);">
				<span class="main-title" style="/*font-family:'Pacifico', Helvetica, sans-serif;*/color:#5b2649;   font-size: 45px;">Construire ensemble<br/>les communes de demain</span>
				<br/>
				<hr class="col-xs-4 col-xs-offset-4" style="border-top: 3px solid #fda521;margin-top: 35px;margin-bottom: 40px;">
				<br/>
				<div  class="col-sm-5 col-xs-12 text-center" style="font-size: 1.4em;padding-right:30px;">
					<i class="fa fa-2x fa-users" style="color:#5b2649;"></i><br/><br/>
					Les habitant.es <br/> définissent les priorités<br/> pour leurs communes
				</div>
				<div  class="col-sm-2 col-xs-4 col-xs-offset-4 col-sm-offset-0 margin-top-10 margin-bottom-10">
					<img class="img-responsive" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/siteDuPactePourLaTransition/logo-min.png'>
				</div>
				<div  class="col-sm-5 col-xs-12 text-center" style="font-size: 1.4em;padding-right:30px;">
					<i class="fa fa-2x fa-university" style="color:#5b2649;"></i><br/><br/>
					Les candidat.es <br/>s'engagent à les mettre en oeuvre<br/> une fois élu.es
				</div>
				</bloquote>
				<!--<hr class="col-xs-4 col-xs-offset-4" style="border-top: 3px solid #fda521; margin-top: 40px;margin-bottom: 35px;">-->
              </h3>
             <!-- <a href="javascript:;" data-hash="#pacte" class="btn btn-redirect-home lbh-menu-app col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1 col-md-6 col-md-offset-3 text-center" style="font-size: 22px !important;">
              	<i class="fa fa-info-circle"></i> Tout sur le pacte</a>-->
    	</div>
    </div>
	<div class="col-xs-12 section-separtor no-padding">
		<div class="col-xs-4 bg-orange"></div>
		<div class="col-xs-4 bg-blue"></div>
		<div class="col-xs-4 bg-orange"></div>
	</div>
	<div id="search" class="section-home col-xs-12 bg-purple padding-20" style="margin-top:0px;color:white; padding-bottom: 40px;">
		<!-- <div class="col-xs-10 col-xs-offset-1 col-md-8 col-md-offset-2 header-section">
			<h3 class="text-center">S'engager dans votre commune en trouvant</h3>
		</div> -->
		<div class="col-xs-12">
			<h3 class="text-center"><i class="fa fa-search"></i> Que se passe-t-il dans <span class="text-orange">ma commune</span> ?</h3>
             <div class="col-xs-12 text-center content-input-scope-pacte"></div>
        </div>
        <div class="col-xs-12 text-center" style="font-size: 20px;padding-right:30px;padding-top: 30px;">
        	<span> Le Pacte pour la Transition vise à proposer 32 mesures concrètes pour construire des communes<br/> plus écologiques, plus solidaires et plus démocratiques, en vue des élections municipales de 2020 </span>
        </div>
        <div class="col-xs-12">
        	<center><h4><a href="javascript:;" data-hash="#mesures" class="lbh-menu-app btn btn-redirect-home onBgPurple col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3 col-xs-12">Voir les mesures</a></h4></center>
        </div>
        
		<!-- <div class="col-xs-12">
			<h3 class="text-center"><span class="text-orange">Mais</span> aussi...</h3>
			<a href="javascript:;" data-hash="#mesures" class="lbh-menu-app btn btn-redirect-home col-md-4 col-sm-6 col-md-offset-2 col-xs-12">
					
				Les<br/>
              	<span class="text-orange number-btn">32</span><br>
				Mesures
			</a>
			<a href="javascript:;" data-hash="#toolkit" class="col-md-4 col-sm-6 col-xs-12 lbh-menu-app btn btn-redirect-home">
				Et ...<br>
				<span class="text-orange">Une multitude</span><br>
				des ressources
			</a>
	
		</div> -->
	</div>
	<div class="col-xs-12 section-separtor no-padding">
		<div class="col-xs-4 bg-orange"></div>
		<div class="col-xs-4 bg-blue"></div>
		<div class="col-xs-4 bg-orange"></div>
	</div>
	<div id="actus" class="section-home col-xs-12 col-md-10 col-md-offset-1 padding-20">
		<div class="col-xs-12 header-section">
			<h3 class="title-section col-sm-8 col-xs-12">
				<i class="fa fa-rss"></i> L'actualité du Pacte
			</h3>
			<a href="javascript:;" data-hash="#actualites" class="lbh-menu-app btn btn-redirect-home btn-small-orange">Toute l'actu<span class="hidden-sm">alité</span></a>
			<hr/>
		</div>
		<div class="col-xs-12" id="actus-pacte">
		</div>
		<!--<div class="col-xs-12">
			<a href="mailto:pacte-local-subscribe@listes.transition-citoyenne.org" target="_blank" class="btn btn-redirect-home col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3 col-xs-12 text-center"
			style="font-size: 22px !important;">Suivre l'actualité</a>
		</div>-->
	</div>
	<div class="col-xs-12 section-separtor no-padding">
		<div class="col-xs-4 bg-orange"></div>
		<div class="col-xs-4 bg-blue"></div>
		<div class="col-xs-4 bg-orange"></div>
	</div>
	<div id="search" class="section-home col-xs-12 no-padding" style="margin-top: 0px;">
		<img class="img-responsive" style="margin: auto;" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/siteDuPactePourLaTransition/bande_site.png'>
	</div>
	<div class="col-xs-12 section-separtor no-padding">
		<div class="col-xs-4 bg-orange"></div>
		<div class="col-xs-4 bg-blue"></div>
		<div class="col-xs-4 bg-orange"></div>
	</div>
	<!--<div id="useit" class="section-home col-md-10 col-md-offset-1">
		<div class="col-xs-12 header-section">
			<h3 id="porterPacte" class="title-section col-sm-8">Vous voulez participer hors de votre commune ?</h3>
			<hr/>
		</div>
        <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 padding-20 text-explain" >
			<!-- <a href="https://framaforms.org/devenir-benevole-pour-le-pacte-pour-la-tran
			sition-1551263682" target="_blank" class="btn-main-menu col-xs-12 col-sm-6 btn-redirect-home btn-useit"  >
				<div class="text-center">
				  <div class="col-md-12 no-padding text-center">
				      <h4 class="no-margin uppercase">
				        <i class="fa fa-group"></i><br/>
				        <?php //echo Yii::t("home","Devenir<br/>bénévole") ?>
				      </h4>
				  </div>
				</div>
			</a> -->

			<!--<a href=" https://my.sendinblue.com/users/subscribe/js_id/3t7aq/id/1" target="_blank" class="btn-main-menu col-xs-12 col-sm-4 pull-right btn-redirect-home btn-useit"  >

			  <div class="text-center">
			      <div class="col-md-12 no-padding text-center ">
			          <h4 class="no-margin uppercase">
			            <i class="fa fa-rss"></i><br/>
			            <?php echo Yii::t("home","Vous abonner<br/> à la Newsletter") ?>
			          </h4>
			      </div>
			  </div>
			</a>
			<a href="mailto:pacte@transitioncitoyenne.org" target="_blank" class="btn-main-menu col-xs-12 col-sm-5  margin-top-20 btn-redirect-home btn-useit">
			  <div class="text-center">
			      <div class="col-md-12 no-padding text-center">
			          <h4 class="no-margin uppercase">
			            <i class="fa fa-envelope"></i><br/>
			            <?php echo Yii::t("home","Nous<br/> écrire") ?>
			          </h4>
			      </div>
			  </div>
			</a>-->
			<!--<a href="https://www.helloasso.com/associations/collectif-pour-une-transitio
			n-citoyenne/formulaires/2" target="_blank" class="btn-main-menu col-xs-12 col-sm-4 margin-top-20 btn-redirect-home btn-useit pull-right">
			  <div class="text-center">
			      <div class="col-md-12 no-padding text-center">
			          <h4 class="no-margin uppercase">
			            <i class="fa fa-heart"></i><br/>
			            <?php echo Yii::t("home","Faire un don") ?>
			          </h4>
			      </div>
			  </div>
			</a>-->

      <!--</div>-->
	   <!--  <div class="text-center col-xs-12 col-md-8 col-md-offset-2 text-center text-orange margin-top-20"><b><span style="font-size:30px;">...Et en parler tout autour de vous !!</span></b>
	    </div> -->
    <!--</div>-->
    <div id="partenaire" class="col-xs-12 support-section section-home col-md-10 col-md-offset-1">
        <div class="col-xs-12 header-section">
        	<h3 class="title-section col-sm-8"><i class="fa fa-connectdevelop"></i> Partenaires</h3>
        	<hr/>
        </div>
        <div class="col-xs-12 no-padding">
          <img class="img-responsive" style="margin: auto;" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/siteDuPactePourLaTransition/partenaires_pacte.png'>

          
        </div>
        <!-- <div class="col-xs-12 padding-20">
          	<a href="mailto:pacte@transitioncitoyenne.org" target="_blank" class="btn-main-menu col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 btn-redirect-home"  >
	            <div class="text-center">
	                <div class="col-md-12 no-padding text-center">
	                    <h4 class="no-margin uppercase">
	                      <i class="fa fa-hand-point-right faa-pulse"></i>
	                      <?php //echo Yii::t("home","Mon organisation veut devenir partenaire") ?>
	                    </h4>
	                </div>
	            </div>
          	</a>
        </div> -->
	</div>
</div>
<script type="text/javascript">
	var imgH=0;
	var articles_une = new CO3_Article(null,<?php echo json_encode($articles_une); ?>);
	function specImage($this){
		imgH=$($this).height();
		$("#launchVideo").css({"line-height": imgH+"px"});
		$(".container-video").css({"max-height": imgH+"px"});
		$("#launchVideo").click(function(){
			str='<div style="padding:0 0 0 0;position:relative;height:'+imgH+'px;">'+
					'<iframe src="https://player.vimeo.com/video/106193787?title=0&byline=0&portrait=0&autoplay=1" '+' style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" '+  'webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>'+
				'</div>';
			$(".container-video").addClass("col-md-10 col-md-offset-1").html(str);	
		});
	}
	jQuery(document).ready(function() {
		pacte.initScopeObj();
		setTitle("Pacte pour la Transition");
		$("#actus-pacte").html(articles_une.SetColNum(3).SetDisplaySocial(false).RenderHtml());
		bindLBHLinks();
		/*$(".smoothScroll").click(function(){
			simpleScroll($($(this).data("hash")).position().top);
		});*/
	});

</script>