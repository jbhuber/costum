
<div class="pageContent">

<style type="text/css">
.row_title {
  margin-top:10px;
}

#bg-homepage {
  width:100%;
}

.numberCircle {
  font-size: 50px;
  font-weight: bold;
  margin-right: 15px;
}

p.step {
  font-size:30px;
  font-weight:bold;
  display : flex;
  align-items : center;
}

ul.mocica {
    list-style: none;
    margin-left: 0;
    padding-left: 1.2em;
    text-indent: -1.2em;
}

ul.mocica li:before {
    content: "►";
    display: block;
    float: left;
    width: 1.2em;
    color: #00B1E4;
}

ul.mocica li {
  font-size:1.5em;
  margin-bottom:10px;
}

</style>

<!--<div class="row row_title">
		<p class="text-center">
			<img src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/mocica/title_mocica3.png' alt="MOCICA" class="mocica" style="width:450px;margin-left:40px;"/>
			<img src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/mocica/title_logo.png' alt="MOCICA" class="logo" style="width:170px;"/>
		</p>
		<h1 class="phrase text-center">Unis et Libres</h1>
	</div>-->

  <div class="container">
   <p style="margin-top:15px;">
  L’objectif du projet Mocica est le passage à une société sans argent ni dirigeant suivant 3 étapes :
  </p>


  <p class="step" style="color:#F44336;"><span class='numberCircle'><!-- &#x2776; -->&#x2780;</span> Le rassemblement</p>
  <p>Nous nous retrouvons dans nos assemblées de quartier et mettons en place notre gouvernance autogérée ODG (Organisation Démocratique Globale).</p>

  <p class="step" style="color:#448AFF;"><span class='numberCircle'><!-- &#x2777; -->&#x2781;</span> La transition</p>
  <p>Nous continuons nos activités habituelles sans utiliser d’argent.</p>

    <p class="step" style="color:#4CAF50;"><span class='numberCircle'><!-- &#x2778; -->&#x2782;</span> L’organisation</p>
  <p>Nous mettons en place notre société non marchande suivant les modalités choisies ensemble.</p>
<p>Tous les détails du projet Mocica <a href='https://mocica.org'>ici</a>.</p>

<p>
<br/>
Vous trouverez sur cette plateforme de nombreux outils :
<ul class='mocica'>
<li>Créer des assemblées</li>
<li>Espace de discussion / débat icônes</li>
<li>Une cartographie</li>
<li>Faire des annonces</li>
<li>Sondage</li>
<li>Un agenda commun</li>
<li>Un espace d’entraide</li>
<li>Télécharger des documents</li>
</p>
  </div>
    
</div>

<script type="text/javascript">
  jQuery(document).ready(function() {
    setTitle("Projet MOCICA Unis et Libre");
  });
</script>

