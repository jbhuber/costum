
<div class="pageContent">


<style type="text/css">
  #customHeader{
    margin-top: 0px;
  }
  #costumBanner{
   /* max-height: 375px; */
  }
  #costumBanner h1{
    position: absolute;
    color: white;
    background-color: rgba(0,0,0,0.4);
    font-size: 29px;
    bottom: 0px;
    padding: 20px;
  }
  #costumBanner h1 span{
    color: #eeeeee;
    font-style: italic;
  }
  #costumBanner img{
    min-width: 100%;
  }
  .btn-main-menu{
    background: #1b7baf;
    border-radius: 20px;
    padding: 20px !important;
    color: white;
    cursor: pointer;
    border:3px solid transparent;
    /*min-height:100px;*/
  }
  .btn-main-menu:hover{
    border:2px solid #1b7baf;
    background-color: white;
    color: #1b7baf;
  }
  .ourvalues img{
    height:70px;
  }
  .main-title{
    color: #450e33;
  }

  .ourvalues h3{
    font-size: 36px;
  }
  .box-register label.letter-black{
    margin-bottom:3px;
    font-size: 13px;
  }
  .bullet-point{
      width: 5px;
    height: 5px;
    display: -webkit-inline-box;
    border-radius: 100%;
    background-color: #fbae55;
  }
  .text-explain{
    color: #555;
    font-size: 18px;
  }
  .blue-bg {
  background-color: white;
  color: #5b2549;
  height: 100%;
  padding-bottom: 20px !important;
}

.circle {
  font-weight: bold;
  padding: 15px 20px;
  border-radius: 50%;
  background-color: #fea621;
  color: white;
  max-height: 50px;
  z-index: 2;
}
.circle.active{
      background: #ea4335;
    border: inset 3px #ea4335;
    max-height: 70px;
    height: 70px;
    font-size: 25px;
    width: 70px;
}
.support-section{
  background-color: white;
}
.support-section h2{
  text-align: center;
    padding: 60px 0px !important;
    background: #450e33;
    font-size: 40px;
    color: white;
    margin-bottom: 20px;
}
.timeline-ctc h2{
 text-align: center;
    padding: 105px 0px 60px 0px !important;
    background: #450e33;
    font-size: 40px;
    color: white;
    margin-bottom: 20px;
}
.how-it-works.row {
  display: flex;
}
.row.timeline{
  display: flex;
  -ms-flex-wrap: wrap;
  flex-wrap: wrap;
  margin-right: -15px;
  margin-left: -15px;
}
.how-it-works.row .col-2 {
  display: inline-flex;
  align-self: stretch;
  position: relative;
  align-items: center;
  justify-content: center;
}
.how-it-works.row .col-2::after {
  content: "";
  position: absolute;
  border-left: 3px solid #0091c6;
  z-index: 1;
}
.pb-3, .py-3 {
    padding-bottom: 1rem !important;
}
.pt-2, .py-2 {
    padding-top: 0.5rem !important;
}
.how-it-works.row .col-2.bottom::after {
  height: 50%;
  left: 50%;
  top: 50%;
}
.how-it-works.row.justify-content-end .col-2.full::after {
  height: 100%;
  left: calc(50% - 3px);
}
.how-it-works.row .col-2.full::after {
    height: 100%;
    left: calc(50% - 0px);
}
.how-it-works.row .col-2.top::after {
  height: 50%;
  left: 50%;
  top: 0;
}

.timeline div {
  padding: 0;
  height: 40px;
}
.timeline hr {
  border-top: 3px solid #0091c6;
  margin: 0;
  top: 17px;
  position: relative;
}
.timeline .col-2 {
  display: flex;
  overflow: hidden;
  flex: 0 0 16.666667%;
    max-width: 16.666667%;
}
.align-items-center {
    -ms-flex-align: center !important;
    align-items: center !important;
}
.justify-content-end {
    -ms-flex-pack: end !important;
    justify-content: flex-end !important;
}
.row {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: -15px;
    margin-left: -15px;
}
.how-it-works.row .col-6 p{
  color: #444;
}
.how-it-works.row .col-6 h5{
font-size: 17px;
    text-transform: inherit;
}
.col-2 {
    -ms-flex: 0 0 16.666667%;
    flex: 0 0 16.666667%;
    max-width: 16.666667%;
}
.col-6 {
    -ms-flex: 0 0 50%;
    flex: 0 0 50%;
    max-width: 50%;
}
.timeline .col-8 {  
    flex: 0 0 66.666667%;
    max-width: 66.666667%;
}
.timeline .corner {
  border: 3px solid #0091c6;
  width: 100%;
  position: relative;
  border-radius: 15px;
}
.timeline .top-right {
  left: 50%;
  top: -50%;
}
.timeline .left-bottom {
  left: -50%;
  top: calc(50% - 3px);
}
.timeline .top-left {
  left: -50%;
  top: -50%;
}
.timeline .right-bottom {
  left: 50%;
  top: calc(50% - 3px);
}

  @media screen and (min-width: 450px) and (max-width: 1024px) {
    .logoDescription{
      width: 60%;
      margin:auto;
    }
  }

  @media (max-width: 1024px){
    #customHeader{
      margin-top: -1px;
    }
  }
  @media (max-width: 768px){

  }
  #titleCostum{
        position: relative;
        top: 465px;
        right: 0px;
        background-color: rgba(0,0,0,0.7);
        color:white;
        font-size: 44px;
        font-weight: bolder;
        z-index: 10;
        padding: 10px;
        text-align: right;
        width: 70%;
    }

</style>


<div class="col-xs-12 no-padding" id="customHeader" style="margin-top: -83px; background-color: white">
  <div id="titleCostum">LILA : Les Interfaces Libre d'Arianne</div>
    <div class="col-xs-12 text-center margin-bottom-50" style="padding:0px;">
    <div id="docCarousel" class="carousel slide" data-ride="carousel">
        <!-- Round button indicators -->
        <ol class="carousel-indicators">
          <li data-target="#docCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#docCarousel" data-slide-to="1" class=""></li>
        </ol>

        <!-- Wrapper for slides -->
        <style type="text/css">
          div.item img{margin:auto;}
        </style>
        <div class="carousel-inner" role="listbox">
          <div class="item active"><img class="img-responsive" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/lila/banner1.jpg'> </div>
          <div class="item"><img class="img-responsive" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/lila/banner.jpg'> </div>
          
        
        </div>
    </div>
  <!--<div class="col-md-12 col-lg-12 col-sm-12 imageSection no-padding" 
     style=" position:relative;">-->
  <div class="col-sm-12 col-md-12 col-xs-12 no-padding" style="background-color:#450e33; max-width:100%; float:left;">
    <div class="col-xs-12 no-padding" style="margin-top:100px;"> 
      <div class="col-xs-12 no-padding">
        <div class="col-md-12 col-sm-12 col-xs-12 padding-20" style="padding-left:100px;background-color: #f6f6f6; min-height:400px;">
          <div class="col-xs-12 col-sm-10 col-sm-offset-1 pull-left padding-20 shadow2" style="margin-top:-80px;margin-bottom:-80px;background-color: #fff;font-size: 14px;z-index: 5;">
            <div class="col-xs-12 font-montserrat ourvalues" style="text-align:center;">
              <h3 class="col-xs-12 text-center">
                <span class="main-title">Construisez les solutions de demain</span><br>
                <small>
                  <b>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam
                  
                </small>
                <hr style="width:40%; margin:20px auto; border: 4px solid #fbae55;">
              </h3>
              <div class="col-xs-12">
                <a href="#porterPacte" data-hash="#dda" class="btn-main-menu lbh-menu-app col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3"  >
                    <div class="text-center">
                        <div class="col-md-12 no-padding text-center">
                            <h4 class="no-margin uppercase">
                              <i class="fa fa-hand-point-right faa-pulse"></i>
                              <?php echo Yii::t("home","JE CONTRIBUE AU PACTE") ?>
                            </h4>
                        </div>
                    </div>
                </a>
              </div>
              <h3 class="col-xs-12 text-center">
                <hr style="width:40%; margin:20px auto; border: 4px solid #fbae55;">
              </h3>
              <div class="col-md-10 col-md-offset-1 col-xs-12">
                <span class="text-explain">Nous sommes de plus en plus nombreux à vouloir agir face au dérèglement climatique, à la croissance des inégalités, à la crise de la démocratie.<br/><span class="bullet-point"></span><br/>
                Pour relever ces défis majeurs, changer nos pratiques quotidiennes de consommation ne suffit plus.
                Il nous faut également œuvrer à la transformation de nos politiques publiques.<br/><span class="bullet-point"></span><br/>
                Si la transition doit avoir lieu à toutes les échelles, les communes peuvent être le fer de lance de ce
                mouvement.<br/>
                En mars 2020, des candidat.e.s se présenteront aux élections municipales. Nous voulons les aider à
                identifier et mettre en œuvre, une fois élu.e.s, des mesures concrètes pour encourager la transition
                écologique, sociale et démocratique de leur commune.<br/><span class="bullet-point"></span><br/>
                Nous, citoyennes et citoyens, sommes les mieux placés pour définir ces priorités et construire les
                communes de demain. Ensemble, nous sommes la transition.
                </span>
              </div>
            </div>
          </div>

        </div>

      </div>
      <div class="timeline-ctc col-xs-12 no-padding" style="font-weight: 300;height: 100%;text-align: inherit;">
             
              <div class="container-fluid blue-bg no-padding">
                <div class="container col-xs-12 no-margin no-padding">
                  <h2 class="pb-3 pt-2">
                    <i class="fa fa-calendar"></i> L'agenda du pacte
                  </h2>
                  <!--first section-->
                  <div class="row align-items-center how-it-works">
                    <div class="col-2 text-center bottom">
                      <div class="circle">1</div>
                    </div>
                    <div class="col-6">
                      <h5>Octobre à Décembre 2018</h5>
                      <p>Consultation des organisations engagées dans la transition écologique et citoyenne</p>
                    </div>
                  </div>
                  <!--path between 1-2-->
                  <div class="row timeline">
                    <div class="col-2">
                      <div class="corner top-right"></div>
                    </div>
                    <div class="col-8">
                      <hr/>
                    </div>
                    <div class="col-2">
                      <div class="corner left-bottom"></div>
                    </div>
                  </div>
                  <!--second section-->
                  <div class="row align-items-center justify-content-end how-it-works">
                    <div class="col-6 text-right">
                      <h5>Du 25 janvier au 28 février 2019</h5>
                      <p>Consultation citoyenne</p>
                    </div>
                    <div class="col-2 text-center full">
                      <div class="circle">2</div>
                    </div>
                  </div>
                  <!--path between 2-3-->
                  <div class="row timeline">
                    <div class="col-2">
                      <div class="corner right-bottom"></div>
                    </div>
                    <div class="col-8">
                      <hr/>
                    </div>
                    <div class="col-2">
                      <div class="corner top-left"></div>
                    </div>
                  </div>
                  <!--third section-->
                  <div class="row align-items-center how-it-works">
                    <div class="col-2 text-center full">
                      <div class="circle active">3</div>
                    </div>
                    <div class="col-6">
                      <h5>Mars 2019</h5>
                      <p>Validation des 30 mesures définitives du Pacte pour la Transition par un comité d'expert.e.s</p>
                    </div>
                  </div>
                   <!--path between 3-4-->
                  <div class="row timeline">
                    <div class="col-2">
                      <div class="corner top-right"></div>
                    </div>
                    <div class="col-8">
                      <hr/>
                    </div>
                    <div class="col-2">
                      <div class="corner left-bottom"></div>
                    </div>
                  </div>
                  <!--4th section-->
                  <div class="row align-items-center justify-content-end how-it-works">
                    <div class="col-6 text-right">
                      <h5>Avril 2019 à Mars 2020</h5>
                      <p>Dialogue des groupes locaux porteurs du Pacte avec les candidats aux élections municipales de 2020</p>
                    </div>
                    <div class="col-2 text-center full">
                      <div class="circle">4</div>
                    </div>
                  </div>
                  <!--path between 4-5-->
                  <div class="row timeline">
                    <div class="col-2">
                      <div class="corner right-bottom"></div>
                    </div>
                    <div class="col-8">
                      <hr/>
                    </div>
                    <div class="col-2">
                      <div class="corner top-left"></div>
                    </div>
                  </div>
                  <!--5th section-->
                  <div class="row align-items-center how-it-works">
                    <div class="col-2 text-center full">
                      <div class="circle">5</div>
                    </div>
                    <div class="col-6">
                      <h5>Mars 2020</h5>
                      <p>Elections municipales en France</p>
                    </div>
                  </div>
                   <!--path between 3-4-->
                  <div class="row timeline">
                    <div class="col-2">
                      <div class="corner top-right"></div>
                    </div>
                    <div class="col-8">
                      <hr/>
                    </div>
                    <div class="col-2">
                      <div class="corner left-bottom"></div>
                    </div>
                  </div>
                  <!--6th section-->
                  <div class="row align-items-center justify-content-end how-it-works">
                    <div class="col-6 text-right">
                      <h5>A partir d'avril 2020</h5>
                      <p>Suivi de la mise en place des mesures avec les élus</p>
                    </div>
                    <div class="col-2 text-center top">
                      <div class="circle">6</div>
                    </div>
                  </div>
                  <!--path between 6-7-->
                  <!--<div class="row timeline">
                    <div class="col-2">
                      <div class="corner right-bottom"></div>
                    </div>
                    <div class="col-8">
                      <hr/>
                    </div>
                    <div class="col-2">
                      <div class="corner top-left"></div>
                    </div>
                  </div>-->
                  <!--7th section-->
                  <!--<div class="row align-items-center how-it-works">
                    <div class="col-2 text-center top">
                      <div class="circle">7</div>
                    </div>
                    <div class="col-6">
                      <h5>À partir de Mars 2020</h5>
                      <p>Suivi de la mise en œuvre</p>
                    </div>
                  </div>-->
                </div>
                 <a href="https://framaforms.org/toutes-expertes-1547813693" target="_blank" class="btn-main-menu col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 margin-top-20"  >
                    <div class="text-center">
                        <div class="col-md-12 no-padding text-center">
                            <h4 class="no-margin uppercase">
                              <i class="fa fa-hand-point-right faa-pulse"></i>
                              <?php echo Yii::t("home","Je suis citoyen.ne expert.e") ?>
                            </h4>
                        </div>
                    </div>
                </a>
              </div>

      </div>
      <div class="col-xs-12 no-padding support-section">
        <h2 id="porterPacte"><i class="fa fa-connectdevelop"></i> Je veux porter le Pacte dans ma commune</h2>
        <div class="col-xs-12 padding-20 text-explain" >
          <b>Bonne nouvelle !</b> Alors voilà des pistes pour procéder : <br/><br/>
          <ul  class="padding-10">
            <li>Une cartographie sortira courant avril 2019 pour s'inscrire et repérer les autres personnes de son territoire
également volontaires pour porter le Pacte. Cet outil vous permettra de vous mettre en relation, pour vous réunir
autour du même but et créer un "groupe Pacte" dans votre commune.</li>
            <li>Une fois quelques personnes motivées réunies dans la commune (et ce groupe constitué), réfléchissez ensemble
aux 30 mesures du Pacte (celles qui sont les plus pertinentes sur votre territoire, celles qui sont déjà mises en
œuvre, etc) et allez échanger avec les candidat.e.s aux élections Municipales de mars 2020. Puis engagez-les à
signer le Pacte en choisissant 5 mesures à mettre en œuvre s'ils.elles sont élu.e.s.</li>
            <li>D'ici là, vous pouvez toujours :</li>
          </ul>
          <br/>
          <a href="https://framaforms.org/devenir-benevole-pour-le-pacte-pour-la-tran
sition-1551263682" target="_blank" class="btn-main-menu col-xs-12 col-sm-8 col-sm-offset-2 col-md-2 col-md-offset-0 margin-top-20"  >
              <div class="text-center">
                  <div class="col-md-12 no-padding text-center">
                      <h4 class="no-margin uppercase">
                        <i class="fa fa-hand-point-right faa-pulse"></i>
                        <?php echo Yii::t("home","Devenir bénévole") ?>
                      </h4>
                  </div>
              </div>
          </a>
          <a href=" https://my.sendinblue.com/users/subscribe/js_id/3t7aq/id/1" target="_blank" class="btn-main-menu col-xs-12 col-sm-8 col-sm-offset-2 col-md-2 col-md-offset-1 margin-top-20"  >
              <div class="text-center">
                  <div class="col-md-12 no-padding text-center">
                      <h4 class="no-margin uppercase">
                        <i class="fa fa-hand-point-right faa-pulse"></i>
                        <?php echo Yii::t("home","Vous abonner à la Newsletter") ?>
                      </h4>
                  </div>
              </div>
          </a>
          <a href="mailto:pacte@transitioncitoyenne.org" target="_blank" class="btn-main-menu col-xs-12 col-sm-8 col-sm-offset-2 col-md-2 col-md-offset-1 margin-top-20"  >
              <div class="text-center">
                  <div class="col-md-12 no-padding text-center">
                      <h4 class="no-margin uppercase">
                        <i class="fa fa-hand-point-right faa-pulse"></i>
                        <?php echo Yii::t("home","Nous écrire") ?>
                      </h4>
                  </div>
              </div>
          </a>
          <a href="https://www.helloasso.com/associations/collectif-pour-une-transitio
n-citoyenne/formulaires/2" target="_blank" class="btn-main-menu col-xs-12 col-sm-8 col-sm-offset-2 col-md-2 col-md-offset-1 margin-top-20"  >
              <div class="text-center">
                  <div class="col-md-12 no-padding text-center">
                      <h4 class="no-margin uppercase">
                        <i class="fa fa-hand-point-right faa-pulse"></i>
                        <?php echo Yii::t("home","Participer à notre cagnotte") ?>
                      </h4>
                  </div>
              </div>
          </a>

          
          <div class="text-center col-xs-12"><br/> <br/><b>...Et en parler tout autour de vous !!</b></div>
        </div>
      </div>
      <div class="col-xs-12 no-padding support-section">
        <h2><i class="fa fa-connectdevelop"></i> A propos des mesures</h2>
        <div class="col-xs-12 padding-20 text-explain">
          Le Pacte est constitué d'une trentaine de mesures, élaborées dans un premier temps grâce à la collaboration d'une cinquantaine d'organisations, puis dans un second temps modifiées et réécrites suite à une consultation citoyenne. <br/><br/>
        Cette consultation, désormais terminée, a su apporter quelques :<br/><br/>
        <ul class="text-center">
          <li>### votes</li>
          <li>## nouvelles idées,</li>
          <li>## propositions d'amendements,</li>
        </ul>
        <br/>
        Et tout cela grâce à ## participants.<br/>
        Toutes ces contributions sont actuellement analysées par un comité d'expert.e.s (citoyen.ne.s, partenaires,
        chercheur.e.s et élu.e.s) afin d'en prendre compte et de rédiger la version finale des mesures, dont la sortie est prévue pour fin mars. L'ensemble des contributions de la consultation reste tout de même accessible, il suffit de cliquer ci-dessous : <br/><br/>

        <a href="https://framaforms.org/toutes-expertes-1547813693" target="_blank" class="btn-main-menu col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 margin-top-20"  >
          <div class="text-center">
              <div class="col-md-12 no-padding text-center">
                  <h4 class="no-margin uppercase">
                    <i class="fa fa-hand-point-right faa-pulse"></i>
                    <?php echo Yii::t("home","Consultez les contributions de la consultation") ?>
                  </h4>
              </div>
          </div>
        </a>
        </div>
      </div>
      <div class="col-xs-12 no-padding support-section">
        <h2><i class="fa fa-connectdevelop"></i> Ils participent</h2>
        <div class="col-xs-12 no-padding">
          <img class="img-responsive" style="margin: auto;" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/pactePourLaTransition/partenaires_pacte.png'>

          
        </div>
        <div class="col-xs-12 padding-20">
          <a href="mailto:pacte@transitioncitoyenne.org" target="_blank" class="btn-main-menu col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 margin-top-20"  >
            <div class="text-center">
                <div class="col-md-12 no-padding text-center">
                    <h4 class="no-margin uppercase">
                      <i class="fa fa-hand-point-right faa-pulse"></i>
                      <?php echo Yii::t("home","Mon organisation veut devenir partenaire") ?>
                    </h4>
                </div>
            </div>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  jQuery(document).ready(function() {
    setTitle("Le pacte pour la transition");
  });
</script>


