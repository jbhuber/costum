
<style type="text/css">

.menuWho .nav-tabs a{
	background-color: #ea7d246b;
}

.menuWho .nav-tabs .active a{
	background-color: #ea7d24 !important ;
}

.contentWho{
	background-color: #faf8e5 ;
}

</style>
<div class=" col-xs-10 col-xs-offset-1 padding-10">

	<div class="menuWho">
		<ul class="row nav nav-tabs text-center" style="font-size: 18px">
			<li class="nav-tab col-xs-3 active projectBgColorActive projectBgColorHover"><a data-toggle="tab" href="#presentation"><b>Présentation</b></a></li>
			<li class="nav-tab col-xs-3 projectBgColorActive projectBgColorHover"><a data-toggle="tab" href="#history"><b>L'histoire</b></a></li>
			<li class="nav-tab col-xs-3 projectBgColorActive projectBgColorHover"><a data-toggle="tab" href="#status"><b>Les statuts</b></a></li>
			<li class="nav-tab col-xs-3 projectBgColorActive projectBgColorHover"><a data-toggle="tab" href="#rihva"><b>Adhérer à RIHVA</b></a></li>
		</ul>
	</div>

	<div class="contentWho tab-content col-xs-12 padding-10">
		<div id="presentation" class="tab-pane active row col-xs-10 col-xs-offset-1">
			Nous sommes un réseau d'acteurs de la <b>Haute Vallée de l'Aude</b> (HVA), adhérents de l'association <b>RIHVA</b> (Réseau des Initiatives de la Haute Vallée de l'Aude), fortement impliqués dans l'économie sociale, solidaire et écologique de ce territoire. <br/><br/>

			Nous vous proposons ce site internet novateur et collaboratif pour : <br/>
			<ul>
				<li>
					<b>Promouvoir les activités des acteurs locaux par une vitrine qui :</b>
					<ul>
						<li>Augmente leur visibilité et leur mutualisation.</li>
						<li>Allège leurs efforts de communication en réduisant leurs coûts de conception, de publication et de distribution de leurs informations.</li>
						<li>Consolide leur ancrage au sein du territoire en accroissant leur panel d'utilisateurs, de consommateurs.</li>
					</ul>
				</li>
				<li>
					<b>Offrir aux habitants et aux visiteurs un espace dédié qui :</b>
					<ul>
						<li>Concentre, classe et filtre les informations pour faciliter la recherche, le choix et l'identification </li>
						<li>Encourage la rencontre, l'échange intergénérationnel, la participation, l'implication.</li>
						<li>Aide à la découverte des nombreuses et dynamiques ressources locales.</li>
						<li>Réduit l'impact écologique de l'addition des moyens de communications.</li>
					</ul>
				</li>
			</ul>

		</div>
		<div id="history" class="tab-pane row col-xs-10 col-xs-offset-1">
			En  2016 à Greffeil ont eu lieu les rencontres annuelles des amis de François De Ravignan ayant pour thème <b>''Relier les Initiatives de la Haute Vallée de l'Aude''. </b>
			Les échanges sur ce sujet ont permis de mettre en exergue un ensemble de constats assez particuliers:<br/><br/>
			<ul class="niv1">
			<li><b>Un foisonnement d'acteurs et de structures collectives</b>, pour la plupart nouvellement arrivés sur ce territoire, œuvrant principalement dans des domaines sociaux, environnementaux et culturels et portés par une vision commune d'un vivre ensemble bienveillant, épanouissant et réjouissant.</li>

			<li>Mais ce tissu, quoique dynamique, souffre d'un manque de réelles synergies entre les initiateurs et de carence de visibilité de leurs actions, les deux étant souvent liées à des problématiques rurales et organisationnelles.</li>
			<li><b>Un besoin apparaissait donc en faveur de la valorisation et du renforcement de ces ressources et de ces potentialités locales inscrites dans une démarche de développement social et économique local.</b></li>
			</ul><br/><br/>
			Les rencontres suivantes des ami-es de François de Ravignan à Serres, Luc/Aude, Festes, Rouvenac, ...ainsi que plusieurs ateliers organisés par des acteurs-initiateurs locaux intéressés à creuser ce sujet, ont montré une convergence d'idées et des pistes de propositions qui exprimaient :

			<ul class="niv1">
				<li><b>Une perception commune</b> que la co-construction d’événements ainsi que leurs efforts de publication pour promouvoir leurs activités était sacrément chronophages et énergivores avec parfois au final des ressentis de faible efficacité et de gaspillage, tous deux créateurs de frustration.</li>

				<li><b>Le souhait de renforcer</b> les liens entre les nouveaux ''arrivants''  sur ce territoire cosmopolite et les habitants autochtones aux structures plus institutionnelles.</li>

				<li>S'est aussi <b>affichée une volonté</b> d'orientation ''éthique'' respectant les valeurs d'une démarche solidaire et écologique dans un cadre d'un développement local de transition.</li>
			</ul>

			<br/>

			Pour donner une autre dimension à leurs actions, il s'avérait ainsi nécessaire de faciliter leur mise en réseau par la mutualisation de leurs différents canaux d'informations et donc de <b>créer un outil de communication collaboratif dans la contribution et la diffusion.</b>
			
			<br/>  <br/>
			En se réunissant plus fréquemment depuis le début d'année 2018, un collectif composé d'une quinzaine d'acteurs locaux s'est alors attelé à élaborer <b>un portail numérique</b> convivial, facile, rapide et actualisé.
			<br/>  <br/>

			Il voit ainsi le jour en 2019 !
		</div>
		<div id="status" class="tab-pane row col-xs-10 col-xs-offset-1">

<b>Article 1er : déclaration</b> <br/><br/>

Il est fondé entre les adhérents aux présents statuts une association collégiale régie par la loi du 1er juillet 1901 et le décret du 16 août 1901 dite 
« Réseau des Initiatives de la Haute Vallée de l'Aude ».<br/><br/>

<b>Article 2 : objets</b><br/><br/>

Cette association a pour objet de faciliter la mise en réseau des acteurs porteurs d'initiatives en haute vallée de l'Aude.<br/>
Par ailleurs, l’association inscrit son projet dans une dimension d’intérêt général en s’ouvrant à tous les publics, notamment les plus fragiles. En toutes circonstances, l’association garantit un fonctionnement démocratique, transparent et assure le caractère désintéressé de sa gestion.<br/><br/>

<b>Article 3 : siège social</b><br/><br/>

Le siège social de l’association est fixé au 1 chemin de Camières, 11260 Rouvenac.<br/>
Il pourra être transféré par simple décision du conseil d’administration ; la ratification par l’assemblée générale sera nécessaire.<br/><br/>

<b>Article 4 : durée</b><br/><br/>

La durée de l’association est illimitée.<br/><br/>

<b>Article 5 : membres</b><br/><br/>

L’association se compose de membres actifs, personnes physiques ou morales.
- conditions d’admission 
Pour faire partie de l’association, il faut être agréé par le conseil d’administration qui statue, lors de ses réunions, sur les demandes d’admission.
- qualités requises
Sont membres actifs les personnes physiques ou morales qui sont à jour de leur cotisation  annuelle fixée lors de la dernière assemblée générale.
La qualité de membre se perd par :
<ul>
	<li>la démission</li>
	<li>le décès</li>
	<li>la radiation prononcée par le conseil d’administration pour non-paiement de la cotisation ou pour motif grave (ex : non respect des objets, du règlement intérieur, de la charte,  non réponse à la convocation,...).</li>
</ul>


<br/><br/>

							
<b>Article 6 : ressources</b><br/><br/>

Les ressources de l’association comprennent :
<ul>
	<li>le produit des cotisations, dont le montant est fixé chaque année par le conseil d’administration </li>
	<li>les subventions de l’état, des régions, des départements, des communes, des communautés de communes, des établissements publics, de l'Europe.</li>
	<li>du produit de manifestations, des intérêts des biens et valeurs qu’elle pourrait posséder ainsi que des rétributions pour services rendus,</li>
	<li>des toutes autres ressources ou subventions qui ne seraient pas contraires aux lois en vigueur.</li>
</ul>


<br/><br/>

<b>Article 7 : conseil d’administration </b><br/><br/>

La direction de l’association est assurée par un conseil d’administration collégial composé d'au moins cinq membres.
Les membres sont élus pour une année par l’assemblée générale.
Les membres du conseil d’administration collégial sont rééligibles. 
En cas de vacances, de démission, d’exclusion ou de décès, le conseil d’administration pourvoit provisoirement au remplacement de ses membres. Il est procédé à leur remplacement définitif lors de l’assemblée générale suivante. Les pouvoirs des membres ainsi élus prennent fin à l’époque où devrait normalement expirer le mandat des membres remplacés.
Nul ne peut faire partie du conseil s’il n’est pas majeur.<br/><br/>

Le conseil d'administration assure la conduite collective des objectifs et des projets de l’association, et participe à la mise en place des orientations et actions prévues par l’assemblée générale.
Il est investi des pouvoirs nécessaires au fonctionnement de l’association, et peut ainsi agir en toutes circonstances en son nom, notamment sur le plan légal.<br/><br/>

Le conseil d’administration collégial est l’organe qui représente légalement l’association en justice. En cas de poursuites judiciaires, les membres du conseil d’administration collégial en place au moment des faits prendront collectivement et solidairement leurs responsabilités devant les tribunaux compétents. Aucun membre de l’association n’est personnellement responsable des engagements contractés par elle. <br/><br/>

Un ou plusieurs des membres du conseil d’administration sont désignés pour représenter l’association dans tous les autres actes de la vie civile et dans ses rapports avec les médias et l’administration. Ils peuvent ainsi être habilités à remplir, au cours d’une période déterminée, toutes les formalités de déclaration et de publication prescrites par la législation et tout autre acte administratif nécessaire au fonctionnement de l’association.<br/><br/>

Le conseil se réunit au moins une fois tous les six mois ou à la demande d’un tiers au moins de ses membres. 
Tout membre du conseil qui, sans excuse valable, n’aura pas assisté à trois réunions consécutives pourra être considéré comme démissionnaire.<br/><br/>


<b>Article 8 : prise de décisions</b><br/><br/>

L’association et ses organes décisionnels s’efforceront de prendre leurs décisions par consentement des membres présents dans l’objectif d’inclure l’opinion de chacun(e), la participation de tous sans pour autant l’imposer. Le consentement est atteint lorsqu’une décision ne rencontre pas ou plus d'objection.<br/><br/>

<b>Article 9 : assemblée générale ordinaire</b><br/><br/>

L’assemblée générale ordinaire comprend tous les membres de l’association à jour de leur cotisation annuelle. <br/>L’assemblée générale ordinaire se réunit une fois par an.<br/>
Les membres sont convoqués quinze jours au moins avant la date fixée. L’ordre du jour est indiqué sur les convocations.<br/><br/>

L’assemblé générale ordinaire présente aux membres:
<ul>
	<li>le rapport moral et d’activités</li>
	<li>le rapport financier</li>
	<li>les perspectives et le budget prévisionnel</li>
</ul>

Elle demande l'approbation des comptes. 
Il est procédé à l'élection des membres du nouveau conseil d'administration.<br/><br/>

Les résolutions de l’assemblée générale ordinaire sont prises au consentement des membres  présents. En cas d'impossibilité de résolutions, le conseil d'administration convoque une assemblée générale extraordinaire.<br/><br/>

<b>Article 10 : assemblée générale extraordinaire</b><br/><br/>

Une assemblée générale extraordinaire peut être convoquée sur décision du conseil d’administration ou sur la demande d’au moins un tiers des membres de l'association.
Les membres sont convoqués quinze jours au moins avant la date fixée. L’ordre du jour est indiqué sur les convocations.
Les résolutions de l’assemblée générale extraordinaire sont prises au consentement ou à défaut à la majorité absolue des membres présents.<br/><br/>

<b>Article 11 : règlement intérieur</b><br/><br/>

Un règlement intérieur peut être établi par le conseil d’administration qui le fait alors approuver par l’assemblée générale.<br/><br/>

<b>Article 12 : dissolution</b><br/><br/>

La dissolution est prononcée à la demande du conseil d’administration par une assemblée générale extraordinaire convoquée spécialement à cet effet dans les conditions prévues à l’art. 10 des statuts. 
En cas de dissolution, l’assemblée générale extraordinaire désigne un ou deux liquidateurs qui seront chargés de la liquidation des biens de l’association conformément à l’art. 9 de la loi du 1er juillet 1901 et à l’art. 15 du décret du 16 août 1901.<br/><br/>


Statuts créés et votés lors de l’assemblée générale constitutive du 14 août 2018 à Rouvenac<br/><br/>
		</div>
		<div id="rihva" class="tab-pane row col-xs-10 col-xs-offset-1">
			<b>Adhérer à l'association "Réseau des Initiatives de la Haute Vallée de l'Aude:</b>
			<ul>
				<li>C'est soutenir ses projets comme la Tambouille des Initiatives, le Festi-Forum, le site Portail HVA, la fête des Plantes ...</li>
				<li>C'est accepter de recevoir des informations sur les besoins en bénévoles de l'association et de ses structures adhérentes.</li>
			<ul>
			<br/><br/>
			<div class="col-xs-12 text-center">
				<a href="https://www.communecter.org/upload/communecter/organizations/5b713de540bb4eb87a808363/file/qui-sommes-nous-4-soutenir-RIHVA.pdf" target="_blanc" class="btn btn-default">Formulaire</a>
			</div>
			<br/><br/>
			
		</div>
	</div>
</div>
