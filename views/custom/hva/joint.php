<div class=" col-xs-10 col-xs-offset-1 padding-10">
	<span>
		Vous souhaitez rendre <b>votre activité, vos initiatives plus visibles et plus lisibles</b> et ainsi de renforcer votre ancage sur les deux communautés de communes du Limouxin et des Pyrénées Audoises.<br/> <br/>

		Ce site vous offre un <b>mode de communication plus rapide et plus efficace</b> entre vous acteurs porteurs d'initiatives (contributeurs du site) et tous visiteurs du site.<br/> <br/>

		Il procure à chacun un <b>regard plus précis, plus exhaustif et plus pertinent</b> sur toutes les activités de la HVA dans le but d'un mieux vivre ensemble.<br/> <br/>

		Il permet aussi des <b>mises en relation plus efficientes entre vous</b> les acteurs porteurs d'initiatives.<br/> <br/>

		Ce site est construit autour d'une <b>charte</b> qui rassemble certaines valeurs afin de promouvoir une information indépendante, cohérente et de confiance. <br/>
		Cette charte est consultable en introduction du <b>formulaire de renseignements</b> et est <b>soumise à votre acceptation.</b><br/> <br/>

		Ce <b>formulaire de renseignements</b> servira a rédiger votre profil dans le répertoire des acteurs et doit donc être <b>le plus précis et complet possible.</b><br/> <br/>

		Après <b>validation de votre inscription</b> par notre équipe, vous recevrez votre <b>mot de passe</b> qui vous permettra de remplir l'agenda, le répertoire et les annonces vous concernant.<br/> <br/>

		Ce portail <b>existe et n'est pérenne</b> que grâce à vos <b>contributions rédactionnelles mais aussi financières</b> dont <b>le montant minimum annuel est de 20€.</b><br/> <br/>
	</span>
	<span class="text-center">
		
		<!-- <h1><a href="https://docs.google.com/forms/d/e/1FAIpQLSc8yce4HQQJHflDViNR_NDdlK9t7n3qoD2EjTQKWqNQLzcx8A/viewform?c=0&w=1&usp=mail_form_link" target="_blanc">S'inscrire</a> <h1> <br/> -->

		

		<?php
		
		$elt = Slug::getElementBySlug("hva", array("_id", "name") );
		?>

		<button id="connectHVA" class="letter-green font-montserrat btn-menu-connect margin-left-10 margin-right-10 menu-btn-top" data-toggle="modal" data-target="#modalLogin" style="font-size: 17px; background-color: #5b2649 !important; color: white !important; padding: 8px 15px !important;">
                <i class="fa fa-sign-in"></i> 
                <span class="hidden-xs"><small style="width:70%;">SE CONNECTER</small></span>
        </button>
		<!-- <a id="connectUserHVA" href="#" data-isco="false" data-id="<?php echo $elt['id'] ; ?>" class="customBtnFull customTabTrigger projectBgColor projectBgColorHover hidden btn btn-default bg-green">Devenir membre</a>

		<a id="connectOrgaHVA" href="javascript:;" data-form-type="inscription" class="addBtnFoot btn-open-form btn btn-default  bg-blue margin-bottom-10 hidden"><i class="fa fa-newspaper-o "></i> <span>Inscrit ton organisation</span></a>
		<span id="connectMsgHVA" class="hidden">En attente de validation</span> -->
	</span>
</div>
<script type="text/javascript">
	
$(document).ready(function() {
	bindButtonOpenForm();
	$('#connectUserHVA').click(function(){
		var id = $(this).data("id");
		var isco = $(this).data("isco");
		if(isco == false){
			links.connectAjax('organizations',id,userId,'citoyens','contributors', null, function(){
				$('#connectUserHVA').html("Désinscrire");
				$('#connectUserHVA').data("isco", true);
				$("#connectUserHVA").addClass("hidden");
				$("#connectOrgaHVA").addClass("hidden");
				$("#connectMsgHVA").removeClass("hidden");
			});
		}else{
			links.disconnectAjax('organizations',id,userId,'citoyens','contributors', null, function(){
				$('#connectUserHVA').html("Devenir membre");
				$('#connectUserHVA').data("isco", false);
				$("#connectUserHVA").removeClass("hidden");
				$("#connectOrgaHVA").addClass("hidden");
				$("#connectMsgHVA").addClass("hidden");
			});
		}
		
	});
	if(typeof userId != "undefined" && userId != null && userId != ""){
		$("#connectHVA").addClass("hidden");
		// if(costum.isMember === true){
		// 	$("#connectOrgaHVA").removeClass("hidden");
		// 	$("#connectUserHVA").addClass("hidden");
		// 	$("#connectMsgHVA").addClass("hidden");
		// }else{
		// 	$("#connectUserHVA").removeClass("hidden");
		// 	$("#connectOrgaHVA").addClass("hidden");
		// 	$("#connectMsgHVA").addClass("hidden");
		// }
	}
});
</script>