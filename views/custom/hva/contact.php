<?php 
$layoutPath = 'webroot.themes.'.Yii::app()->theme->name.'.views.layouts.';
$this->renderPartial($layoutPath.'forms.'.Yii::app()->params["CO2DomainName"].'.formContact'); ?>
<div class=" col-xs-10 col-xs-offset-1 padding-10">
	
	<h3>Contact</h3>
	<a href="javascript:;" class="tooltips btn btn-default btn-sm openFormContact" 
	data-id-receiver="" 
	data-email=""
	data-name="">
	<i class="fa fa-envelope"></i> Envoyer un e-mail</a>
</div>

<script type="text/javascript">
	
$(document).ready(function() {

	$(".openFormContact").click(function(){
		mylog.log("openFormContact");
		// var idReceiver = $(this).data("id-receiver");
		// var idReceiverParent = contextData.id;
		// var typeReceiverParent = contextData.type;
		// var contactMail = $(this).data("email");
		// var contactName = $(this).data("name");
		// //mylog.log('contactMail', contactMail);
		$("#formContact .contact-email").html(costum.admin.email);
		// $("#formContact #contact-name").html(contactName);

		//$("#formContact #emailSender").val(costum.admin.email);
		// $("#formContact #name").val(userConnected.name);
		
		// $("#formContact #form-control").val("");
		
		// $("#formContact #idReceiver").val(idReceiver);
		// $("#formContact #idReceiverParent").val(idReceiverParent);
		// $("#formContact #typeReceiverParent").val(typeReceiverParent);
		
		$("#conf-fail-mail, #conf-send-mail, #form-fail").addClass("hidden");
		$("#form-group-contact").removeClass("hidden");
		$("#formContact").modal("show");
	});
});
</script>