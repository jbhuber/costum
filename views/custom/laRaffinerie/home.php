<?php

$allProjectsCount = 0;
$allMembersCount = 0;
$projectList = [];
$eventsList = [];
$newsList = [];
$docsList = [];
$toolsList = [];
$allLinksList = [];
$colorCount = 0;

// LEVEL 1
$type = Project::COLLECTION;
$elt = PHDB::findOne($type,array("slug" => "laRaffinerie"));
$idElt = (String)$elt["_id"] ;
$linksElt = Element::getAllLinks($elt["links"], $type, $idElt);
$allLinksList = array_merge($allLinksList,$linksElt);
array_push($projectList, $idElt);

$hasRC = ( @$elt["hasRC"] ) ? "true" : "false" ;
$canEdit = "false";
$loadChat = StringHelper::strip_quotes($elt["name"]);
$edit = Authorisation::canEditItem(Yii::app()->session["userId"], $type, $idElt);
$thumbAuthor =  @$element['profilThumbImageUrl'] ? 
		                      Yii::app()->createUrl('/'.@$elt['profilThumbImageUrl']) 
		                      : $this->module->assetsUrl.'/images/thumbnail-default.jpg';

$openEdition = Authorisation::isOpenEdition($idElt, $type, @$elt["preferences"]);
$iconColor = Element::getColorIcon($type) ? Element::getColorIcon($type) : "";
$params = array(  "element" => @$elt, 
	                "type" => @$type, 
	                "edit" => @$edit,
	                "thumbAuthor"=>@$thumbAuthor,
	                "openEdition" => $openEdition,
	                "iconColor" => $iconColor
	            );

	$this->renderPartial('dda.views.co.pod.modals', $params );
/**
/* TEMPLATE LA RAFFINERIE
/*
/* */

// ADDITIONAL FILES

	// THEME

	$cssAndScriptFilesTheme = array(
		'/plugins/moment/min/moment.min.js' ,
        '/plugins/moment/min/moment-with-locales.min.js',
		// SHOWDOWN
		'/plugins/showdown/showdown.min.js',
		// MARKDOWN
		'/plugins/to-markdown/to-markdown.js',
		'/plugins/fullcalendar/fullcalendar/fullcalendar.min.js',
		'/plugins/fullcalendar/fullcalendar/fullcalendar.css', 
		'/plugins/fullcalendar/fullcalendar/locale/'.Yii::app()->language.'.js',

		//'/plugins/jquery.dynForm.js',
	);
	HtmlHelper::registerCssAndScriptsFiles($cssAndScriptFilesTheme, Yii::app()->request->baseUrl);


	// MODULES

	$cssAndScriptFilesModule = array(
		'/js/default/calendar.js',
		'/js/links.js',
		//'/js/gallery/index.js',
		'/js/default/profilSocial.js'
	);
	HtmlHelper::registerCssAndScriptsFiles($cssAndScriptFilesModule, Yii::app()->getModule( "co2" )->getAssetsUrl());
	HtmlHelper::registerCssAndScriptsFiles( array('/css/calendar.css'), Yii::app()->theme->baseUrl. '/assets');


	// CUSTOM CSS
	
	HtmlHelper::registerCssAndScriptsFiles(array('/css/laRaffinerie/laRaffinerie.css'), Yii::app()->getModule( "costum" )->getAssetsUrl());

	 

// CUSTOM VARS

	// ELEMENT

	// $type = Organization::COLLECTION;
	// $elt = PHDB::findOne($type,array("slug" => "laRaffinerie"));
	// $idElt = (String)$elt["_id"] ;
	// $linksElt = Element::getAllLinks($elt["links"], $type, $idElt);
	// $icon = Element::getFaIcon($type) ? Element::getFaIcon($type) : "";
	// $iconColor = Element::getColorIcon($type) ? Element::getColorIcon($type) : "";
	$edit = false;


	// COLORS

	$bgColor1 = "#01809b";
	$bgColorHover1 = "#01607b";

	$projectColorDefault = "rgba(239,42,0, 1)";
	$projectColor = [
		'rgba(140,87,79, 1)',
		'rgba(218,107,15, 1)',
		'rgba(203,6,13,1)',
		'rgba(216,20,104,1)',
		'rgba(100,40,114, 1)',
		'rgba(1,75,120,1)',
		'rgba(60,141,158,1)',
		'rgba(130,157,40,1)',
		'rgba(1,121,96,1)',
		'rgba(231,184,36,1)'
	]

?>

<!-- CUSTOM STYLE -->

	<style>
		.customBtnTrigger{
			background-color: <?php echo $bgColor1 ?>;
	  }
	  .customBtnTrigger:hover{
			background-color: <?php echo $bgColorHover1 ?>;
	  }
	  .projectNavFirstLvl > li a:hover::after{
			background-color: <?php echo $projectColorDefault ?>;
		}
		.projectNavSecondLvl .projectNavThirdLvl a:hover::after{
			background-color: <?php echo $projectColorDefault ?>;
		}
		.projectNavSecondLvl >  li a:hover::after{
			background-color: <?php echo str_replace('1)','.82)',$projectColorDefault) ?>;
		}
		<?php 
			$i=1;
			foreach ($projectColor as $key => $value) {
				echo "
					.projectNavSecondLvl .projectNavThirdLvl:nth-of-type(".$i.") a:hover::after{
						background-color: ".str_replace('1)','.82)',$value).";
					}
					.projectNavSecondLvl >  li:nth-of-type(".$i.") a:hover::after{
						background-color: ".$value.";
					}
				";
				$i++;
			}
		?>
	</style>


<!-- HTML START -->

	<div class="project">
		<a href="" class="projectNavTriggerMobile"><i class="fa fa-bars tooltips"></i></a>
		<div class="projectNav">
			<div class="projectNavContent">
				<button class="toggleAllProjects">Ouvrir / Fermer sous-projets</button>
				<?php

					function getCountMembers($el){
						return count(@$el['links']['members']);
					}
					function getCountEvents($el){
						return count(@$el['links']['events']);
					}
					function getCountProjects($el){
						return count(@$el['links']['projects']);
					}
					function getAllEvents($el){
						$arr = [];
						if(@$el['links']['events']){
							foreach ($el['links']['events'] as $key => $value) {
								array_push($arr, $key);
							}
						}
						return $arr;
					}

					

					if(!empty($elt) && !empty($elt["links"])){

						echo "
							<ul class='projectNavFirstLvl'>
								<li>
									<a href='javascript:;' class='linkMenu linkOrganization' 
										data-key='".$idElt."' 
										data-col='".$type."'
										data-countmembers='".getCountMembers($elt)."'
										data-countevents='".getCountEvents($elt)."'
										data-countprojects='".getCountProjects($elt)."'
										data-color='".$projectColorDefault."';
									>".$elt["name"]."</a>
								</li>";
						
						$eventsList = array_merge($eventsList, getAllEvents($elt));
						array_push($projectList, $idElt);

						// LEVEL 2

						if(!empty($elt["links"]["projects"])){

							echo "<ul class='projectNavSecondLvl'>";	

							foreach ($elt["links"]["projects"] as $idElt2 => $valElt2) {

								$elt2 = Element::getElementById($idElt2, "projects");
								if(!empty($elt2)){
									$linksElt2 = Element::getAllLinks($elt2["links"], $type, $idElt);
									$allLinksList = array_merge($allLinksList,$linksElt2);
									array_push($projectList, $idElt2);
									$allProjectsCount++;
									$eventsList = array_merge($eventsList, getAllEvents($elt2));
									$private = false ;
									if(	isset($elt2["preferences"]) && 
										isset($elt2["preferences"]["private"]) &&
										$elt2["preferences"]["private"] == true ){
										$private = true ;
										if( !empty(Yii::app()->session["userId"]) &&
											!empty($elt2["links"]) && 
											!empty($elt2["links"]["contributors"]) &&
											!empty($elt2["links"]["contributors"][Yii::app()->session["userId"]]) &&
											empty($elt2["links"]["contributors"][Yii::app()->session["userId"]]["toBeValidated"]) ) {
											$private = false ;
										}
									}
									//echo $private ;
									if($private === false){
										echo "
											<li>
												<i class='toggleProjects fa fa-plus' aria-hidden='true'></i>
												<a href='javascript:;' class='linkMenu projectBgColorAfterHover projectBgColorAfterActive' 
													data-key='".$idElt2."'
													data-slug='".$elt2["slug"]."'
													data-col='".Project::CONTROLLER."' 
													data-img='".Yii::app()->createUrl(@$elt2['profilMediumImageUrl'])."'
													data-countmembers='".getCountMembers($elt2)."'
													data-countevents='".getCountEvents($elt2)."'
													data-countprojects='".getCountProjects($elt2)."'
													data-color='".$projectColor[$colorCount]."'
												>".$elt2["name"]."</a></li>";

										// LEVEL 3

										if(!empty($elt2["links"]["projects"])){

											echo "<ul class='projectNavThirdLvl'>";

											foreach ($elt2["links"]["projects"] as $idElt3 => $valElt3) {

												$elt3 = Element::getElementById($idElt3, "projects");
												$linksElt3 = Element::getAllLinks($elt3["links"], $type, $idElt);
												$allLinksList = array_merge($allLinksList,$linksElt3);
												array_push($projectList, $idElt3);
												$allProjectsCount++;
												$eventsList = array_merge($eventsList, getAllEvents($elt3));

												echo "
													<li>
														<a href='javascript:;' class='linkMenu' 
															data-key='".$idElt3."' 
															data-slug='".$elt3["slug"]."'
															data-col='".Project::CONTROLLER."' 
															data-img='".Yii::app()->createUrl(@$elt3['profilMediumImageUrl'])."'
															data-countmembers='".getCountMembers($elt3)."'
															data-countevents='".getCountEvents($elt3)."'
															data-countprojects='".getCountProjects($elt3)."'
															data-color='".str_replace('1)','.82)',$projectColor[$colorCount])."'
														>".$elt3["name"]."</a></li>";
											}
											echo "</ul>"; $colorCount++;
										}
									}
								}
								
							}
							echo "</ul>"; 
						}
						echo "</ul>";
					}
				?>
			</div>
		</div>

		<?php 

			function getProfilBannerUrl($el){
				if ( !@$el["profilBannerUrl"] || (@$el["profilBannerUrl"] && empty($el["profilBannerUrl"]))){
					return Yii::app()->theme->baseUrl. '/assets/img/background-onepage/connexion-lines.jpg';
				}
				else{
					return Yii::app()->createUrl($el["profilBannerUrl"]);
				}
			}
			function getProfileThumbUrl($el){
				if ( !@$el["profilMediumImageUrl"] || (@$el["profilMediumImageUrl"] && empty($el["profilMediumImageUrl"]))){
					return Yii::app()->theme->baseUrl.'/assets/img/background-onepage/connexion-lines.jpg';
				}
				else{
					return Yii::app()->createUrl($el["profilMediumImageUrl"]);
				}
			}
			function getDescription($el){
				if ( !@$el["description"] || (@$el["description"] && empty($el["description"]))){
					return false;
				}
				else{
					return $el["description"];
				}
			}
			function getShortDescription($el){
				if ( !@$el["shortDescription"] || (@$el["shortDescription"] && empty($el["shortDescription"]))){
					return false;
				}
				else{
					return $el["shortDescription"];
				}
			}

			$bannerImgUrl 		= getProfilBannerUrl($elt);
			$thumbImgUrl 			= getProfileThumbUrl($elt);
			$shortDescription = getShortDescription($elt);


			$nbInscrits = (!empty(@$elt['links']['members']) ? count(@$elt['links']['members']) : 0);
				
		?>

		<div class="projectMask">
			<div class="projectMaskContent">
				<div class="projectMaskCell">
					<img src="http://bestanimations.com/Science/Gears/loadinggears/loading-gears-animation-10.gif" alt="">
				</div>
			</div>
		</div>
		<div class="projectWrapper">
			<div class="projectHeader">
				<a href="" class="projectAdmin" target="_blank"><i class="fa tooltips fa-cog" data-original-title="" title=""></i></a>
				
				<p class="projectShortDescription"><?php echo $shortDescription; ?></p>
				<div class="projectBanner" style="background-image: url(<?php //echo $bannerImgUrl; ?>)"></div>
				<div class="projectThumb"  style="background-image: url(<?php //echo $thumbImgUrl; ?>)"></div>
				<div class="projectHeaderOptions">
					<div class="row">
						<div class="col-xs-12 col-md-6 col-md-push-6 projectHeaderOptionsCont1">
							<div class="col col-xs-4">
								<a href="https://chat.communecter.org/channel/laRaffinerie" target="_blank" class="customBtnFull projectBgColor projectBgColorHover" id="btnChatRaffinerie"><?php echo Yii::t("cooperation", "Chat");?></a>
							</div>
							<div class="col col-xs-4">
								<a id="follows" href="#" data-isco="false" data-id="<?php echo $idElt ; ?>" class="customBtnFull customTabTrigger projectBgColor projectBgColorHover">S'inscrire</a>
							</div>
							<div class="col col-xs-4">
								<a id="outils2" href="#" data-id="<?php echo $idElt ; ?>" class="customBtnFull customTabTrigger projectBgColor projectBgColorHover">Outils</a>
							</div>
						</div>
						<div class="col-xs-12 col-md-6 col-md-pull-6 projectHeaderOptionsCont2">
							<div class="col col-xs-4 customTabTriggerDescr"><a href="#projectDescription" class="customTabTrigger customBtnLine">À propos</a></div>
							<div class="col col-xs-4 customTabTriggerProj" ><a href="#projectChildren" class="customTabTrigger customBtnLine">Projets</a></div>
							<div class="col col-xs-4 customTabTriggerContacts"><a href="#projectContacts" class="customTabTrigger customBtnLine">Contacts</a></div>
						</div>
						<!-- <div class="col-sm-3"><span class="projectNbMembers"></span> Inscrits</div>
						<div class="col-sm-3"><span class="projectNbEvents"></span> Évenements</div>
						<div class="col-sm-3"><span class="projectNbProjects"><?php echo $allProjectsCount ?></span> Projets</div> -->
					</div>
				</div>
			</div>

			<?php 
				//var_dump($linksElt);
			?>
			
			<?php $description = getDescription($elt);?>
			<div id="descriptionMarkdown" class="hidden"><?php echo (empty($description) ? "" : $description ); ?></div>
			<div id="projectDescription" class="customBlock customTab">
				<button class="closeCustomTab">X</button>
				<div id="descriptionAbout"></div>
				<button class="customBtnTrigger">Lire la suite <i class="fa fa-angle-down"></i></button>
			</div>

			<div id="projectChildren" class="customBlock customTab">
				<button class="closeCustomTab">X</button>
				<div class="row"></div>
			</div>

			<div id="projectContacts" class="customBlock customTab">
				<button class="closeCustomTab">X</button>
				<div class="row"></div>
			</div>

			<div class="projectInfos customBlock">

				<div class="projectInfosHeader">
					<ul class="row nav nav-tabs">
						<li class="nav-tab col-xs-4 active projectBgColorActive projectBgColorHover"><a data-toggle="tab" href="#journal">Journal</a></li>
						<li class="nav-tab col-xs-4 projectBgColorActive projectBgColorHover"><a data-toggle="tab" href="#agenda">Agenda</a></li>
						<li class="nav-tab col-xs-4 projectBgColorActive projectBgColorHover"><a data-toggle="tab" href="#documents">Fichiers</a></li>
					</div>
				</ul>
				
				<style>
					.customBlockImg{
						padding-bottom: 50%;
						background-position: center;
						background-size: cover;
					}
				</style>

				<div class="projectInfosContent tab-content">
					<div id="journal" class="tab-pane active row">
						<div id="journalTimeline"></div>
					</div>
					<div id="agenda" class="tab-pane row">
						<!-- <div class='col-xs-12 margin-bottom-10'>
							<a href='javascript:;' id='showHideCalendar' class='text-azure' data-hidden='0'><i class='fa fa-caret-up'></i> Hide calendar</a>
						</div> -->
						<div id='profil-content-calendar' class='col-xs-12 margin-bottom-20'></div>
						<div id='list-calendar' class='col-xs-12 margin-bottom-20'></div>
					</div>
					<div id="documents" class="tab-pane row">
						<!-- <div id="GridAlbums"></div> -->
					</div>
				</div>

			</div>
		</div>
	</div>

	<script type="text/javascript">
		//urlCtrl.loadByHash = false;
		var subView="<?php echo @$_GET['idLaR']; ?>";
		var elt = <?php echo json_encode(@$elt); ?> ;
		var type = <?php echo json_encode(@$type); ?> ;
		var idElt = <?php echo json_encode(@$idElt); ?> ;
		var events = <?php echo json_encode(@$events); ?> ;
		var baseUrl = `<?php echo Yii::app()->createUrl("") ?>` ;
		var defaultBannerUrl = ` <?php echo Yii::app()->theme->baseUrl. '/assets/img/background-onepage/connexion-lines.jpg';?> `;


		var itemType = type;
		var itemId = idElt;
		var docType = "file";
		var folderId = "";
		var contentKey = null;

		var initEvent = false;
		var initDoc = false;
		var initNews = false;

		var hashUrlPage = "#home";


		//var links = <?php //echo json_encode(@$linksElt); ?> ;
	</script>

	<?php 
		// CUSTOM JS
	HtmlHelper::registerCssAndScriptsFiles(array('/js/laRaffinerie/laRaffinerie.js'), Yii::app()->getModule( "costum" )->getAssetsUrl());
	?>

	<!-- 
		TODO
			- [ RAPHA ] Contextualisation agenda et news
			- [ RAPHA ] Docs et Outils ( Galerie et Espace Co)
			- [ Alex ] Inscription projet
			- [ RAPHA ] Requete ajax pas acceptée quand user hors ligne
 	-->