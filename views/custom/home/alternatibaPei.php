
<div class="pageContent">


<style type="text/css">
  #customHeader{
    margin-top: 0px;
  }
  #costumBanner{
   /* max-height: 375px; */
  }
  #costumBanner h1{
    position: absolute;
    color: white;
    background-color: rgba(0,0,0,0.4);
    font-size: 29px;
    bottom: 0px;
    padding: 20px;
  }
  #costumBanner h1 span{
    color: #eeeeee;
    font-style: italic;
  }
  #costumBanner img{
    min-width: 100%;
  }
  .btn-main-menu{
    background: #1b7baf;
    border-radius: 20px;
    padding: 20px !important;
    color: white;
    cursor: pointer;
    border:3px solid transparent;
    /*min-height:100px;*/
  }
  .btn-main-menu:hover{
    border:2px solid #1b7baf;
    background-color: white;
    color: #1b7baf;
  }
  .ourvalues img{
    height:70px;
  }
  .main-title{
    color: #487614;
  }

  .ourvalues h3{
    font-size: 36px;
  }
  .box-register label.letter-black{
    margin-bottom:3px;
    font-size: 13px;
  }
  .bullet-point{
      width: 5px;
    height: 5px;
    display: -webkit-inline-box;
    border-radius: 100%;
    background-color: #fbae55;
  }
  .text-explain{
    color: #555;
    font-size: 18px;
  }
  .blue-bg {
  background-color: white;
  color: #5b2549;
  height: 100%;
  padding-bottom: 20px !important;
}

.circle {
  font-weight: bold;
  padding: 15px 20px;
  border-radius: 50%;
  background-color: #fea621;
  color: white;
  max-height: 50px;
  z-index: 2;
}
.circle.active{
      background: #ea4335;
    border: inset 3px #ea4335;
    max-height: 70px;
    height: 70px;
    font-size: 25px;
    width: 70px;
}
.support-section{
  background-color: white;
}
.support-section h2{
  text-align: center;
    padding: 60px 0px !important;
    background: #450e33;
    font-size: 40px;
    color: white;
    margin-bottom: 20px;
}
.timeline-ctc h2{
 text-align: center;
    padding: 105px 0px 60px 0px !important;
    background: #450e33;
    font-size: 40px;
    color: white;
    margin-bottom: 20px;
}
.how-it-works.row {
  display: flex;
}
.row.timeline{
  display: flex;
  -ms-flex-wrap: wrap;
  flex-wrap: wrap;
  margin-right: -15px;
  margin-left: -15px;
}
.how-it-works.row .col-2 {
  display: inline-flex;
  align-self: stretch;
  position: relative;
  align-items: center;
  justify-content: center;
}
.how-it-works.row .col-2::after {
  content: "";
  position: absolute;
  border-left: 3px solid #0091c6;
  z-index: 1;
}
.pb-3, .py-3 {
    padding-bottom: 1rem !important;
}
.pt-2, .py-2 {
    padding-top: 0.5rem !important;
}
.how-it-works.row .col-2.bottom::after {
  height: 50%;
  left: 50%;
  top: 50%;
}
.how-it-works.row.justify-content-end .col-2.full::after {
  height: 100%;
  left: calc(50% - 3px);
}
.how-it-works.row .col-2.full::after {
    height: 100%;
    left: calc(50% - 0px);
}
.how-it-works.row .col-2.top::after {
  height: 50%;
  left: 50%;
  top: 0;
}

.timeline div {
  padding: 0;
  height: 40px;
}
.timeline hr {
  border-top: 3px solid #0091c6;
  margin: 0;
  top: 17px;
  position: relative;
}
.timeline .col-2 {
  display: flex;
  overflow: hidden;
  flex: 0 0 16.666667%;
    max-width: 16.666667%;
}
.align-items-center {
    -ms-flex-align: center !important;
    align-items: center !important;
}
.justify-content-end {
    -ms-flex-pack: end !important;
    justify-content: flex-end !important;
}
.row {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: -15px;
    margin-left: -15px;
}
.how-it-works.row .col-6 p{
  color: #444;
}
.how-it-works.row .col-6 h5{
font-size: 17px;
    text-transform: inherit;
}
.col-2 {
    -ms-flex: 0 0 16.666667%;
    flex: 0 0 16.666667%;
    max-width: 16.666667%;
}
.col-6 {
    -ms-flex: 0 0 50%;
    flex: 0 0 50%;
    max-width: 50%;
}
.timeline .col-8 {  
    flex: 0 0 66.666667%;
    max-width: 66.666667%;
}
.timeline .corner {
  border: 3px solid #0091c6;
  width: 100%;
  position: relative;
  border-radius: 15px;
}
.timeline .top-right {
  left: 50%;
  top: -50%;
}
.timeline .left-bottom {
  left: -50%;
  top: calc(50% - 3px);
}
.timeline .top-left {
  left: -50%;
  top: -50%;
}
.timeline .right-bottom {
  left: 50%;
  top: calc(50% - 3px);
}

  @media screen and (min-width: 450px) and (max-width: 1024px) {
    .logoDescription{
      width: 60%;
      margin:auto;
    }
  }

  @media (max-width: 1024px){
    #customHeader{
      margin-top: -1px;
    }
  }
  @media (max-width: 768px){

  }
</style>

<div class="col-xs-12 no-padding" id="customHeader" style="background-color: white">
  <div id="costumBanner" class="col-xs-12 col-sm-12 col-md-12 no-padding">
 <!--  <h1>L'entraide<br/><span class="small">Une interface numérique pour échanger</span></h1>-->
    <img class="img-responsive" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/alternatibaPei/home.jpg'> 
  </div>
  <!--<div class="col-md-12 col-lg-12 col-sm-12 imageSection no-padding" 
     style=" position:relative;">-->
 </div>

<script type="text/javascript">
  jQuery(document).ready(function() {
        setTitle("Alternatiba Péi");
  });
</script>


