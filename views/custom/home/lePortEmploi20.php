
<div class="pageContent">


<style type="text/css">
  #costumBanner{
    max-height: 375px;
  }
  #costumBanner h1{
    position: absolute;
    color: white;
    background-color: rgba(0,0,0,0.4);
    font-size: 29px;
    bottom: 0px;
    padding: 20px;
  }
  #costumBanner h1 span{
    color: #eeeeee;
    font-style: italic;
  }
  #costumBanner img{
    min-width: 100%;
  }
  @media screen and (min-width: 450px) and (max-width: 1024px) {
    .logoDescription{
      width: 60%;
      margin:auto;
    }
  }

  @media (max-width: 1024px){
    #customHeader{
      margin-top: -1px;
    }
  }
  @media (max-width: 768px){

  }
</style>

<div class="col-xs-12 no-padding" id="customHeader" style="background-color: white">
  <div id="costumBanner" class="col-xs-12 col-sm-12 col-md-9 no-padding">
   <h1>Emploi 2.0<br/><span class="small">Une interface numérique dédiée à l'emploi et à la formation</span></h1>
  <img class="img-responsive" src='<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/lePortEmploi20/banner.png'> 
  </div>
  <div class="col-xs-12 col-sm-12 col-md-3 text-center padding-10" >
    <img class="img-responsive logoDescription" src='<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/lePortEmploi20/LOGO.jpg'> 
    <!--<h2>Une ville Dynamique</h2>-->
    <span style="overflow-y: hidden;max-height: 375px;">
      <span class="col-xs-12 margin-bottom-5">
        <b>Dynamiser le parcours demandeur d'emploi</b> tout en le sécurisant</span><br/>
      <span class="col-xs-12 margin-bottom-5">
        <b>Préparer</b> à l'emploi et <b>réduire</b> la durée
      </span> <br/> 
      <span class="col-xs-12 margin-bottom-5">
        <b>Lever les obstacles à l'emploi</b> : mobilité, garde d'enfant, freins socio économiques
      </span> <br/> 
      <span class="col-xs-12 margin-bottom-5">
        Partir des <b>besoins des entreprises</b> et des <b>compétences attendues</b>
      </span><br/>
      <span class="col-xs-12 margin-bottom-5"><b>
        Développer les compétences</b> transversales et transférables des participants <br/> 
      </span>
      <span class="col-xs-12 margin-bottom-5">
        <b>Mutualiser</b> les moyens
      </span>
    </span>
  </div>
</div>
<!--Dynamiser le parcours du participant demandeur d'emploi tout en le sécurisant, pour le préparer à l'emploi (et réduire la durée) <br/> Lever les obstacles à l'emploi : travail sur les freins périphériques (mobilité, garde d'enfant, freins socio économiques). <br/> Une clé d’entrée : partir des besoins des entreprises et des compétences attendues en situation de travail <br/> Une approche : développer les compétences transversales et transférables des participants <br/> Mutualiser les moyens.-->


  <div class="col-md-12 col-lg-12 col-sm-12 imageSection no-padding" 
     style=" position:relative;">

    

    
    <div class="col-sm-12 col-md-12 col-xs-12 no-padding" style="background-color:#fff; max-width:100%; float:left;">
      <div class="col-xs-12 margin-top-50 margin-bottom-25 text-center hidden" >
        <h2 class="text-red text-center">Filiere Numerique</h2>
        <h5 class=" col-xs-12 text-center" style="font-style:italic;">
          <?php echo Yii::t("home","Collective intelligence at service for citizens") ?>
        </h5>
        <br/>
        <h2 class="text-red text-center homestead">1 + 1 = 3</h2>
        <h5 class=" col-xs-12 text-center" style="font-style:italic;">
          Wikipedia <i class="fa fa-plus text-red"></i> Open Street Maps 
          <i class="fa fa-plus  text-red"></i> Open source Society
        </h5>
        <br/>
        <div class="center"  >
          <div  style="position:absolute; transform: rotate(60deg);margin:0 47%;" >
            <img class="img-responsive" width=50 src="<?php echo $this->module->assetsUrl; ?>/images/home/triangle.png" />
          </div>
        </div>
      </div>

      <style>
        .btn-main-menu{
          border:2px solid transparent;
          min-height:100px;
        }
        .btn-main-menu:hover{
          border:2px solid #ccc;
        }
        .ourvalues img{
          height:70px;
        }

        .box-register label.letter-black{
          margin-bottom:3px;
          font-size: 13px;
        }
      </style>

      <div class="col-xs-12 no-padding" style="text-align:center;margin-bottom:24px;margin-top:100px;"> 
        <div class="col-xs-12 no-padding">
          <div class="col-md-12 col-sm-12 col-xs-12 padding-20" style="padding-left:100px;background-color: #f6f6f6; min-height:400px;">
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 pull-left padding-20 shadow2" style="margin-top:-100px;margin-bottom:50px;background-color: #fff;font-size: 14px;">
              <div class="col-xs-12 font-montserrat ourvalues" style="text-align:center;">
                        <!-- <div class="col-md-1 col-sm-1 hidden-xs"></div> -->
                        <div class="col-lg-3 col-md-2 col-sm-4 col-xs-6" style="text-align:center;">
                          <img class="img-responsive" style="margin:0 auto;" 
                             src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/lePortEmploi20/LOGO.jpg"/>
                             VILLE LE PORT
                        </div>
                        <div class="col-lg-3 col-md-2 col-sm-4 col-xs-6">
                          <a href="#annonces?searchSType=jobs&source=poleEmploi"><img class="img-responsive" style="margin:0 auto;" 
                             src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/lePortEmploi20/pôle-emploi-logo-300x300.jpg"/>
                             POLE EMPLOI</a>
                        </div>
                        <div class="col-lg-3 col-md-2 col-sm-4 col-xs-6">
                          <img class="img-responsive" style="margin:0 auto;" 
                             src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/lePortEmploi20/cma.jpg"/>
                             CHAMBRE DE METIERS ET DE L'ARTISANAT de la Réunion 
                        </div>
                        <div class="col-lg-3 col-md-2 col-sm-4 col-xs-6">
                          <img class="img-responsive" style="margin:0 auto;" 
                             src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/lePortEmploi20/tco.png"/>
                             TERRITOIRE <br/>DE LA COTE OUEST
                        </div>
                        
                        <div class="col-lg-3 col-md-2 col-sm-4 col-xs-6">
                          <img class="img-responsive" style="margin:0 auto;" 
                             src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/lePortEmploi20/dep.jpg"/>
                             DEPARTEMENT RÉUNION
                        </div>
                        <div class="col-lg-3 col-md-2 col-sm-4 col-xs-6">
                          <img class="img-responsive" style="margin:0 auto;" 
                             src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/lePortEmploi20/logo_mls.png"/>
                             MISSIONS LOCALES
                        </div>
                        <div class="visible-lg col-lg-3">
                          <img class="img-responsive" style="margin:0 auto;" 
                             src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/lePortEmploi20/CCIr.jpg"/>
                             CHAMBRE DE COMMERCE ET DE L'INDUSTRIE RÉUNION
                        </div>
                        <div class="visible-lg col-lg-3">
                          <img class="img-responsive" style="margin:0 auto;" 
                             src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/lePortEmploi20/Logo_CR-Reunion1.png"/>
                             RÉGION RÉUNION
                        </div>
                    </div>
                  </div>
            
            <h3 class="col-xs-12 text-center">
              <i class="fa fa-th"></i> <?php echo Yii::t("home", "5 main applications") ?><br>
              <small>
                <b>Le Port 2.0 :</b> <?php echo Yii::t("home", "L'innovation au service de l'emploi") ?>,<br>
                <?php //echo Yii::t("home", "created for citizens actors of change") ?>
              </small>
              <hr style="width:40%; margin:20px auto; border: 4px solid #cecece;">
            </h3>

                    <a href="javascript:;" data-hash="#annonces" class=" btn-main-menu lbh-menu-app col-xs-12 col-sm-6 col-md-4 col-md-offset-2 padding-10 margin-top-5" data-type="classifieds" >
                        <div class="text-center">
                            <div class="col-md-12 no-padding text-center">
                                <h4 class="no-margin text-red">
                                  <i class="fa fa-bullhorn"></i>
                                  <?php echo Yii::t("home","Offres d'emploi") ?>
                                    <br><small class="text-dark">
                                        <?php echo Yii::t("home","Retoruvez toutes les annonces d'offres d'emploi, de stages et de formations dans votre ville")?>
                                    </small>
                                </h4>
                            </div>
                        </div>
                    </a>
            <a href="javascript:;" data-hash="#search" class="btn-main-menu lbh-menu-app col-xs-12 col-sm-6 col-md-4 padding-10 margin-top-5" data-type="search" >    
                        <div class="text-center">
                            <!-- <h4 class="text-red no-margin "><i class="fa fa-search"></i>
                                <span class="homestead"> <?php //echo Yii::t("home","SEARCH") ?></span>
                            </h4><br/> -->
                            <div class="col-md-12 no-padding text-center">
                                <h4 class="no-margin text-red">
                                  <i class="fa fa-search"></i>
                                  <?php echo Yii::t("home","Search engine") ?>
                                    <br>
                                    <small class="text-dark">
                                        <?php echo Yii::t("home","Find & connect with local actors") ?>
                                    </small>
                                </h4>
                            </div>
                        </div>
                    </a>

                    
                    
                    <a href="javascript:;" data-hash="#live" class="btn-main-menu lbh-menu-app col-xs-12 col-sm-6 col-md-4 padding-10 margin-top-5" > 
                        <div class="text-center">
                            <div class="col-md-12 no-padding text-center">
                                <h4 class="no-margin text-red">
                                  <i class="fa fa-newspaper-o"></i>
                                  <?php echo Yii::t("home","A common news stream") ?>
                                    <br><small class="text-dark">
                                        <?php echo Yii::t("home","Local Message sharing and group communication")?>
                                    </small>
                                </h4>
                            </div>
                        </div>
                    </a>
                            <div class=" col-xs-12 col-sm-6 col-md-4 padding-20 hidden-xs hidden-sm" style="">
              <img class="img-responsive" style="margin:0 auto;margin-top: 0px;" src="<?php echo $this->module->assetsUrl; ?>/images/home/modules_screen.png"/>
            </div>  
                    <a href="javascript:;" data-hash="#agenda" class="btn-main-menu lbh-menu-app col-xs-12 col-sm-6 col-md-4 padding-10 margin-top-5" data-type="agenda">
                        <div class="text-center">
                            <div class="col-md-12 no-padding text-center">
                                <h4 class="no-margin text-red">
                                  <i class="fa fa-calendar"></i>
                                  <?php echo Yii::t("home","A common agenda") ?>
                                    <br><small class="text-dark">
                                        <?php echo Yii::t("home","All local events in a click away") ?>
                                    </small>
                                </h4>
                            </div>
                        </div>
                    </a>


            

                   
             <div class=" col-xs-12 col-sm-6 col-md-4 padding-20 visible-xs visible-sm" style="">
              <img class="img-responsive" style="margin:0 auto;margin-top: 0px;" src="<?php echo $this->module->assetsUrl; ?>/images/home/modules_screen.png"/>
            </div>

        </div>


      
    </div>

  

  <div class="col-sm-12 col-md-12 col-xs-12 no-padding" style="background-color:#E33551; max-width:100%; float:left;" id="teamSection">
    
    <center>
      <i class="fa fa-caret-down" style="color:#f6f6f6"></i><br/>
    
      <i class="fa fa-briefcase fa-4x text-white"></i>
      <h1 class="homestead" style="color:#fff">
        L'EMPLOI 2.0
      </h1>
      
          
      <style>.hhh a{color:white; font-weight: bold;text-transform: underline;}</style>
      <div class="col-sm-12 text-white padding-bottom-15 hhh">
        <h3>
          <small class="text-white">
          <?php echo Yii::t("home","Everything we do is <a href='https://github.com/pixelhumain'>OpenSource</a> and built in collaborative way") ?>.<br/>
          <?php echo Yii::t("home","We are experimenting new ways of gouvernance,<br>managing territory, implicating local actor into everything and everywhere") ?>.
          </small>
        </h3>

        <!-- <i>"EN AMÉLIORATION CONTINUE"</i> -->
        <br>
        <hr style="width:40%; margin:10px auto; border: 4px solid #f68989;">
        <h3 class="no-margin"><i class="fa fa-link"></i> <?php echo Yii::t("home","Join us") ?> !</h3><br>
        
      </div>
    </center>
    <div class="space20"></div>
  </div>

  <div class="col-md-12 font-montserrat padding-bottom-50" style="color:#293A46; float:left; width:100%;">
    <center>
      <i class="fa fa-caret-down" style="color:#E33551"></i>
      <br/>
      
      <a class="lbh"  href="#@co-communication">
        <div class="ahover bg-white padding-10 col-sm-12 col-md-4">
          <i class="fa fa-building fa-2x"></i>
          <br/>
          <span class="uppercase text-red" style="font-size: 18px;"> #BTP </span><br/>
          <span style="font-size: 16px;font-style:italic"> <?php echo Yii::t("home", "Share and imagine great ideas") ?> <br/></span>
        </div>  
      </a>

      <a class="lbh"  href="#@codesign">
        <div class="ahover bg-white padding-10 col-sm-12 col-md-4">
          <i class="fa fa-coffee fa-2x"></i>
          <br/>
          <span class="uppercase text-red" style="font-size: 18px;"> #ADMINISTRATIF </span><br/>
          <span style="font-size: 16px;font-style:italic"> <?php echo Yii::t("home", "Ideas Design Graphics Video") ?> <br/></span>
        </div>  
      </a>
      
      <a class="lbh"  href="#@codev">
        <div class="ahover bg-white padding-10 col-sm-12 col-md-4">
          <i class="fa fa-desktop fa-2x"></i>
          <br/>
          <span class="uppercase text-red" style="font-size: 18px;"> #NUMÉRIQUE </span><br/>
          <span style="font-size: 16px;font-style:italic"> <?php echo Yii::t("home", "Core Development team") ?> <br/></span>
        </div>
      </a>

      <a class="lbh"  href="#@communecter">
        <div class="ahover bg-white padding-10 col-sm-12 col-md-4">
          <i class="fa fa-lightbulb-o fa-2x"></i>
          <br/>
          <span class="uppercase text-red" style="font-size: 18px;"> #ECONOMIE </span><br/>
          <span style="font-size: 16px;font-style:italic"> <?php echo Yii::t("home", "Project Management") ?> <br/></span>
        </div>  
      </a>


      <a class="lbh"  href="#@openatlas">
        <div class="ahover bg-white padding-10 col-sm-12 col-md-4">
          <i class="fa fa-group fa-2x"></i>
          <br/>
          <span class="uppercase text-red" style="font-size: 18px;"> #SOCIAL </span><br/>
          <span style="font-size: 16px;font-style:italic"> <?php echo Yii::t("home", "Non Governmental Organization") ?> <br/></span>
        </div>  
      </a>


      <a class="lbh"  href="#@pixelhumain">
        <div class="ahover bg-white padding-10 col-sm-12 col-md-4">
          <i class="fa fa-circle-thin fa-2x"></i>
          <br/>
          <span class="uppercase text-red" style="font-size: 18px;"> #SERVICE PUBLIQUE </span><br/>
          <span style="font-size: 16px;font-style:italic"> <?php echo Yii::t("home", "Active contributors and soon a cooperative") ?> <br/></span>
        </div>  
      </a>

      <a class="lbh"  href="#@connections">
        <div class="ahover bg-white padding-10 col-sm-12 col-md-4">
          <i class="fa fa-connectdevelop fa-2x"></i>
          <br/>
          <span class="uppercase text-red" style="font-size: 18px;"> #AGRICULTURE </span><br/>
          <span style="font-size: 16px;font-style:italic"> <?php echo Yii::t("home", "All people we meet.") ?> <br/></span>
        </div>  
      </a>
      
      <a class="lbh"  href="#@cofinanceur">
        <div class="ahover bg-white padding-10 col-sm-12 col-md-4">
          <i class="fa fa-heart fa-2x"></i>
          <br/>
          <span class="uppercase text-red" style="font-size: 18px;"> #METIER DE LA MER</span><br/>
          <span style="font-size: 16px;font-style:italic"> <?php echo Yii::t("home", "Money for bills & Love to live.") ?> <br/></span>
        </div>
      </a>

      <a class="lbh"  href="#@cotest">
        <div class="ahover bg-white padding-10 col-sm-12 col-md-4">
          <i class="fa fa-child fa-2x"></i>
          <br/>
          <span class="uppercase text-red" style="font-size: 18px;"> #RESSOURCES HUMAINES</span><br/>
          <span style="font-size: 16px;font-style:italic"> <?php echo Yii::t("home", "Good tools have great testers") ?> <br/></span>
        </div>
      </a>

      <a class="lbh"  href="#@cobugs">
        <div class="ahover bg-white padding-10 col-sm-12 col-md-4">
          <i class="fa fa-plane fa-2x"></i>
          <br/>
          <span class="uppercase text-red" style="font-size: 18px;"> #TOURISME </span><br/>
          <span style="font-size: 16px;font-style:italic"> <?php echo Yii::t("home", "Help share & destroy bugs") ?> <br/></span>
        </div>  
      </a>

      <a class="lbh"  href="#@cointerop">
        <div class="ahover bg-white padding-10 col-sm-12 col-md-4">
          <i class="fa fa-ambulance fa-2x"></i>
          <br/>
          <span class="uppercase text-red" style="font-size: 18px;"> #SANTÉ</span><br/>
          <span style="font-size: 16px;font-style:italic"> <?php echo Yii::t("home", "Connecting Systems together") ?> <br/></span>
        </div>
      </a>
      
      <a class="lbh"  href="#@cotools">
        <div class="ahover bg-white padding-10 col-sm-12 col-md-4">
          <i class="fa fa-bus fa-2x"></i>
          <br/>
          <span class="uppercase text-red" style="font-size: 18px;"> #TRANSPORT</span><br/>
          <span style="font-size: 16px;font-style:italic"> <?php echo Yii::t("home", "Open Source Tools For Communities") ?> <br/></span>
        </div>
      </a>

  </center>
  </div>


  
<div class="col-sm-12 col-md-12 col-xs-12 no-padding" style="background-color:#E33551; max-width:100%; float:left;" id="teamSection">
     <center>
       <i class="fa fa-caret-down" style="color:#f6f6f6"></i><br>
    
      <i class="fa fa-handshake-o fa-4x text-white"></i>
      <h1 class="homestead" style="color:#fff">
      <?php echo Yii::t("home","Financé par") ?>
      </h1>
      
      <div class="col-xs-8 col-md-4 col-md-offset-4 col-xs-offset-2">
        <div class="col-xs-4 padding-10 bg-white"> <img class="img-responsive" style="margin:0 auto;" 
                             src="<?php echo Yii::app()->getModule("eco")->assetsUrl; ?>/images/custom/leport/LOGO.jpg"/></div>
        <div class="col-xs-4"> <img class="img-responsive" style="margin:0 auto;" 
                             src="<?php echo Yii::app()->getModule("eco")->assetsUrl; ?>/images/custom/leport/prefecture-reunion.jpg"/>
                         </div>
        <div class="col-xs-4"> <img class="img-responsive" style="margin:0 auto;" 
                             src="<?php echo Yii::app()->getModule("eco")->assetsUrl; ?>/images/custom/leport/cgte.png"/>
                         </div>
      </div>
    </center>
    <div class="space20 col-xs-12" style="margin-bottom: 20px;"></div>
  </div>
  <div class="col-md-12 contact-map padding-bottom-50" style="color:#293A46; float:left; width:100%;" id="contactSection">
    <center>
      <i class="fa fa-caret-down" style="color:#E33551"></i>
      <h1 class="homestead">
      <?php echo Yii::t("home","CONTACT") ?>
      </h1>
      + 262 262 34 36 86<br>mailleport@leport.fr

      <br/><a href="gitlab.adullact.net/pixelhumain/" target="_blank"><?php echo Yii::t("home","powered by <span style='color:#E33551;'>@Pixel'Humain</span>") ?></a>
    <center>
  </div>

</div>




<div class="portfolio-modal modal fade" id="modalForgot" tabindex="-1" role="dialog" aria-hidden="true">
    <form class="modal-content form-email box-email padding-top-15"  >
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <span class="name hidden" >
                        <?php if(Yii::app()->params["CO2DomainName"] == "kgougle"){ ?>
                            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/img/KGOUGLE-logo.png" height="60" class="inline margin-bottom-15">
                       <?php } else { ?>
                            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/img/CO2r.png" height="100" class="inline margin-bottom-15">
                        <?php } ?>
                    </span>
                    <h4 class="letter-red no-margin" style="margin-top:-5px!important;">Mot de passe oublié ?</h4><br>
                    <hr>
                    <p><small>Indiquez votre addresse e-mail, vous recevrez un e-mail contenant votre mot de passe.</small></p>
                    <hr>
                    
                </div>
            </div>
            <div class="col-md-4 col-md-offset-4 text-left">
                
                <label class="letter-black"><i class="fa fa-envelope"></i> E-mail</label><br>
                <input class="form-control" id="email2" name="email2" type="text" placeholder="E-mail"><br/>
                
                <hr>

                <div class="pull-left form-actions no-margin" style="width:100%; padding:10px;">
                    <div class="errorHandler alert alert-danger no-display registerResult pull-left " style="width:100%;">
                        <i class="fa fa-remove-sign"></i> <?php echo Yii::t("login","You have some form errors. Please check below.") ?>
                    </div>
                </div>

                <!-- <div class="form-actions">
                     <button type="submit"  data-size="s" data-style="expand-right" style="background-color:#E33551" class="forgotBtn ladda-button center center-block">
                        <span class="ladda-label">XXXXXXXX</span><span class="ladda-spinner"></span><span class="ladda-spinner"></span>
                    </button>
                </div> -->

                <a href="javascript:" class="btn btn-danger pull-left" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo Yii::t("common","Back") ?></a>
                <button class="btn btn-success text-white pull-right forgotBtn"><i class="fa fa-sign-in"></i> Envoyer</button>
                
                
                <div class="col-md-12 margin-top-50 margin-bottom-50"></div>
            </div>      
        </div>
    </form>
</div>


<script type="text/javascript">


jQuery(document).ready(function() {
  topMenuActivated = false;
  hideScrollTop = true;
  checkScroll();


  setTitle("<?php echo Yii::t("home","Welcome on") ?> <span class='text-red'>LePort</span>, Emploi 2.0","home","<?php echo Yii::t("home","Le Port, Emploi 2.0") ?>");
  $('.tooltips').tooltip();

 
});



</script>





