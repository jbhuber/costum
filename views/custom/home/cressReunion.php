
<div class="pageContent">


<style type="text/css">
  #customHeader{
    margin-top: 0px;
  }
  #costumBanner{
   /* max-height: 375px; */
  }
  #costumBanner h1{
    position: absolute;
    color: white;
    background-color: rgba(0,0,0,0.4);
    font-size: 29px;
    bottom: 0px;
    padding: 20px;
  }
  #costumBanner h1 span{
    color: #eeeeee;
    font-style: italic;
  }
  #costumBanner img{
    min-width: 100%;
  }
  .btn-main-menu{
    background: #1b7baf;
    border-radius: 20px;
    padding: 20px !important;
    color: white;
    cursor: pointer;
    border:3px solid transparent;
    /*min-height:100px;*/
  }
  .btn-main-menu:hover{
    border:2px solid #1b7baf;
    background-color: white;
    color: #1b7baf;
  }
  .ourvalues img{
    height:70px;
  }
  .main-title{
    color: #487614;
  }

  .ourvalues h3{
    font-size: 36px;
  }
  .box-register label.letter-black{
    margin-bottom:3px;
    font-size: 13px;
  }
  .bullet-point{
      width: 5px;
    height: 5px;
    display: -webkit-inline-box;
    border-radius: 100%;
    background-color: #fbae55;
  }
  .text-explain{
    color: #555;
    font-size: 18px;
  }
  .blue-bg {
  background-color: white;
  color: #5b2549;
  height: 100%;
  padding-bottom: 20px !important;
}


.support-section{
  background-color: white;
}
.support-section h2{
  text-align: center;
    padding: 60px 0px !important;
    background: #450e33;
    font-size: 40px;
    color: white;
    margin-bottom: 20px;
}


  @media screen and (min-width: 450px) and (max-width: 1024px) {
    .logoDescription{
      width: 60%;
      margin:auto;
    }
  }

  @media (max-width: 1024px){
    #customHeader{
      margin-top: -1px;
    }
  }
  @media (max-width: 768px){

  }
</style>

<div class="col-xs-12 no-padding" id="customHeader" style="background-color: white">
  <div id="costumBanner" class="col-xs-12 no-padding">
 <!--  <h1>L'entraide<br/><span class="small">Une interface numérique pour échanger</span></h1>-->
    <img class="img-responsive" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/cressReunion/Encart-Accueil-v2.jpg'> 
  </div>
  <!--<div class="col-md-12 col-lg-12 col-sm-12 imageSection no-padding" 
     style=" position:relative;">-->
  <div class="col-sm-12 col-md-12 col-xs-12 no-padding" style="max-width:100%; float:left;">
    <div class="col-xs-12 no-padding" style=""> 
      <div class="col-xs-12 no-padding">
        <div class="col-md-12 col-sm-12 col-xs-12 padding-20" style="padding-left:100px;background-color: #f6f6f6; min-height:400px;">
          <div class="col-xs-12 col-sm-10 col-sm-offset-1 pull-left padding-20 shadow2" style="margin-bottom:-80px;background-color: #fff;font-size: 14px;z-index: 5;">
            <div class="col-xs-12 font-montserrat ourvalues" style="text-align:center;">
              <h3 class="col-xs-12 text-center">
                <span class="main-title">Un outil pour : </span><br>
                <small>
                  <b>Nos valeurs sont solidaires, nos services sont diversifiés, non délocalisables et mettent l’humain au cœur des projets.<br>
                </small>
                <hr style="width:40%; margin:20px auto; border: 4px solid #fbae55;">
              </h3>
              <div class="col-xs-12">
                <a href="javascript:;" data-hash="#search" class="btn-main-menu lbh-menu-app col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3"  >
                    <div class="text-center">
                        <div class="col-md-12 no-padding text-center">
                            <h4 class="no-margin uppercase">
                              <i class="fa fa-hand-point-right faa-pulse"></i>
                              <?php echo Yii::t("home","Découvrir") ?>
                            </h4>
                        </div>
                    </div>
                </a>
              </div>
              <h3 class="col-xs-12 text-center">
                <hr style="width:40%; margin:20px auto; border: 4px solid #fbae55;">
              </h3>
              <!--<div class="col-md-10 col-md-offset-1 col-xs-12">
                <span class="text-explain">Nous sommes de plus en plus nombreux à vouloir agir face au dérèglement climatique, à la croissance des inégalités, à la crise de la démocratie.<br/><span class="bullet-point"></span><br/>
                Pour relever ces défis majeurs, changer nos pratiques quotidiennes de consommation ne suffit plus.
                Il nous faut également œuvrer à la transformation de nos politiques publiques.<br/><span class="bullet-point"></span><br/>
                Si la transition doit avoir lieu à toutes les échelles, les communes peuvent être le fer de lance de ce
                mouvement.<br/>
                En mars 2020, des candidat.e.s se présenteront aux élections municipales. Nous voulons les aider à
                identifier et mettre en œuvre, une fois élu.e.s, des mesures concrètes pour encourager la transition
                écologique, sociale et démocratique de leur commune.<br/><span class="bullet-point"></span><br/>
                Nous, citoyennes et citoyens, sommes les mieux placés pour définir ces priorités et construire les
                communes de demain. Ensemble, nous sommes la transition.
                </span>
              </div>-->
            </div>
          </div>

        </div>

      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  jQuery(document).ready(function() {
    setTitle("CRESS Réunion");
  });
</script>


