<?php 
/* TODO 

[ ] reload is empty on hope page
[ ] reload on any page 
[ ] use categories or types to organize poi.docs 
[ ] create poi.timeline as an action plan 
[ ] edit poi.doc
[ ] delete poi.doc

wishlist 
[ ] poi.doc templates
[ ] move doc position index
[ ] use poi.html as piece of a page

*/

$cssJS = array(
    '/plugins/jQuery-Knob/js/jquery.knob.js',
    '/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js',
    '/plugins/jQuery-Smart-Wizard/styles/smart_wizard.css',
	// SHOWDOWN
	'/plugins/showdown/showdown.min.js',
	// MARKDOWN
	'/plugins/to-markdown/to-markdown.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->request->baseUrl); 
?>

<div id="header-doc" class="shadow2">
	<a href='javascript:;' id="show-menu-xs" class="visible-xs visible-sm pull-left" data-placement="bottom" data-title="Menu"><i class="fa fa-bars"></i></a>
	<h2 class="elipsis no-margin"><i class="fa fa-book hidden-xs"></i> <?php echo Yii::t("docs", "All <span class='hidden-xs'>you need to know</span> about") ?> LE <span style="color:#65BA91">CONTRAT DE TRANSITION ÉCOLOGIQUE</span></h2>
	
    <a href='javascript:;' class="lbh pull-right" id="close-docs"><span><i class="fa fa-sign-out"></i> <?php echo Yii::t("common","Back") ?></span></a>
</div>

<div id="menu-left" class="col-md-3 col-sm-2 col-xs-12 shadow2">
  	<ul class="col-md-12 col-sm-12 col-xs-12 no-padding">
		
		<li class="col-xs-12 no-padding">
			<a href="javascript:" class="link-docs-menu down-menu" data-type="cte" data-dir="costum.views.custom.ctenat.docs">
				<i class="fa fa-angle-right"></i> Parcours
			</a>
			<ul class="subMenu col-xs-12 no-padding">
				<li class="col-xs-12 no-padding">
					<a href="javascript:;" class="link-docs-menu" data-type="public" data-dir="costum.views.custom.ctenat.docs">
						Acteurs publics
					</a>
				</li>
				<li class="col-xs-12 no-padding">
					<a href="javascript:;" class="link-docs-menu" data-type="socieco" data-dir="costum.views.custom.ctenat.docs">
						Acteurs socio-économiques
					</a>
				</li>
				

				<?php 
				$pois = PHDB::find( Poi::COLLECTION, array( "source.key" => "ctenat") );
				
				foreach ($pois as $key => $value) {
				?>
				
				<li class="col-xs-12 no-padding">
					<a href="javascript:" class="link-docs-menu-poi" data-poi="<?php echo (string)$value["_id"]; ?>" >
						<?php echo $value["name"]; ?>
					</a>
				</li>

				<?php
				}
				?>
				<li class="col-xs-12 no-padding">
					<a href="javascript:dyFObj.openForm('poi');" class="text-red"	>
						<i class="fa fa-plus-circle"></i> AJOUTER
					</a>
				</li>
			</ul>
		</li>
		
	</ul>
</div>

<script type="text/javascript">
	
	jQuery(document).ready(function() {

		$("#close-docs").attr("href",urlBackHistory);

		$(".link-docs-menu-poi").off().on("click",function(){
			alert(".link-docs-menu-poi");
			var url = baseUrl+'/co2/element/get/type/poi/id/'+$(this).data("poi");
			ajaxPost(null ,url, null,function(data){
				descHtml = dataHelper.markdownToHtml(data.map.description); 
	   			$('#container-docs').html(descHtml);
			},"html");

		})
	})
</script>

<!-- 
				<li class="col-xs-12 no-padding">
					<a href="javascript:;" class="link-docs-menu" data-type="map" data-dir="costum.views.custom.ctenat.docs">
						CARTE 
					</a>
				</li>
				<li class="col-xs-12 no-padding">
					<a href="javascript:;" class="link-docs-menu" data-type="calendrier" data-dir="costum.views.custom.ctenat.docs">
						CALENDRIER 
					</a>
				</li>
				<li class="col-xs-12 no-padding">
					<a href="javascript:;" class="link-docs-menu" data-type="ministere" data-dir="costum.views.custom.ctenat.docs">
						MINISTERE DE L'EGOLOGIE
					</a>
				</li>
				<li class="col-xs-12 no-padding">
					<a href="javascript:;" class="link-docs-menu" data-type="pcaet" data-dir="costum.views.custom.ctenat.docs">
						PCAET from views folder
					</a>
				</li>

				<li class="col-xs-12 no-padding">
					<a href="javascript:" class="link-docs-menu down-menu" data-type="welcome" data-dir="costum.views.custom.ctenat.docs">
						<i class="fa fa-angle-right"></i> COMPRENDRE
					</a>
				</li>

				<li class="col-xs-12 no-padding">
					<a href="javascript:" class="link-docs-menu down-menu" data-type="participer" data-dir="costum.views.custom.ctenat.docs">
						<i class="fa fa-angle-right"></i> PARTICIPER
					</a>
				</li> -->