<?php $cssJS = array(
    '/plugins/jQuery-Knob/js/jquery.knob.js',
    '/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js',
    // SHOWDOWN
    '/plugins/showdown/showdown.min.js',
    // MARKDOWN
    '/plugins/to-markdown/to-markdown.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->request->baseUrl);
?>
<h1>Public</h1>

<div class="col-xs-12 margin-top-20">
    <?php 
    $params = array(
        "poiList" => PHDB::find(Poi::COLLECTION, 
                    array( "parent.".Yii::app()->session["costum"]["contextId"] => array('$exists'=>1), 
                           "parent.".Yii::app()->session["costum"]["contextId"].".type"=>Yii::app()->session["costum"]["contextType"],
                           "type"=>"cms") ),
        "listSteps" => array("one","two","three","four","five","six")
    );
    //var_dump($params);
    echo $this->renderPartial("costum.views.tpls.wizard",$params,true);
    ?>
</div>