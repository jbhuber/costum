<?php

class Costum {
	const COLLECTION = "costum";
	const CONTROLLER = "costum";
	const MODULE = "costum";
	
	//used in initJs.php for the modules definition
	public static function getConfig($context=null){
		return array(
			"collection"    => self::COLLECTION,
            "controller"   => self::CONTROLLER,
            "module"   => self::MODULE,
            "categories" => CO2::getModuleContextList(self::MODULE, "categories", $context),
       		"lbhp" => true
		);
	}

	/**
	 * get a Poi By Id
	 * @param String $id : is the mongoId of the poi
	 * @return poi
	 */
	public static function getById($id) { 
	  	$classified = PHDB::findOneById( self::COLLECTION ,$id );
	  	//$classified["parent"] = Element::getElementByTypeAndId()
	  	$classified["typeClassified"]=@$classified["type"];
	  	$classified["type"]=self::COLLECTION;
	  	$where=array("id"=>@$id, "type"=>self::COLLECTION, "doctype"=>"image");
	  	$classified["images"] = Document::getListDocumentsWhere($where, "image");//(@$id, self::COLLECTION);
	  	return $classified;
	}

	/**
	* Function called by @self::filterThemeInCustom in order to treat params.json of communecter app considering like the config
	* This loop permit to genericly update value, add values or delete value in the tree
	* return the communecter entry filtered by costum expectations
	**/
	public static function checkCOstumList($check, $filter,$k=null){
        $newObj=array();

        $arrayInCustom=["label","subdomainName","placeholderMainSearch","icon","height","imgPath","useFilter","slug","formCreate", "setParams", "initType", "inMenu","types","actions", "dataTarget", "dynform", "id", "filters", "tagsList"];
        
        foreach($check as $key => $v){
            
            if(!empty($v)){
                $newObj[$key]=(isset($filter[$key])) ? $filter[$key] : $v;
                $checkArray=true;
                foreach($arrayInCustom as $entry){
                    if(isset($v[$entry])){
                        $newObj[$key][$entry]=$v[$entry];
                        $checkArray=false;
                    }
                }
            }
            if(is_array($v) && $checkArray)
                $newObj[$key]=self::checkCOstumList($v, $newObj[$key], $k);
        }
        return $newObj;
    }

    /*
    * @filterThemeInCustom permits to clean, update or add entry in paramsConfig of communecter thanks to costumParams 
    *	Receive Yii::app()->session["paramsConfig"] in order to modify it if Yii::app()->session["costum"] is existed
    *	
    */
    public static function filterThemeInCustom($params){
        //filter menu app custom 
        // Html construction of communecter
        if(isset(Yii::app()->session["costum"]["htmlConstruct"])){
            $constructParams=Yii::app()->session["costum"]["htmlConstruct"];
            //var_dump($params);exit;
            
            // Check about header construction
            // - Entry banner to adda tpl path to a custom banner, by default not isset
            // - Entry menuTop will config btn present in menu top (navLeft & navRight)
            if(isset($constructParams["header"])){
                if(isset($constructParams["header"]["banner"])) //URL
                    $params["header"]["banner"]=$constructParams["header"]["banner"];
                if(isset($constructParams["header"]["css"]))
                    $params["header"]["css"]=$constructParams["header"]["css"];
                if(isset($constructParams["header"]["menuTop"])){
                    if(isset($constructParams["header"]["menuTop"]["navLeft"]))
                        $params["header"]["menuTop"]["navLeft"]=self::checkCOstumList($constructParams["header"]["menuTop"]["navLeft"],$params["header"]["menuTop"]["navLeft"]);
                    if(isset($constructParams["header"]["menuTop"]["navRight"])){
                        $params["header"]["menuTop"]["navRight"]=self::checkCOstumList($constructParams["header"]["menuTop"]["navRight"],$params["header"]["menuTop"]["navRight"]);
                    }
                }
            }
            if(isset($constructParams["subMenu"])){
                if(empty($constructParams["subMenu"]))
                    $params["subMenu"]=false;
                else
                    $params["subMenu"]=self::checkCOstumList($constructParams["subMenu"],$params["subMenu"]);
            }
            if(isset($constructParams["footer"]))
                $params["footer"]=self::checkCOstumList($constructParams["footer"],$params["footer"]);
            
            // Check about admin Panel navigation (cosstumized space for costum administration expectations)
            if(isset($constructParams["adminPanel"]))
                $params["adminPanel"]=self::checkCOstumList($constructParams["adminPanel"],$params["adminPanel"]);
            // if(isset($constructParams["directory"]))
              //  $params["directory"]=self::checkCOstumList($constructParams["directory"],$params["directory"]);
                
            // Check about element page organization and content
            // - #initView is a param to give first view loaded when arrived in a page (ex : newstream, detail, agenda or another)
            // - #menuLeft of element => TODO PLUG ON checkCOstumList
            // - #menuTop of element => TODO finish customization & add it in params.json
            if(isset($constructParams["element"])){
                if(isset($constructParams["element"]["initView"]))
                    $params["element"]["initView"]=$constructParams["element"]["initView"];
                if(isset($constructParams["element"]["banner"])){
                    $params["element"]["banner"]=self::checkCOstumList($constructParams["element"]["banner"],$params["element"]["banner"]);
                }
                if(isset($constructParams["element"]["menuLeft"])){
                    $params["element"]["menuLeft"]=self::checkCOstumList($constructParams["element"]["menuLeft"],$params["element"]["menuLeft"]);
                }
               
            }
            // #appRendering permit to get horizontal and vertical organization of menu 
            //	 - horizontal = first version of co2 with menu of apps in horizontal 
            //	 - vertical = current version of co2 with menu of apps on the left 
            if(isset($constructParams["appRendering"]))
                $params["appRendering"]=$constructParams["appRendering"];
            if(isset($constructParams["directory"])){
                //var_dump($params["directory"]);
                $directoryP=$params["directory"];
                //var_dump($directoryP);
                $params["directory"]=$constructParams["directory"];
                //var_dump($params["directory"]);
                foreach($directoryP as $k => $v){
                    if(!isset($params["directory"][$k]))
                        $params["directory"][$k]=$v;
                }
            }

            // # redirect permits to application in case of undefined view or error or automatic redirection to fall on costum redirect
            // To be setting for logged user && unlogged user
            if(isset($constructParams["redirect"]))
                $params["pages"]["#app.index"]["redirect"]=$constructParams["redirect"];
            if(isset($constructParams["menuBottom"]))
                $params["menuBottom"]=self::checkCOstumList($constructParams["menuBottom"],$params["menuBottom"]);
        }
        // Check about app (#search, #live, #etc) if should costumized
        $menuApp = array("#annonces", "#search", "#agenda", "#live", "#dda");

        if(isset(Yii::app()->session["costum"]["app"])){
            $params["numberOfApp"]=count(Yii::app()->session["costum"]["app"]);
            $menuPages=self::checkCOstumList(Yii::app()->session["costum"]["app"], $params["pages"]);
           // Rest::json($menuPages); exit ;
          //var_dump($menuPages); exit ;
            foreach($params["pages"] as $hash => $v){
                if(!in_array($hash,$menuApp))
                    $menuPages[$hash]=$v;
            }
            $params["pages"]=$menuPages;
        }
        if(isset(Yii::app()->session["costum"]["typeObj"]))
            $params["typeObj"]=Yii::app()->session["costum"]["typeObj"];
        return $params;
    }
    public static function getContextList($slug, $contextName){

        $layoutPath ="../../modules/costum/json/".$slug."/".$contextName.".json";

        $str = file_get_contents($layoutPath);
        $list = json_decode($str, true);
        return $list;
    }

     /*
    * @checkUserPreferences will check for a costum presence of user settings
    * Example : Geographical selction to oriented a user experience (cf : @meuseCampagnes)
    * String $userId to get preferences 
    * Array $costum to find specific user preferences
    * Return costum array with or without user settings about this costum 
    */
    public static function checkUserPreferences($costum, $userId){
        $userPref=Preference::getPreferencesByTypeId($userId, Person::COLLECTION);
        if(!empty($userPref) && isset($userPref["costum"]) && isset($userPref["costum"][$costum["slug"]])){
            $costum["userPreferences"]=$userPref["costum"][$costum["slug"]];
        }
        return $costum;
    }
    /*
    *   @initMetaData will initialize variable to get set metaDatas in mainSearch.php
    *   Meta is composed of title, description, keywords, image and author
    */
    public static function initMetaData($c){

        if(isset($c["metaDesc"]))
            $shortDesc=$c["metaDesc"];
        else{
            $shortDesc =  @$c["shortDescription"] ? $c["shortDescription"] : "";
                if($shortDesc=="")
                    $shortDesc = @$c["description"] ? $c["description"] : "";
        }
        $this->getController()->module->description = $shortDesc;
        $this->getController()->module->pageTitle = (isset($c["metaTitle"])) ? $c["metaTitle"] : @$c["title"];
        if(isset($c["metaKeywords"]))
            $this->getController()->module->keywords = $c["metaKeywords"];
        else
           $this->getController()->module->keywords = (@$c["tags"]) ? implode(",", @$c["tags"]) : "";
        if (isset($c["favicon"])) {
            $mod = $this->getController()->module->id;
            //ex images can be given 
            if( substr_count($c["favicon"], '#') ){
                $pieces = explode("#", $c["favicon"]);
                $mod = $pieces[0];
                $c["favicon"] = $pieces[1];
            }
            $this->getController()->module->favicon = Yii::app()->getModule( $mod )->getAssetsUrl().$c["favicon"];
        }
        if (@$c["metaImg"]) {
            $mod = $this->getController()->module->id;
            //ex images can be given 
            if( substr_count($c["metaImg"], '#') ){
                $pieces = explode("#", $c["metaImg"]);
                $mod = $pieces[0];
                $c["metaImg"] = $pieces[1];
            }
            $this->getController()->module->image = Yii::app()->getModule( $mod )->getAssetsUrl().$c["metaImg"];
        }
        else if(@$c["logo"]){
            $this->getController()->module->image = $c["logo"];
        }
        //if(@)

    }
    /*
    *   @initScopeSelector will init geographical object of selection and user choice in costum
    *   return costum with entry necessary :
    *   Scopeseclector with entiere specifity of scope
    *   UserPreferences refering to its costum geographical experience
    */
    public static function initScopeSelector($params){
        $params["scopeSelector"]=Zone::getScopeByIds($params["scopeSelector"])["scopes"];
        /*foreach(["cities", "cp", "zones"] as $impact){
            if(isset($params["userPreferences"]) && isset($params["userPreferences"][$impact])){
                foreach($params["userPreferences"][$impact] as $key){
                    $valueScope=$params["scopeSelector"][$key];
                    $idScope=$params["scopeSelector"][$key]["id"];
                }
                //$params["filters"]=(!isset($params["filters"])) ? [] : $params["filters"];
                //$params["filters"]["scopes"][$impact]=$idScope;         
            }
        }*/
        return $params;
    }
    public static function prepData($params){
        if(!empty(Yii::app()->session["costum"] ) ) {

            if( !empty($params["source"]) && !empty($params["source"]["toBeValidated"]) ) {
                $params["source"]["toBeValidated"] = array(Yii::app()->session["costum"]["slug"] => true);
            }


        }


        if(!empty(Yii::app()->session["costum"]) && Yii::app()->session["costum"]["slug"] == "siteDuPactePourLaTransition"){


            if($params["collection"] == Organization::COLLECTION && $params["type"] == Organization::TYPE_GROUP){
               /* if(!empty($params["scope"])){

                    $listScope =  array();
                    foreach ($params["scope"] as $key => $value) {
                        $listScope[] = array("scope.".$key => array('$exists' => 1));
                        $params["name"] = (!empty($value["cityName"]) ? $value["cityName"] : $value["name"]);
                    }
                    $where = array('$and' => 
                                array(  array("type" => Organization::TYPE_GROUP),
                                        array("source.keys" => array('$in' => array(Yii::app()->session["costum"]["slug"]))),
                                        array('$or' => $listScope)
                                    ) );

                    $orga = PHDB::find(Organization::COLLECTION, $where, array("name", "scope"));
                    if(!empty($orga)){
                        throw new CTKException("Ce scope est déjà pris par un autre groupe");
                    }
                    
                }*/
            } else if($params["collection"] == Action::COLLECTION){
            	if(!empty($params["measures"] )){
            		foreach ($params["measures"] as $key => $value) {
	                	$params["name"] = $value["name"];
	                }
                	//throw new CTKException("HERE "+$params["name"]);
            	}
                
            } else {
                //throw new CTKException("it's good ");
            }
            
        }
        return $params;
    }

    public static function value($value){
        if( substr_count($value , 'this.') > 0 ){
            $field = explode( ".", $value );
            if( isset( Yii::app()->session["costum"][ $field[1] ] ) )
                return Yii::app()->session["costum"][ $field[1] ];
        }
        else 
            return Yii::app()->session["costum"][ $value ];
    }
}
?>