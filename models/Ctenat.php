<?php

class Ctenat {
	const COLLECTION = "costums";
	const CONTROLLER = "costum";
	const MODULE = "costum";
	
    public static $dataBinding_allProject  = array(
        "@type"     => "Project",
        "name"      => array("valueOf" => "name"),
        "image"     => array("valueOf" => "image",
                             "type"     => "url"),
        "maturity"      => array("valueOf" => "maturity"),
		"siren"     => array("valueOf" => "siren"),
		"why"     => array("valueOf" => "why"),
		"nbHabitant"     => array("valueOf" => "nbHabitant"),
		"dispositif"     => array("valueOf" => "dispositif"),
		"planPCAET"     => array("valueOf" => "planPCAET"),
		"singulartiy"     => array("valueOf" => "singulartiy"),
		"caracteristique"     => array("valueOf" => "caracteristique"),
		"actions"     => array("valueOf" => "actions"),
		"economy"     => array("valueOf" => "economy"),
		"inCharge"     => array("valueOf" => "inCharge"),
		"autreElu"     => array("valueOf" => "autreElu"),
		"contact"     => array("valueOf" => "contact"),
        "url"       => array("valueOf" => array(
                                
                                "communecter"   => array(   "valueOf" => '_id.$id',
                                                                "type"  => "url", 
                                                                "prefix"   => "/#project.detail.id.",
                                                                "suffix"   => ""),
                                "api"           => array(   "valueOf"   => '_id.$id', 
                                                                "type"  => "url", 
                                                                "prefix"   => "/api/project/get/id/",
                                                                "suffix"   => "" ),
                                "pdf"           => array(   "valueOf"   => '_id.$id', 
                                                                "type"  => "url", 
                                                                "prefix"   => "/co2/export/pdfelement/type/projects/id/",
                                                                "suffix"   => "" ),
                                "website"       => array(   "valueOf" => 'url'))),
        "address"   => array("parentKey"=>"address", 
                             "valueOf" => array(
                                    "@type"             => "PostalAddress", 
                                    "streetAddress"     => array("valueOf" => "streetAddress"),
                                    "postalCode"        => array("valueOf" => "postalCode"),
                                    "addressLocality"   => array("valueOf" => "addressLocality"),
                                    "codeInsee"         => array("valueOf" => "codeInsee"),
                                    "addressRegion"     => array("valueOf" => "addressRegion"),
                                    "addressCountry"    => array("valueOf" => "addressCountry")
                                    )),
        "startDate"     => array("valueOf" => "startDate"),
        "endDate"       => array("valueOf" => "endDate"),
        "geo"   => array("parentKey"=>"geo", 
                             "valueOf" => array(
                                    "@type"             => "GeoCoordinates", 
                                    "latitude"          => array("valueOf" => "latitude"),
                                    "longitude"         => array("valueOf" => "longitude")
                                    )),

    );
	
    // public static function prepData($params){
    //     if($params["collection"] == Project::COLLECTION){
    //     	if(!empty($params["scope"] )){
    //     		foreach ($params["scope"] as $key => $value) {
    //             	$params["name"] = $value["name"];
    //             }
    //     	}
            
    //     }
    //     return $params;
    // }

}
?>