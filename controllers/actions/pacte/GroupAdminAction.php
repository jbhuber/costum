<?php 
 /**
  * Display the directory of back office
  * @param String $id Not mandatory : if specify, look for the person with this Id. 
  * Else will get the id of the person logged
  * @return type
  */
class GroupAdminAction extends CAction
{
    public function run( $tpl=null, $view=null )
    {
      $controller = $this->getController();
      $limitMin=0;
      $stepLim=100;
      if(@$_POST["page"]){
        $limitMin=$limitMin+(100*$_POST["page"]);
      }
     
      $search="";
      if(@$_POST["text"] && !empty($_POST["text"])){
        $search = trim(urldecode($_POST['text']));
      }
      if(isset(Yii::app()->session["costum"]) 
        && isset(Yii::app()->session["costum"]["slug"]))
        $sourceKey=Yii::app()->session["costum"]["slug"];
      $query = array();
      $query = Search::searchString($search, $query); 
      $query = Search::searchSourceKey("siteDuPactePourLaTransition", $query);
     
      $params["typeDirectory"]=[Organization::COLLECTION];
      $params["results"]["organizations"] = PHDB::findAndLimitAndIndex ( Organization::COLLECTION , $query, $stepLim, $limitMin);
      if($tpl!="json")
        $params["results"]["count"]["organizations"] = PHDB::count( Organization::COLLECTION , $query);
      $page = "groupAdmin";
      if($tpl=="json")
        Rest::json( $params );
      else if(Yii::app()->request->isAjaxRequest)
          echo $controller->renderPartial("/custom/siteDuPactePourLaTransition/".$page,$params,true);
      //  else {
      //    $controller->render($page,$params);
        //}
      //}
    }
}
