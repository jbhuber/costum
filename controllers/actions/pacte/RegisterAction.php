<?php
class RegisterAction extends CAction
{
	public function run(){ 	
		
		$res = array(
			"result" => false,
			"msg"  => ""
		);
		//Rest::json(Yii::app()->session["costum"]); exit ;
		
		if(!empty($_POST["scope"]) && !empty($_POST["email"])){
			$html = "";
			$userMail=$_POST["email"];
			$where = array( "email" => $_POST["email"] );
			$user = PHDB::findOne(Person::COLLECTION, $where, array("_id", "name", "email"));
			$sendTopacteCreate=false;
			if(empty($user)){
				$newPerson = array( "name" => $_POST["name"],
									"email" => $_POST["email"],
									"invitedBy" => "");
				$user = Person::createAndInvite($newPerson, "");
				$html .= "<span>Un nouvelle utilisateur s'est connecté : ".$_POST["name"]."</span><br/>";
			}else{
				$user["id"] = (String) $user["_id"];
				$html .= "<span>Un utilisateur s'est connecté : ".$user["name"]."</span><br/>";
			}

			$name = "";
			foreach ($_POST["scope"] as $key => $value) {
				$name .= (!empty($value["name"]) ? $value["name"] : $value["cityName"]).' ';
				$checkScope=$key;
				$postalCode=$value["postalCode"];
			}
			$name = "Collectif local ".trim($name);
			$where = array( "source.key" => Yii::app()->session["costum"]["slug"],
							"scope.".$checkScope => array('$exists' => true )  );
			$exits = PHDB::findOne(Organization::COLLECTION, $where, array("_id", "name", "email"));


			if(!empty($exist)){
				$res["exist"] = true;
				$res["elt"] = $exist;
				$res["email"] = $exist["email"];
				//On rentre dans le cas ou l'orga existe et la mailing est créée 
				if(!isset($exits["source.toBeValidated"]) || empty($exits["source.toBeValidated"])){
					// TODO MAIL TO FRAMA
					//Si message groupe on envoie le mail avec le texte de l'utilisateur
					$sub=explode("@", $exist["email"])[0];
					$paramsMails = array("tplMail" => $sub."-subscribe@listes.transition-citoyenne.org",
									"tplObject" => "sub ".$sub,
									"tpl" => "basic",
									"fromMail"=>$userMail,
									"html" => $html);
					Mail::createAndSend($paramsMails);
					if(isset($_POST["msgGroup"])){
						$tplMsgList='<span><b>'.$_POST["name"].'</b> a écrit ce message :</span><br/><span style="padding:15px; margin-top:10px;background-color:#f9f9f9; border:1px solid #eee; border-radius: 10px;float:left;">'.$_POST["msgGroup"].'</span>';
						$paramsMails = array("tplMail" => $exist["email"],
									"tplObject" => "Message d'un nouveau membre sur le ".$sub,
									"tpl" => "basic",
									"fromMail"=>$userMail,
									"html" => $tplMsgList);
						Mail::createAndSend($paramsMails);
					}
					//$sendMailToPacte=false;
				}else{
					$sendTopacteCreate=true;
				}
			}else{

				$res["exist"] = false;
				$res["email"] = "pacte-".mb_strtolower($postalCode)."@listes.transition-citoyenne.org";
				$orga = array(	"name" => $name,
								"collection" => Organization::COLLECTION, 
								"type" => Organization::TYPE_GROUP,
								"email" => $res["email"],
								"creator" => $user["id"],
								"scope" => $_POST["scope"],
								"preferences"=>array("private"=>true,"isOpenData"=>true, "isOpenEdition"=>false),
								"created" => time(),
								"source" => array(
									"key" => Yii::app()->session["costum"]["slug"],
									"keys" => array(Yii::app()->session["costum"]["slug"]),
									"origin" => "costum")
								);

				$where = array( "source.key" => Yii::app()->session["costum"]["slug"],
							"email"=> $orga["email"] );
				$findSameEmail = PHDB::findOne(Organization::COLLECTION, $where, array("_id", "name", "email", "source"));
				if(empty($findSameEmail) || (isset($findSameEmail["source.toBeValidated"]) && $findSameEmail["source.toBeValidated"]==true)){
					$orga["source"]["toBeValidated"]=true;
				}
				$orga = Element::prepData($orga) ;
                PHDB::insert(Organization::COLLECTION, $orga );

				$slug=Slug::checkAndCreateSlug($orga["name"]);
    			Slug::save(Organization::COLLECTION,(String)$orga["_id"],$slug);
    			PHDB::update(Organization::COLLECTION,
							array("_id" => (String)$orga["_id"]) , 
							array('$set' => array("slug" => $slug)));
    			
    			// Cas ou un groupe est crée mais le code postal est commun donc mail commun
				if(!empty($findSameEmail)){
					if(!isset($findSameEmail["source.toBeValidated"]) || empty($findSameEmail["source.toBeValidated"])){
						// TODO MAIL TO FRAMA
						$sub=explode("@", $findSameEmail["email"])[0];
						$paramsMails = array("tplMail" => $sub."-subscribe@listes.transition-citoyenne.org",
											"tplObject" => "sub ".$sub,
											"tpl" => "basic",
											"fromMail"=>$userMail,
											"html" => $html);
						Mail::createAndSend($paramsMails);
						if(isset($_POST["msgGroup"])){
								$tplMsgList='<span><b>'.$_POST["name"].'</b> a écrit ce message :</span><br/><span style="padding:15px; margin-top:10px;background-color:#f9f9f9; border:1px solid #eee; border-radius: 10px;float:left;">'.$_POST["msgGroup"].'</span>';
						
								$paramsMails = array("tplMail" => $findSameEmail["email"],
									"tplObject" => "Message d'un nouveau membre sur le ".$sub,
									"tpl" => "basic",
									"fromMail"=>$userMail,
									"html" => $tplMsgList);

							Mail::createAndSend($paramsMails);
						}
					}else{
						$sendTopacteCreate=true;
					}
				}else
					$sendTopacteCreate=true;
			}
			$res["result"] = true;

			if( $sendTopacteCreate===true) {
				$html = "<span>Une nouvelle personne a crée le collectif : ".$name."</span><br/><span>Mail de la liste à créer : ".$res["email"]."</span><br/><span>Mail de l'utilisateur : ".$_POST["email"]."</span><br/>";
    				$paramsMails = array("tplMail" => Yii::app()->session["costum"]["admin"]["email"],
									"tplObject" => "Un nouveau membre ".$_POST["name"],
									"tpl" => "basic",
									"html" => $html);
				Mail::createAndSend($paramsMails);
			}

			//Inscription newsletter pacte
			$paramsMails = array("tplMail" => "collectifs-locaux-subscribe@listes.transition-citoyenne.org",
							"tplObject" => "sub ".$_POST["email"],
							"tpl" => "basic",
							"fromMail"=>$_POST["email"],
							"html" => "");
			Mail::createAndSend($paramsMails);
			
			/*$ch = curl_init();
			//$curl = curl_init();

			curl_setopt_array($ch, array(
				CURLOPT_HTTPHEADER => array('Accept: application/json', 'Content-Type: application/json', 'api-key: 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'),
			  	CURLOPT_URL => "https://api.sendinblue.com/v3/contacts",
			  	CURLOPT_RETURNTRANSFER => true,
			  	CURLOPT_ENCODING => "",
			  	CURLOPT_MAXREDIRS => 10,
			  	CURLOPT_TIMEOUT => 30,
			  	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  	CURLOPT_CUSTOMREQUEST => "POST",
			  	CURLOPT_POSTFIELDS => "{\"email\":\"".$_POST["email"]."\"}"//json_encode(array("email" => $_POST["email"]))
			));
		    $resultat = curl_exec($ch);
		    $err = curl_error($ch);
			curl_close($ch);
		    if ($err) {
		    	$res = array(
					"result" => false,
					"msg"  => "Un problème est survenu lors de votre inscription à la mailing du pacte:".$msg." / Contacter l'admin"
				);
		        //print curl_error($ch);
		    }*/
			
		}

		
		Rest::json($res);
	}
}