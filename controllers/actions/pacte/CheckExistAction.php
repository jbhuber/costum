<?php
class CheckExistAction extends CAction
{
	public function run(){ 	
		
		$res = array(
			"result" => false,
			"msg"  => ""
		);

		if(!empty($_POST["scope"])){
			$name = "";
			foreach ($_POST["scope"] as $key => $value) {
				$checkScope=$key;
				$postalCode=$value["postalCode"];
			}
			$name = trim($name);
			$where = array( "source.key" => "siteDuPactePourLaTransition",
							"email" => "pacte-".mb_strtolower($postalCode)."@listes.transition-citoyenne.org",
							"source.toBeValidated" => array('$exists' => false )  );
			$exist = PHDB::findOne(Organization::COLLECTION, $where);


			if(!empty($exist)){
				$res["exist"] = true;
				$res["elt"] = $exist;
			}else{
				$res["exist"] = false;
			}
		}
		Rest::json($res) ;
	}
}