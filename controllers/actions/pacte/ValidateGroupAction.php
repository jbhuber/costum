<?php
class ValidateGroupAction extends CAction
{
    public function run($id=null, $type=null)
    {
    	$query=array( "source.toBeValidated" => 1);
    	$res= PHDB::update ($_POST["type"], 
                                    array("_id" => new MongoId($_POST["id"])), 
                                    array('$unset'=>$query));
    	Rest::json(array("result"=>true));
                   
    }
}