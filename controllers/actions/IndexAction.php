<?php
class IndexAction extends CAction
{
    public function run($id=null,$type=null,$slug=null)
    { 	
    	$activeCOs = array("cocampagne");
    	$this->getController()->layout = (in_array(@$slug, $activeCOs ) || in_array(@$id, $activeCOs )) ? "//layouts/mainSearch2" : "//layouts/mainSearch";
		//$params = CO2::getThemeParams();
    	//Yii::app()->session['paramsConfig']=$params;
    	$elParams = ["shortDescription", "profilImageUrl", "profilThumbImageUrl", "profilMediumImageUrl","profilBannerUrl", "name", "tags", "description","costum"];

    	//ex : http://127.0.0.1/ph/costum/co/index/slug/cocampagne
    	//le slug appartient à un élément
    	if(isset($slug)){
    		$el = Slug::getElementBySlug($slug, $elParams );
    		if(@$el["el"]["costum"]){
    			$id = (@$el["el"]["costum"]['id']) ? $el["el"]["costum"]['id'] : $el["el"]["costum"]['slug'];
    		}
    		else {
    			//generate the costum tag on the element with a default COstum template 
    			//can be changed in the costum admin 
    			$id = "cocampagne";	
    		}

    	} 
    	//ex : http://127.0.0.1/ph/costum/co/index/id/xxxx/type/organizations
    	//l'id et le type permettent de retrouver un élément
    	else if(isset($type) && isset($id)){
    		$el = array("el"=>Element::getByTypeAndId($type, $id, $elParams ));
    		if(@$el["el"]["costum"]){
    			$id = (@$el["el"]["costum"]['id']) ? $el["el"]["costum"]['id'] : $el["el"]["costum"]['slug'];
    		}
    		
    	}
    	//ex : http://127.0.0.1/ph/costum/co/index/id/cocampagne
    	//l'id est celui du costum
    	else 
    		$id=(!empty($id)) ? $id : @$_GET["id"];

    	// var_dump($id);
     //    	exit;
    	if(!empty($id)){
			if(strlen($id) == 24 && ctype_xdigit($id)  )
		        $c = PHDB::findOne( Costum::COLLECTION , array("_id"=> new MongoId($id)));
		    else 
		   		$c = PHDB::findOne( Costum::COLLECTION , array("slug"=> $id));
	   	}else if(@$_GET["host"]){
	   		$c = PHDB::findOne( Costum::COLLECTION , array("host"=> $_GET["host"]));
	   	}

	   	// var_dump($c);
     //    	exit;

	   	if(@$c && !empty($c)){ 
            if(isset($c["redirect"])){
               // $slug=explode(".", $_GET["el"])[1];
                $redirect = PHDB::findOne( Costum::COLLECTION , array("slug"=> $c["redirect"]));
                $url = (isset($redirect["host"])) ? "https://".$redirect["host"] : Yii::app()->createUrl("/costum/co/index/id/".$redirect["slug"]);
                header('Location: '.$url);//Yii::app()->createUrl("/costum/co/index/id/".$redirect["slug"]));
                exit;
            }
	   		if(!isset($slug)){
        		$el = Slug::getElementBySlug($c["slug"], $elParams );
        		$slug = $c["slug"];
	   		}
	   		$el["slug"] = $slug;
        	//var_dump($el);

        	if(!empty($el["el"]))
        	{
        		$element=array(
        		    "contextType" => $el["type"],
	                "contextId" => $el["id"],
	                "title"=> $el["el"]["name"],
	                "description"=> @$el["el"]["shortDescription"],
                   	"tags"=> @$el["el"]["tags"],
                   	"isCostumAdmin"=>false,
                   	"assetsUrl"=> Yii::app()->getModule($this->getController()->module->id)->getAssetsUrl(true),
                   	"url"=> "/costum/co/index/id/".$c["slug"] );

        		if( @$el["el"]["profilImageUrl"] ) 
        			$c["logo"] = Yii::app()->createUrl($el["el"]["profilImageUrl"]);
        		 if(@$c["logoMin"]){
		            $c["logoMin"] = Yii::app()->getModule( $this->getController()->module->id )->getAssetsUrl().$c["logoMin"];
		        }
        		$c["admins"]= Element::getCommunityByTypeAndId($el["type"], $el["id"], Person::COLLECTION,"isAdmin");
        		$links = (!empty($el["links"]) ? $el["links"] : null );
        		$c["isMember"]= Link::isLinked($el["id"], $el["type"], Yii::app()->session["userId"],$links);
	        	if(@Yii::app()->session["userIsAdmin"] && !@$c["admins"][Yii::app()->session["userId"]])
	            	$c["admins"][Yii::app()->session["userId"]]=array("type"=>Person::COLLECTION, "isAdmin"=>true);
	    		if(@$c["admins"][Yii::app()->session["userId"]])
	    			Yii::app()->session['isCostumAdmin']=true;

        		$c = array_merge( $c , $element );
        		//possibly overload the costum template 

        		if( isset( $el["el"]["costum"] ) && $c["slug"] == @$el["el"]["costum"]["slug"]  ){
        			
        			unset($el["el"]["costum"]['id']);
        			unset($el["el"]["costum"]['slug']);
        			$c = combine($c,$el["el"]["costum"] ); 
        		}

	        	/* metadata */
                //print_r($this->getController()->module);exit;
                //Costum::initMetaData($c, $this->getController()->module);
		        if(isset($c["metaDesc"]))
                    $shortDesc=$c["metaDesc"];
                else{
                    $shortDesc =  @$c["shortDescription"] ? $c["shortDescription"] : "";
                        if($shortDesc=="")
                            $shortDesc = @$c["description"] ? $c["description"] : "";
                }
                $this->getController()->module->description = $shortDesc;
                $this->getController()->module->pageTitle = (isset($c["metaTitle"])) ? $c["metaTitle"] : @$c["title"];
                if(isset($c["metaKeywords"]))
                    $this->getController()->module->keywords = $c["metaKeywords"];
                else
                   $this->getController()->module->keywords = (@$c["tags"]) ? implode(",", @$c["tags"]) : "";
                if (isset($c["favicon"])) {
                    $mod = $this->getController()->module->id;
                    //ex images can be given 
                    if( substr_count($c["favicon"], '#') ){
                        $pieces = explode("#", $c["favicon"]);
                        $mod = $pieces[0];
                        $c["favicon"] = $pieces[1];
                    }
                    $this->getController()->module->favicon = Yii::app()->getModule( $mod )->getAssetsUrl().$c["favicon"];
                }
                if (@$c["metaImg"]) {
                    $mod = $this->getController()->module->id;
                    //ex images can be given 
                    if( substr_count($c["metaImg"], '#') ){
                        $pieces = explode("#", $c["metaImg"]);
                        $mod = $pieces[0];
                        $c["metaImg"] = $pieces[1];
                    }
                    $this->getController()->module->image = Yii::app()->getModule( $mod )->getAssetsUrl().$c["metaImg"];
                }
                else if(@$c["logo"]){
                    $this->getController()->module->image = $c["logo"];
                }
                if(@$c["logo"]){
                    $this->getController()->module->image = $c["logo"];
                }
                $this->getController()->module->relCanonical = (@$c["host"]) ? $c["host"] : Yii::app()->createUrl($c["url"]);   
                    
		      	// Besoin d'approfindir ce sujet des jsons et de leurs utilisations
		      	// Ici je l'ai utilisé pour injecter des tags spécifiques au pacte / on pourrait aussi penser au catégories d'événement
                //var_dump($c["tags"]); exit;
		      	if(isset($c["json"])){
		      		foreach($c["json"] as $k => $v){
                        
		      			$c[$k]=Costum::getContextList($c["slug"],$v);
		      			if($k=="tags")
                            $c["request"]["searchTag"]=$c["tags"];
		      				//$c["request"]["searchTag"]=[$c["tags"]];			
		      		}
		      	}
                
		      	if( isset($c["searchTag"]) ) 
		      		$c["request"]["searchTag"]=[$c["searchTag"]];
		      	if(isset(Yii::app()->session["userId"])){
		      	 	$c=Costum::checkUserPreferences($c, Yii::app()->session["userId"]);
		      	}
		      	if(isset($c["searchTag"])) $c["request"]["searchTag"]=[$c["searchTag"]];
		      	if(isset($c["scopeSelector"]))
		      		$c=Costum::initScopeSelector($c);
		      	/* metadata */
        	}
        	
    		$c["request"]["sourceKey"] = [$c["slug"]];
		    Yii::app()->session['costum'] = $c;
		    //if(!@Yii::app()->session['paramsConfig'] || empty(Yii::app()->session['paramsConfig'])) 
    		Yii::app()->session['paramsConfig'] = CO2::getThemeParams(); 

	    	Yii::app()->session["paramsConfig"]=Costum::filterThemeInCustom(Yii::app()->session["paramsConfig"]);
    	}//else
    		//Yii::app()->session["costum"] = null;
    	
	  	if( @Yii::app()->session['costum']["welcomeTpl"])
        	$this->getController()->render( Yii::app()->session["costum"]["welcomeTpl"],array( "el" => @$el ) );
	  	else 
	  		$this->getController()->render("index",array( "el" => @$el["el"] ) );

	  	//contient des éléments partagé par tout les costums
	  	//echo $this->renderPartial("costum.views.common",array("el"=>$el),true);

    }
}

function combine($a1, $a2) {
    foreach($a2 as $k => $v) {
        if(is_array($v)) {
            if(!isset($a1[$k]))
                $a1[$k] = null;

            $a1[$k] = combine($a1[$k], $v);
        } else {
            $a1[$k] = $v;
        }
    }
    return $a1;
}