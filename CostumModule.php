<?php
/**
 * Communect Module
 *
 * @author Tibor Katelbach <oceatoon@mail.com>
 * @version 0.0.3
 *
*/

class CostumModule extends CWebModule {

	private $_assetsUrl;

	private $_version = "v0.1.0";
	private $_versionDate = "17/01/2019";
	private $_keywords = "economy, collaborative, classifieds, ressources, needs, services, competence, exchange, module,opensource,CO,communecter";
	private $_description = "Create your community tool";
	private $_pageTitle = "Welcome on COstum board";
	private $_image = null;
	private $_favicon = null;
	private $_relCanonical = null;

	public function getVersion(){return $this->_version;}
	public function getVersionDate(){return $this->_versionDate;}
	public function getKeywords(){return $this->_keywords;}
	public function getDescription(){return $this->_description;}
	public function getPageTitle(){return $this->_pageTitle;}
	public function getImage(){return $this->_image;}
	public function getFavicon(){return $this->_favicon;}
	public function getRelCanonical(){return $this->_relCanonical;}

	public function setPageTitle($title){ $this->_pageTitle = $title; }
	public function setDescription($desc){ $this->_description = $desc; }
	public function setImage($image){ $this->_image = $image; }
	public function setKeywords($keywords){ $this->_keywords = $keywords; }
	public function setFavicon($favicon){ $this->_favicon = $favicon; }
	public function setRelCanonical($relCanonical){ $this->_relCanonical = $relCanonical; }

	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		Yii::app()->setComponents(array(
		    'errorHandler'=>array(
		        'errorAction'=>'/'.$this->id.'/error'
		    )
		));
		
		Yii::app()->homeUrl = Yii::app()->createUrl($this->id);
		
		//sudo ln -s co2 network
		Yii::app()->theme = "CO2";
		Yii::app()->session["theme"] == "CO2";
//		Yii::app()->params['customParams'] = ( @Yii::app()->session["customParams"] ) ? Yii::app()->session["customParams"] : @$_GET["custom"];
		
		if(@Yii::app()->request->cookies['lang'] && !empty(Yii::app()->request->cookies['lang']->value))
        	Yii::app()->language = (string)Yii::app()->request->cookies['lang'];
        else 
			Yii::app()->language = (isset(Yii::app()->session["lang"])) ? Yii::app()->session["lang"] : 'fr';

		//Yii::app()->language = (isset(Yii::app()->session["lang"])) ? Yii::app()->session["lang"] : 'fr';
		Yii::app()->params["module"] = array(
			"name" => self::getPageTitle(),
			"parent" => "co2",
			"costumId"=>"",
			"overwrite" => array(
				"views" => array(),
				"assets" => array(),
				"controllers" => array(),
			));
		// import the module-level models and components
		$this->setImport(array(
			'citizenToolKit.models.*',
			'map.models.*',
			'news.models.*',
			'dda.models.*',
			'eco.models.*',
			'places.models.*',
			'chat.models.*',
			'interop.models.*',
			'survey.models.*',
			Yii::app()->params["module"]["parent"].'.models.*',
			Yii::app()->params["module"]["parent"].'.components.*',
			$this->id.'.models.*',
			$this->id.'.components.*',
			$this->id.'.messages.*'
		));

		// var_dump("HERERE"); exit;
		// var_dump(Yii::app()->session["costum"]); exit;
		// $this->setImport(array(
		// 	'costum.models.ctenat.*',
		// ));
		/*$this->components =  array(
            'class'=>'CPhpMessageSource',
            'basePath'=>'/messages'
        );*/
	}

	public function beforeControllerAction($controller, $action)
	{
		if (parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
	//private $_assetsUrl;

	public function getAssetsUrl()
	{
		if ($this->_assetsUrl === null)
	        $this->_assetsUrl = Yii::app()->getAssetManager()->publish(
	            Yii::getPathOfAlias($this->id.'.assets') );
	    return $this->_assetsUrl;
	}

	public function getParentAssetsUrl()
	{
		return ( @Yii::app()->params["module"]["parent"] ) ?  Yii::app()->getModule( Yii::app()->params["module"]["parent"] )->getAssetsUrl()  : self::getAssetsUrl();
	}
	
	/**
	 * Retourne le theme d'affichage de communecter.
	 * Si option "theme" dans paramsConfig.php : 
	 * Si aucune option n'est précisée, le thème par défaut est "ph-dori"
	 * Si option 'tpl' fixée dans l'URL avec la valeur "iframesig" => le theme devient iframesig
	 * Si option "network" fixée dans l'URL : theme est à network et la valeur du parametres fixe les filtres d'affichage
	 * @return type
	 */
	public function getTheme() {
		//$theme = "network";
		$theme = (@Yii::app()->session["theme"]) ? Yii::app()->session["theme"] : "CO2";
		//$theme = "notragora";
		if (!empty(Yii::app()->params['theme'])) {
			$theme = Yii::app()->params['theme'];
		} else if (empty(Yii::app()->theme)) {
			$theme = (@Yii::app()->session["theme"]) ? Yii::app()->session["theme"] : "CO2";
			//$theme = "network";
			//$theme = "notragora";
		}

		if(@$_GET["tpl"] == "iframesig"){ $theme = $_GET["tpl"]; }

		if(@$_GET["custom"]) {
            $theme = "CO2";
            //Yii::app()->params['customParams'] = $_GET["network"];
        }
        Yii::app()->session["theme"] = $theme;
		return $theme;
	}
}
