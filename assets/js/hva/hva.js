var dateHVA = {

} ;

function addDateHVA(dateH, keyElt, elt){
	if(typeof dateHVA[dateH] == "undefined")
		dateHVA[dateH] = {} ;
	if(typeof dateHVA[dateH][keyElt] == "undefined")
		dateHVA[dateH][keyElt] = {} ;
	dateHVA[dateH][keyElt] = elt ;
}

directory.showResultsDirectoryHtml = function ( data, contentType, size, edit, viewMode){ //size == null || min || max
	//mylog.log("START -----------showResultsDirectoryHtml :",Object.keys(data).length +' elements to render');
	mylog.log("HVA showResultsDirectoryHtml data", data,"size",  size, "contentType", contentType);
	//mylog.log(" dirLog",directory.dirLog);
	var str = "";
	directory.colPos = "left";
	if(typeof data == "object" && data!=null){
		if(typeof page != "undefined" && page == "agenda"){
			// var keyGG = params.startDayNum + ' ' + params.startDay + ' ' + params.startMonth ;
			// var keyGG = moment(params.startDate).format('l');
			// if(typeof dateHVA[keyGG] == "undefined")
			// 	dateHVA[keyGG] = {} ;
			// if(typeof dateHVA[keyGG][params.id] == "undefined")
			// 	dateHVA[keyGG][params.id] = {} ;
			// dateHVA[keyGG][params.id] = params ;

			$.each(data, function(kElt,vElt){
				var keyGG = moment(vElt.startDate).format('l');
				// if(typeof dateHVA[keyGG] == "undefined")
				// 	dateHVA[keyGG] = {} ;
				// if(typeof dateHVA[keyGG][kElt] == "undefined")
				// 	dateHVA[keyGG][kElt] = {} ;
				// dateHVA[keyGG][kElt] = vElt ;
				addDateHVA(keyGG, kElt, vElt);
				var jour = moment(keyGG).date();
				var endGG = moment(vElt.endDate).format('l');

				mylog.log("HVA Data end", kElt, jour, keyGG, endGG, moment().dates(jour).format('l'));
				while(moment(moment().dates(jour).format('l')).isSameOrBefore(endGG) ) {
					addDateHVA(moment().dates(jour).format('l'), kElt, vElt);
					jour++;
					mylog.log("HVA Data end", kElt, jour, keyGG, endGG, moment().dates(jour).format('l'));
				}
				
			});
			mylog.log("HVA vListElt2 dateHVA", dateHVA);
			if( typeof dateHVA != "undefined"){
				mylog.log("HVA vListElt dateHVA", dateHVA);
				$.each(dateHVA, function(dateElt,vListElt){
					str += "<div class='col-xs-12 margin-top-5' style='background-color : #3b9ca263; '><center><h5>"+moment(dateElt).locale("fr").format('dddd DD MMMM')+"</h5></center></div>";
					mylog.log("HVA vListElt", vListElt);
					str += listHVA(vListElt, contentType, size, edit, viewMode) ;
				});
			}

		} else{
			mylog.log("HVA vListElt data", data);
			str += listHVA(data, contentType, size, edit, viewMode) ;
		}
        
      } //end each
      mylog.log("END -----------showResultsDirectoryHtml ("+str.length+" html caracters generated)")
      return str;
 };



directory.lightPanelHtml = function(params){
	mylog.log("HVA lightPanelHtml 2", params);
	var linkAction = ( $.inArray(params.type, ["poi","classifieds","ressources"])>=0 ) ? " lbh-preview-element" : " lbh";
	//linkAction = "lbh-preview-element";

	var onepageKey = (typeof CO2params != "undefined") ? CO2params["onepageKey"][0] : ".co";

	params.htmlIco ="!<i class='fa "+ params.ico +" fa-2x letter-"+params.color+"'></i>";

	if(params.targetIsAuthor){   
		nameAuthor=params.target.name;
		authorType=params.target.type;
		authorId=params.target.id;
	}else if(params.author){
		nameAuthor=params.author.name;  
		authorType="citoyens";
		authorId=params.author.id;
	} 
  
	var grayscale = ( ( notNull(params.isInviting) && params.isInviting == true) ? "grayscale" : "" ) ;
    var tipIsInviting = ( ( notNull(params.isInviting) && params.isInviting == true) ? trad["Wait for confirmation"] : "" ) ;
    var descHVA = "" ;
    var heightHva = "";
    if(typeof params.shortDescription != "undefined" && params.shortDescription != "" && params.shortDescription != null){
       descHVA = "<br><span class='pull-left padding-5 col-xs-12'>"+params.shortDescription+"</span>";
       heightHva = " height: 90px; ";
    }
        

	str = "";
	str += "<a href='"+params.hash+"' class='margin-top-15 iconType "+linkAction+"'><div class='col-xs-10 margin-bottom-5 tooltips "+params.elRolesList+" "+grayscale+" ' " +
				"data-toggle='tooltip' data-placement='left' data-original-title='"+tipIsInviting+"' "+
				"style='background-color : #ffa500d1; "+heightHva+"'" +
				"id='entity"+params.id+"'>";

		mylog.log("tagListC ", typeof costum.app["#"+page], typeof costum.app["#"+page].tagsList);
		var tagListC = ( ( typeof costum.app["#"+page] != "undefined" && typeof costum.app["#"+page].tagsList != "undefined") ? costum.app["#"+page].tagsList : "tags" ) ;
		mylog.log("tagListC ", tagListC);
		if(typeof params.tags != "undefined" && params.tags != null && params.tags.length > 0){
			str += "<div class='pull-left' style='margin-left: -15px;' >";
			$.each(params.tags, function(kT,vT){
				mylog.log("tagListC tags", vT, costum.paramsData );

				if ( typeof costum.paramsData != "undefined"  && 
					typeof costum.paramsData[tagListC] != "undefined" && 
					typeof costum.paramsData[tagListC][vT] != "undefined"){
					str += "<div class='padding-10 pull-left' style=' height : 100%; width : 40px; background-color : "+costum.paramsData[tagListC][vT].color+"'> &nbsp ";

					//str += vT ;
					str += "</div> ";
				}
			});
			str += "</div>";
		}

		var hour =  "";
		if(params.type == "events"){
			hour =  moment(params.startDate).format('HH:mm');
		}

		str += "<div class='pull-left padding-10' style='font-size: 16px;' ><b>"+hour+ " " + params.name + "</b></div>";
		// if(params.type == "events"){
		// 	 var dateFormated = directory.getDateFormated(params);
	 //        var countSubEvents = ( params.links && params.links.subEvents ) ? "<br/><i class='fa fa-calendar'></i> "+Object.keys(params.links.subEvents).length+" "+trad["subevent-s"]  : "" ;
	 //        // str += dateFormated+countSubEvents;
	 //        str += "<div class='pull-right padding-10'  >" + dateFormated+countSubEvents + "</div>";
		// 	// str += "<div class='pull-left padding-10'  >" + moment(params.startDate, "HH:mm").format("HH:mm") + "</div>";
		// }

    // if(typeof params.shortDescription != "undefined" && params.shortDescription != "" && params.shortDescription != null)
    //     str += "<br><span class='pull-right padding-5'>"+params.shortDescription+"</span>";
	
		str += descHVA ;
	

	
	str += "</div>";
	var nameCity = ( typeof params.cityName != "undefined" && params.cityName != null && params.cityName != "" ) ? params.cityName : trad.notSpecified;
	str += "<div class='col-xs-2 padding-10 margin-bottom-5' style='background-color : #cc7439bf; color : black; "+heightHva+"' >" + nameCity + "</div>";
  str +="</a>";
  	mylog.log("HVA lightPanelHtml 2 str", str);
	return str;
};


function listHVA(data, contentType, size, edit, viewMode){
	mylog.log("HVA2 listHVA 2", data, contentType, size, edit, viewMode);
	var str ="";
	$.each(data, function(i, params) {
	          if(i!="count"){
	            //if(directory.dirLog) mylog.log("params", params, typeof params);
	            mylog.log("params", params);
	            //mylog.log("params interoperability", location.hash.indexOf("#interoperability"));

	            if ((typeof(params.id) == "undefined") && (typeof(params["_id"]) !== "undefined")) {
	              params.id = params['_id'];
	            } else if (typeof(params.id) == "undefined" && location.hash.indexOf("#interoperability") >= 0) {
	              params.id = Math.random();
	              params.type = "poi";
	            }
	            mylog.log(params.sorting);
	            //mylog.log("--->>> params", params["name"] , params.name, params.id, params.type );
	            //mylog.log("--->>> params.id", params.id, params["_id"], notNull(params["_id"]), notNull(params.id));

	            if(notNull(params["_id"]) || notNull(params.id)){

	              itemType=(contentType) ? contentType : params.type;
	              mylog.log("params itemType", itemType);
	              if( itemType ){ 
	                 // if(directory.dirLog) mylog.warn("TYPE -----------"+contentType);
	                  mylog.log("HVA showResultsDirectoryHtml 3", params);
	                  //if(directory.dirLog) mylog.log("itemType",itemType,"name",params.name,"dyFInputs.get( itemType )",dyFInputs.get( itemType ));
	                  
	                  var typeIco = i;
	                  params.size = size;
	                  params.id = getObjectId(params);
	                  mylog.log(params.id);
	                  params.name = notEmpty(params.name) ? params.name : "";
	                  params.description = notEmpty(params.shortDescription) ? params.shortDescription : 
	                                      (notEmpty(params.message)) ? params.message : 
	                                      (notEmpty(params.description)) ? params.description : 
	                                      "";

	                  //mapElements.push(params);
	                  //alert("TYPE ----------- "+contentType+":"+params.name);
	                  if(typeof edit != "undefined" && edit != false)
	                    params.edit = edit;
	                  
	                  if ( params.type && typeof typeObj.classifieds != "undefined" && $.inArray(params.type, typeObj.classifieds.subTypes )>=0  ) {
	                    itemType = "classifieds";
	                  } else if(typeof( typeObj[itemType] ) == "undefined") {
	                    itemType="poi";
	                  }

	                  if( dyFInputs.get( itemType ) == null){
	                    itemType="poi";
	                  }

	                  typeIco = itemType;
	                  if(directory.dirLog) mylog.warn("itemType",itemType,"typeIco",typeIco);

	                  if(typeof params.typeOrga != "undefined")
	                    typeIco = params.typeOrga;
	                  if(typeof params.typeClassified != "undefined")
	                    typeIco = params.typeClassified;
	                  var obj = (dyFInputs.get(typeIco)) ? dyFInputs.get(typeIco) : typeObj["default"] ;
	                  params.ico =  "fa-"+obj.icon;
	                  params.color = obj.color;
	                  if(params.parentType){
	                      if(directory.dirLog) mylog.log("params.parentType",params.parentType);
	                      var parentObj = (dyFInputs.get(params.parentType)) ? dyFInputs.get(params.parentType) : typeObj["default"] ;
	                      params.parentIcon = "fa-"+parentObj.icon;
	                      params.parentColor = parentObj.color;
	                  }
	                  if((typeof searchObject.countType != "undefined" && searchObject.countType.length==1) && params.type == "classifieds" && typeof params.category != "undefined" && typeof modules[params.typeClassified] != "undefined"){
	                    getIcoInModules=modules[params.typeClassified].categories;
	                    params.ico = (typeof getIcoInModules.filters != "undefined" && typeof getIcoInModules.filters[params.category] != "undefined") ?
	                                 "fa-" + getIcoInModules.filters[params.category]["icon"] : "fa-bullhorn";
	                  }
	                  if(params.type=="poi" 
	                    && typeof modules.poi != "undefined" 
	                    && typeof modules.poi.categories != "undefined" 
	                    && typeof modules.poi.categories.filters != "undefined"
	                    && typeof modules.poi.categories.filters[params.typePoi] != "undefined"
	                    && typeof modules.poi.categories.filters[params.typePoi].icon != "undefined")
	                    params.ico="fa-"+modules.poi.categories.filters[params.typePoi].icon;
	                  params.htmlIco ="<i class='fa "+ params.ico +" fa-2x bg-"+params.color+"'></i>";

	                  params.useMinSize = typeof size != "undefined" && size == "min";

	                  params.imgProfil = ""; 
	                  if(!params.useMinSize){
	                      params.imgProfil = "<i class='fa fa-image fa-2x'></i>";
	                      params.imgMediumProfil = "<i class='fa fa-image fa-2x'></i>";
	                  }
	                  if("undefined" != typeof directory.costum && notNull(directory.costum)  
	                    && typeof directory.costum.results != "undefined" 
	                    && typeof directory.costum.results[params.type] != "undefined" 
	                    && typeof directory.costum.results[params.type].defaultImg != "undefined")

	                  params.imgMediumProfil= "<img class='img-responsive' onload='directory.checkImage(this);' src='"+assetPath+directory.costum.results[params.type].defaultImg+"'/>";
	     
	              if("undefined" != typeof params.profilMediumImageUrl && params.profilMediumImageUrl != "")
	                  params.imgMediumProfil= "<img class='img-responsive' onload='directory.checkImage(this);' src='"+baseUrl+params.profilMediumImageUrl+"'/>";
	                  
	              if("undefined" != typeof params.profilThumbImageUrl && params.profilThumbImageUrl != "")
	                  params.imgProfil= "<img class='shadow2' src='"+baseUrl+params.profilThumbImageUrl+"'/>";


	                  params.imgBanner = ""; 
	                  if(!params.useMinSize)
	                    params.imgBanner = "<i class='fa fa-image fa-2x'></i>";

	     
	                  if (false && typeof params.addresses != "undefined" && params.addresses != null) {
	                    $.each(params.addresses, function(key, val){
	                //console.log("second address", val);
	                    var postalCode = val.address.postalCode ? val.address.postalCode : "";
	                    var cityName = val.address.addressLocality ? val.address.addressLocality : "";
	                  
	                    params.fullLocality += "<br>"+ postalCode + " " + cityName;
	                  });
	                }
	              params.type = dyFInputs.get(itemType).col;
	              params.urlParent = (notEmpty(params.parentType) && notEmpty(params.parentId)) ? 
	                            '#page.type.'+params.parentType+'.id.' + params.parentId : "";
	              // var urlImg = "/upload/communecter/color.jpg";
	              // params.profilImageUrl = urlImg;
	              

	              /*if(dyFInputs.get(itemType) && 
	                  dyFInputs.get(itemType).col == "poi" && 
	                  typeof params.medias != "undefined" && typeof params.medias[0].content.image != "undefined")
	              params.imgProfil= "<img class='img-responsive' src='"+params.medias[0].content.image+"'/>";
	              */
	              params.insee = params.insee ? params.insee : "";
	              params.postalCode = "", params.city="",params.cityName="";
	              if (params.address != null) {
	                  params.city = params.address.addressLocality;
	                  params.postalCode = params.cp ? params.cp : params.address.postalCode ? params.address.postalCode : "";
	                  params.cityName = params.address.addressLocality ? params.address.addressLocality : "";
	              }
	              params.fullLocality = params.postalCode + " " + params.cityName;

	              params.hash = '#page.type.'+params.type+'.id.' + params.id;

	              if(typeof params.slug != "undefined" && params.slug != "" && params.slug != null)
	                params.hash = "#@" + params.slug;

	              if(typeof networkJson != "undefined" && typeof networkJson.dataSrc != "undefined")
	                params.hash = params.source;

	              params.onclick = 'urlCtrl.loadByHash("' + params.url + '");';
	              if(params.type=="circuits")
	                  params.hash = '#circuit.index.id.' + params.id;
	                  params.onclick = 'urlCtrl.loadByHash("' + params.url + '");';

	              if( params.type == "poi" && params.source  && ( notNull(params.source.key) && params.source.key.substring(0,7) == "convert")) {
	                var interop_type = getTypeInteropData(params.source.key);
	                params.type = "poi.interop."+interop_type;
	              }
	              // params.tags = "";
	              params.elTagsList = "";
	              var thisTags = "";
	              if(typeof params.tags != "undefined" && params.tags != null){
	                $.each(params.tags, function(key, value){
	                  if(typeof value != "undefined" && value != "" && value != "undefined"){
	                    var tagTrad = typeof tradCategory[value] != "undefined" ? tradCategory[value] : value;
	                    thisTags += "<span class='badge bg-transparent text-red btn-tag tag' data-tag-value='"+slugify(value, true)+"' data-tag-label='"+tagTrad+"'>#" + tagTrad + "</span> ";
	                    // mylog.log("sluggify", value, slugify(value, true));
	                    params.elTagsList += slugify(value, true)+" ";
	                  }
	                });
	                params.tagsLbl = thisTags;
	              }else{
	                params.tagsLbl = "";
	              }
	              params.elRolesList = "";
	              var thisRoles = "";
	              params.rolesLbl = "";
	              if(typeof params.rolesLink != "undefined" && params.rolesLink != null){
	                thisRoles += "<small class='letter-blue'><b>"+trad.roleroles+" :</b> ";
	                thisRoles += params.rolesLink.join(", ");
	                $.each(params.rolesLink, function(key, value){
	                  if(typeof value != "undefined" && value != "" && value != "undefined")
	                    params.elRolesList += slugify(value)+" ";
	                });
	                thisRoles += "</small>";
	                params.rolesLbl = thisRoles;
	              }
	              params.updated   = notEmpty(params.updatedLbl) ? params.updatedLbl : null;
	              if(notNull(params.tobeactivated) && params.tobeactivated == true){
	                params.isInviting = true ;
	              }
	                  
	                  if(directory.dirLog) mylog.log("template principal",params,params.type, itemType);
	                  
	                  if( typeof domainName != "undefined" && domainName=="terla"){
	                    if(params.type=="circuits")
	                      str += directory.circuitPanelHtml(params);
	                    else
	                      str += directory.storePanelHtml(params);
	                    //template principal
	                  }else{
	                    mylog.log("template principal",params,params.type, itemType);
	                    if((((typeof directory.viewMode != "undefined" && directory.viewMode=="list" && !notNull(viewMode))) || (notNull(viewMode) && viewMode=="list"))  && $.inArray(params.type, ["citoyens","organizations","projects","events","poi","news","places","ressources","classifieds"] )>=0) 
	                     str += directory.lightPanelHtml(params);  
	                    else{ 
	                      if(params.type == "cities")
	                        str += directory.cityPanelHtml(params);  
	                    
	                      else if( $.inArray(params.type, ["citoyens","organizations","projects","poi","places","ressources"] )>=0) 
	                        str += directory.elementPanelHtml(params);  
	                    
	                      else if(params.type == "events"){
	                        if(typeof searchObject.countType != "undefined" && searchObject.countType.length > 1)
	                          str += directory.elementPanelHtml(params);
	                        else
	                          str += directory.eventPanelHtml(params);  
	                      }
	                      
	                      else if (params.type == "news")
	                        str += directory.newsPanelHtml(params);
	                      //else if($.inArray(params.type, ["surveys","actionRooms","vote","actions","discuss"])>=0 ) 
	                      //    str += directory.roomsPanelHtml(params,itemType);  
	                    
	                      else if(params.type == "classifieds"){
	                        if(typeof searchObject.countType != "undefined" && searchObject.countType.length > 1)
	                          str += directory.elementPanelHtml(params);  
	                        else
	                          str += directory.classifiedPanelHtml(params);
	                      }
	                      else if(params.type == "proposals" || 
	                              params.type == "actions" || 
	                              params.type == "resolutions" || 
	                              params.type == "rooms"){

	                        if(location.hash.indexOf('#dda') == 0)
	                          str += directory.coopPanelHtml(params,null,"S");
	                        else   
	                          str += directory.coopPanelHtml(params);
	                      }
	                      else if(params.type.substring(0,11) == "poi.interop")
	                        str += directory.interopPanelHtml(params);
	                      else if(params.type == "network")
	                        str += directory.network2PanelHtml(params);
	                        //str += directory.networkPanelHtml(params);
	                      else
	                        str += directory.defaultPanelHtml(params);
	                      }
	                  }
	                }
	            }else{
	              mylog.log("pas d'id");
	              if(contentType == "urls")
	                  str += directory.urlPanelHtml(params, i);
	              if(contentType == "contacts")
	                  str += directory.contactPanelHtml(params, i);
	            }
	             
	          }
	});
	return str ;
}

directory.getDateFormated = function(params, onlyStr){
    console.log("getDateFormated 2", params, onlyStr);
    var timezone = directory.get_time_zone_offset();
    

     //getDateFormated: function(params, onlyStr){
    
        params.startDateDB = notEmpty(params.startDate) ? params.startDate : null;
        params.startDay = notEmpty(params.startDate) ? moment(params.startDate).local().locale("fr").format("DD") : "";
        params.startMonth = notEmpty(params.startDate) ? moment(params.startDate).local().locale("fr").format("MM") : "";
        params.startYear = notEmpty(params.startDate) ? moment(params.startDate).local().locale("fr").format("YYYY") : "";
        params.startDayNum = notEmpty(params.startDate) ? moment(params.startDate).local().locale("fr").format("d") : "";
        params.startTime = notEmpty(params.startDate) ? moment(params.startDate).local().locale("fr").format("HH:mm") : "";
        //params.startDate = notEmpty(params.startDate) ? moment(params.startDate).local().locale("fr").format("DD MMMM YYYY - HH:mm") : null;
        
        params.endDateDB = notEmpty(params.endDate) ? params.endDate: null;
        params.endDay = notEmpty(params.endDate) ? moment(params.endDate).local().locale("fr").format("DD") : "";
        params.endMonth = notEmpty(params.endDate) ? moment(params.endDate).local().locale("fr").format("MM") : "";
        params.endYear = notEmpty(params.startDate) ? moment(params.endDate).local().locale("fr").format("YYYY") : "";
        params.endDayNum = notEmpty(params.startDate) ? moment(params.endDate).format("d") : "";
        params.endTime = notEmpty(params.endDate) ? moment(params.endDate).local().locale("fr").format("HH:mm") : "";
        //params.endDate   = notEmpty(params.endDate) ? moment(params.endDate).local().locale("fr").format("DD MMMM YYYY - HH:mm") : null;
        params.startDayNum = directory.getWeekDayName(params.startDayNum);
        params.endDayNum = directory.getWeekDayName(params.endDayNum);

        params.startMonth = directory.getMonthName(params.startMonth);
        params.endMonth = directory.getMonthName(params.endMonth);
        params.color="orange";
        

        var startLbl = (params.endDay != params.startDay) ? trad["fromdate"] : "";
        var endTime = ( params.endDay == params.startDay && params.endTime != params.startTime) ? " - " + params.endTime : "";
        mylog.log("params.allDay", !notEmpty(params.allDay), params.allDay);
       
        
        var str = "";
        var dStart = params.startDay + params.startMonth + params.startYear;
        var dEnd = params.endDay + params.endMonth + params.endYear;
        mylog.log("DATEE", dStart, dEnd);

        


        if(params.startDate != null){
          if(notNull(onlyStr)){
            str+="<span class='text-bold  no-margin'>";
            if(params.endDate != null && dStart != dEnd)
              str +=  '<small class="">'+trad.fromdate+'</small> ';
            str += '<span class="">'+params.startDay+'</span>';
            if(params.endDate == null || dStart == dEnd || (params.startMonth != params.endMonth || params.startYear != params.endYear))
              str += ' <small class="">'+ params.startMonth+'</small>';
            if(params.endDate == null || dStart == dEnd || params.startYear != params.endYear)
              str += ' <small class="">'+ params.startYear+'</small>';
            if(params.endDate != null && dStart != dEnd)
              str += ' <small class="">'+trad.todate+'</small> <span class="">'+params.endDay +'</span> <small class="">'+ params.endMonth +' '+ params.endYear+"</small>";
            str+="</span>";
            if(!notNull(params.allDay) || params.allDay != true){
              str +=  '<small class="margin-top-5"><b><i class="fa fa-clock-o"></i> '+
                                params.startTime+endTime+"</b></small>";
            }
          }else{ 
            str += '<span class="" style="">'+
                    startLbl+ " <b>" + params.startDayNum + ' ' +
                    params.startDay + ' ' + params.startMonth + " </b>" ;
                   // ' <small class="">' + params.startYear + '</small>';
                    if(!notNull(params.allDay) || params.allDay != true){
                      str +=  " à <b>" +params.startTime+ " </b>";
                    }              
            str +=  '</span> ';
          }
        }    
        
        if(params.endDate != null && dStart != dEnd && !notNull(onlyStr)){
            str += '<span class="" style="">'+
                        trad["todate"]+ " <b>" +params.endDayNum + ' ' +
                        params.endDay + ' ' + params.endMonth  + " </b>"
                       // ' <small class="">' + params.endYear + '</small>';
                        if(!notNull(params.allDay) || params.allDay != true){
                          str += " jusquà <b>" + params.endTime + " </b>" ;
                        }
              str +=  '</span> ';
        } 

        return str;
  };