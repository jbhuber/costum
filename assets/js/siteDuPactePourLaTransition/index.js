

/*[] Enregistre citoyens Ou tu regardes si il existe déjà mettre à jour ces infos 

[] Recherche si groupe existe par adress 
	[] si oui retour avec lemail framalist
	[] si non création du groupe
		[] nom automatique / code automatique / email framaliste auto
		[] envoie mail au pacte nouveau citoyen gérard - email framaliste
[] Retour data
	bool exist (pour le groupe) 
	object elt (frama) 
[] Retour subscribe auto mailing sendInblue
[] Mot si cest le premier à créer 
[] Mot avec email framaliste suscribe auto à la mailing*/
var pacte={
	initScopeObj : function(){
		$(".content-input-scope-pacte").html(scopeObj.getHtml("Code postal"));
		var params = {
			subParams : {
				cities : {
					type : ["cities"],
					country : ["FR", "RE", "GP", "GF", "MQ", "YT", "NC", "PM"],
					cp : true
				}
			}
		}
		scopeObj.initVar(params);
		scopeObj.init();
	}
};
scopeObj.onclickScope = function () {
	$(".content-input-scope-pacte .item-globalscope-checker").off().on('click', function(){
		var key = $(this).data("scope-value");
		if( typeof myScopes != "undefined" &&
				typeof myScopes.search != "undefined" &&
				typeof myScopes.search[key]  != "undefined" ){
					var scopeDF = myScopes.search[key];
					var nameZone = (typeof scopeDF.cityName != "undefined") ? scopeDF.cityName : scopeDF.name ;
		}
		scopeObj.selected={};
		scopeObj.selected[key] = myScopes.search[key];
		$.ajax({
	    	  	type: "POST",
	    	  	url: baseUrl+"/costum/pacte/checkexist",
	    	  	data: {
	    	  		scope:scopeObj.selected,
	    	  	},
	    	 	success: function(data){
	    	 			Login.runRegisterValidator();
	    		  		$(".form-register .msgGroup").remove();
	    		  		$('#modalRegister').modal("show");

	    		  		var params={};
	    		  		if(data.exist){
	    		  			params.elt=elt;
	    		  			params.msgHeader='<span class="text-justify">Bienvenue ! Plusieurs habitant.es de <b>'+nameZone+'</b> se sont déjà regroupé.es pour encourager la transition.<br/>'+ 
										'Remplissez ce formulaire pour entrer en contact avec eux !'+								
									'</span>';
							$(".form-register .emailRegister").after("<div class='msgGroup'>"+
								"<label class='letter-black'><i class='fa fa-pencil'></i> Ecrire un message au groupe</label>"+
								"<textarea class='form-control' id='textMsgGroup' name='textMsgGroup' placeholder='Ecrire un message'></textarea>"+
                    		"</div>");
	    		  		}else{
	    		  			params.elt=elt;
	    		  			params.msgHeader='<span class="text-justify">Vous êtes le.la premier.e à souhaiter encourager la transition de <b>'+nameZone+'</b> en portant le Pacte !<br/>'+
				'Remplissez ce formulaire, nous vous mettrons en contact avec les prochaines personnes de votre commune qui s’inscriront !<br/> Personne ne peut impulser le changement seul, n’hésitez pas à en parler autour de vous, à vos amis, vos voisins, ou sur <a href="https://www.facebook.com/PacteTransition" target="_blank" class="text-orange">Facebook</a> et <a href="https://twitter.com/PacteTransition" target="_blank" class="text-orange">Twitter</a> !'+
								'</span>';
	    		  		}
	    		  		if($(".form-register .info-register-form").length > 0)
	    		  			$('.form-register .info-register-form').html(params.msgHeader);
	    		  		else
							$('.form-register').find(".surnameRegister").before("<div class='col-xs-12 bg-purple text-white text-center info-register-form'>"+params.msgHeader+"</div>");
				}
		});
				
			
	});
};
Login.runRegisterValidator = function(params) { console.log("runRegisterValidatorCOOOOOOOOOOOOOOOOOOOOOOOOOOSTUME!!!!");
	var form3 = $('.form-register');
	var errorHandler3 = $('.errorHandler', form3);
	var createBtn = null;
	if($(".form-register .surnameRegister").length <= 0){
	$('.form-register').find(".nameRegister").before('<div class="surnameRegister">'+
                    '<label class="letter-black"><i class="fa fa-address-book-o"></i> Prénom</label>'+
                    '<input class="form-control" id="registerSurname" name="surnname" type="text" placeholder="Prénom"><br/>'+
               '</div>');
	}
	$('.form-register').find(".nameRegister label").html('<i class="fa fa-address-book-o"></i> Nom');
	$('.form-register').find(".nameRegister input").attr("placeholder","Nom");
	$('.form-register').find(".usernameRegister").remove();
	$('.form-register').find(".passwordRegister").remove();
	$('.form-register').find(".passwordAgainRegister").remove();
	

	form3.validate({
		rules : {
			name : {
				required : true,
				minlength : 4
			},
			email3 : {
				required : { 
				 	depends:function(){
				 		$(this).val($.trim($(this).val()));
				 		return true;
				 	}
				},
				email : true
			},
			agree: {
				minlength : 1,
				required : true
			}
		},

		messages: {
			agree: trad["mustacceptCGU"],
		},
		submitHandler : function(form) { 
			console.log("runRegisterValidator submitHandler", form);
			errorHandler3.hide();
			//createBtn.start();
			$(".createBtn").prop('disabled', true);
    		$(".createBtn").find(".fa").removeClass("fa-sign-in").addClass("fa-spinner fa-spin");
			var params = { 
			   "name" : $('.form-register #registerSurname').val()+" "+$('.form-register #registerName').val(),
			   "email" : $(".form-register #email3").val(),
               "pendingUserId" : pendingUserId,
            };
            if($('.form-register #isInvitation').val())
            	params.isInvitation=true;
            if(Object.keys(scopeObj.selected).length > 0){
		  		params.scope = scopeObj.selected;
		  	}
		  	if($('.form-register .msgGroup').length && $(".form-register #textMsgGroup").val() != "")
		  		params.msgGroup=$(".form-register #textMsgGroup").val();
		  	$.ajax({
	    	  type: "POST",
	    	  url: baseUrl+"/costum/pacte/register",
	    	  data: params,
	    	  success: function(data){
	    		  if(data.result) {
	    		  	//createBtn.stop();
					$(".createBtn").prop('disabled', false);
    				$(".createBtn").find(".fa").removeClass("fa-spinner fa-spin").addClass("fa-sign-in");
					$("#registerName").val("");
					$("#username").val("");
					$("#email3").val("");
					$("#password3").val("");
					$("#passwordAgain").val("");
					$("#passwordAgain").val("");
					$("#registerSurname").val("");
					$('#agree').prop('checked', false);
	    		  	if(typeof msgToFrama != "undefined"){
	    		  		//alert("ceci est le message à envoyer au group:"+msgToFrama);
	    		  	}
	    		  	/*if(typeof data.exist == "undefined" || !data.exist){
		    		  		$('.modal').modal('hide');
		    		  		$("#modalRegisterSuccess .modal-content").html(htmlStr);
			    		  	$("#modalRegisterSuccess").modal({ show: 'true' });
			    	}else{
			    		htmlStr='<div class="modal-header text-purple">'+
	                			'<h4 class="modal-title"><i class="fa fa-check"></i> Votre inscription dans XXX s\'est bien déroulée</h4>'+
	            			'</div>'+
				            '<div class="modal-body center text-dark">'+
				                '<span>Bienvenue ! Plusieurs habitant.es de [nom de la commune] se sont déjà regroupés pour encourager la transition.'+ 
				                	'Vous pouvez dores et déjà entrer en contact avec eux !'+

								'</span>'+  
				            	'<div class="modal-footer">'+
				                 	'<button type="button" class="btn btn-default letter-green" data-dismiss="modal"><i class="fa fa-check"></i> A très vite</button>'+
				            '</div>';
				    }*/
				    toastr.success("Merci, votre inscription a bien été prise en compte. Vous allez être mis en contact par mail avec votre collectif local");
    		  		$('.modal').modal('hide');
    		  		scopeObj.selected={};
    		  		//$("#modalRegisterSuccess .modal-content").html(htmlStr);
	    		  	//$("#modalRegisterSuccess").modal({ show: 'true' }); 
		    		  	// Hide modal if "Okay" is pressed
					    /*$('#modalRegisterSuccess .btn-default').click(function() {
					        mylog.log("hide modale and reload");
					        $('.modal').modal('hide');
					    	//window.location.href = baseUrl+'/#default.live';
					    	window.location.href = baseUrl+"/"+moduleId;
					    	window.location.reload();
					    });*/
	    		  	//}
	        		//urlCtrl.loadByHash("#default.directory");
	    		  }
	    		  else {
	    		  	toastr.error(data.msg);
	    		  	$('.modal').modal('hide');	    		  	
    		  		scopeObj.selected={};
	    		  }
	    	  },
	    	  error: function(data) {
	    	  	toastr.error(trad["somethingwentwrong"]);
	    	  	$(".createBtn").prop('disabled', false);
    			$(".createBtn").find(".fa").removeClass("fa-spinner fa-spin").addClass("fa-sign-in");
	    	  	//createBtn.stop();
	    	  },
	    	  dataType: "json"
	    	});
		    return false;
		},
		invalidHandler : function(event, validator) {//display error alert on form submit
			errorHandler3.show();
			$(".createBtn").prop('disabled', false);
    		$(".createBtn").find(".fa").removeClass("fa-spinner fa-spin").addClass("fa-sign-in");
			//createBtn.stop();
		}
	});
};
//jQuery(document).ready(function() {
Login.runRegisterValidator();
//});