if(notNull(costum.userPreferences) && notNull(costum.userPreferences.cities)){
    costum.scopeActivated=costum.scopeSelector[costum.userPreferences.cities[0]];
}
else if( notNull(localStorage) && notNull(localStorage.costum)){
    localStorageCostum=JSON.parse(localStorage.getItem("costum"));
    if(typeof localStorageCostum[costum["slug"]] != "undefined" && typeof localStorageCostum[costum["slug"]].cities != "undefined"){
        costum.scopeActivated=costum.scopeSelector[localStorageCostum[costum["slug"]].cities[0]];
      /*  keyScope=costum.scopeActivated.id;
        if(typeof costum.filters != "undefined"){
            costum.filters.scopes= {cities: keyScope };
        }else{
            costum.filters={scopes : {cities: keyScope}};            
        }*/
    }
}

themeObj.blockUi.setLoader=function(){
    if(notNull(costum)){
        logoLoader=costum.logo;
        if(typeof costum.css != "undefined" && typeof costum.css.loader !="undefined"){
            if(typeof costum.css.loader.ring1 != "undefined" && costum.css.loader.ring1.color != "undefined")
                color1=costum.css.loader.ring1.color;
            if(typeof costum.css.loader.ring2 != "undefined" && costum.css.loader.ring2.color != "undefined")
                color2=costum.css.loader.ring2.color;
        }
    }
    if(typeof costum.scopeActivated != "undefined"){
          themeObj.blockUi.processingMsg=
                '<div class="lds-css ng-scope">'+
                    '<div style="width:100%;height:100%" class="lds-dual-ring">'+
                        '<img src="'+logoLoader+'" class="loadingPageImg" height=80>'+
                        '<div style="border-color: transparent '+color2+' transparent '+color2+';"></div>'+
                        '<div style="border-color: transparent '+color1+' transparent '+color1+';"></div>'+
                        '<span class="selectedCity text-blue">'+costum.scopeActivated.name+'</span>'+
                    '</div>'+
            '</div>';
        urlCtrl.loadByHash(location.hash,true);
        myScopes.type="open";
        myScopes.open=costum.scopeSelector;
        $.each(myScopes.open, function(e, v){
            if(v.id == costum.scopeActivated.id)
                myScopes.open[e].active=true;
            else
                myScopes.open[e].active=false;
        });
        if(typeof costum.filters != "undefined"){
            costum.filters.scopes= {cities: costum.scopeActivated.id };
        }else{
            costum.filters={scopes : {cities: costum.scopeActivated.id}};            
        }
        costum.scopes=myScopes.open;
        localStorage.setItem("myScopes",JSON.stringify(myScopes));
    }else{
        $(".progressTop").hide();
        cssBlock=' initScope';
        //$(".blockUI.blockMsg.blockPage").addClass("initScope");
        themeObj.blockUi.processingMsg=
            '<div class="col-xs-12">'+
                //'<h3 class="text-blue">Bienvenu-e sur</h3>'+
                '<div class="col-xs-12 text-center"><img src="'+logoLoader+'" class="loadingPageImg" height=120></div>'+
                '<span class="col-xs-12 text-center padding-20 textExplain margin-bottom-20">Choisissez d\'abord votre commune</span>'+
                '<div class="selectMeuseScope col-xs-12 text-center">';
                $.each(costum.scopeSelector, function(e, v){
                    themeObj.blockUi.processingMsg+="<button class='btn bg-white init-selector-city col-xs-8 col-xs-offset-2 col-sm-4' "+
                        ' onclick="initMeuseScopeSelection(\''+e+'\')">'+
                            "<span class='col-xs-12 text-center color-btn-"+v.name+"'>"+v.name+"</span>"+
                            "<img src='"+assetPath+"/images/"+costum.slug+"/"+v.name+"/select-pic.jpg' class='img-responsive'/>"
                        "</button>";
                });
            themeObj.blockUi.processingMsg+='</div></div>';
    }
};
var cssBlock="";
themeObj.blockUi.setLoader();
$.blockUI({ message : themeObj.blockUi.processingMsg, blockMsgClass : "blockMsg"+cssBlock});
$(".blockUI.blockMsg.blockPage.initScope").css({'background-image': 'url("'+assetPath+'/images/'+costum.slug+'/la_meuse_bis.jpg")'});
costum.scopeHeaderFunction = function(){
    //appendScopeBreadcrum("<i class='fa fa-cirlce'></i>");
    $("#scope-container").empty();
    htmlMeuse='<button id="meuse-btn" class="btn btn-link letter-red btn-menu-scopes pull-left">'+
            '<i class="fa fa-sign-in"></i>'+ 
            ' Meuse@campagnes'+ 
        '</button><div class="scopes-container pull-left">';
    $.each(myScopes.open, function(key, value){
        mylog.log("constructScopesHtml each", key, value);
        if(value.active == false){
            disabled =  "";
            btnScopeAction="<i class='fa fa-circle-o'></i>";
        }
        else{
            disabled="active";
            btnScopeAction="<i class='fa fa-check-circle'></i>";
        }

        if(typeof value.name == "undefined") value.name = value.id;
        htmlMeuse += "<div class='scope-order "+disabled+" text-red' data-level='"+value.level+"''>"+
                    btnScopeAction+
                    "<span data-toggle='dropdown' data-target='dropdown-multi-scope' "+
                        "class='item-scope-checker item-scope-input' "+
                        'data-scope-key="'+key+'" '+
                        'data-scope-value="'+value.id+'" '+
                        'data-scope-name="'+name+'" '+
                        'data-scope-type="'+value.type+'" '+
                        'data-scope-level="'+value.type+'" ' +
                        'data-scope-country="'+value.country+'" ';
                        if(notNull(value.level))
                            htmlMeuse += 'data-level="'+value.level+'"';
                        htmlMeuse += '>' + 
                        value.name + 
                    "</span>"+
                "</div>";
    }); 
    htmlMeuse+="</div>";
    $("#scope-container").append(htmlMeuse);
    $(".item-scope-input").off().on("click", function(){ 
        scopeValue=$(this).data("scope-value");
        typeSearch=$(this).data("btn-type");
        key=$(this).data("scope-key");
            
        if($(this).parent().hasClass("active")){
            mylog.log(".item-scope-input");
            myScopes.open[key].active=false;
            //scopeActiveScope(key);
          //  $(".scope-order").removeClass("active").find("i").removeClass("fa-check-circle").addClass("fa-circle-o");
            $(this).parent().removeClass("active").find("i").removeClass("fa-check-circle").addClass("fa-circle-o");
        }else{
            $(this).parent().addClass("active").find("i").removeClass("fa-circle-o").addClass("fa-check-circle");
            myScopes.open[key].active=true;
            //if(myScopes.type!="open")
        }
        localStorage.setItem("myScopes",JSON.stringify(myScopes));
        searchObject.count=true;
        //    appendHeaderFilterActive();
            if(location.hash.indexOf("#live") >= 0){
                startNewsSearch(true)
            }else{
                if(typeof searchObject.ranges != "undefined") searchAllEngine.initSearch();
                startSearch(0, indexStepInit); 
            }
        });
    //appendScopeBreadcrum("<i class='fa fa-cirlce-o'></i>");
    $("#meuse-btn").click(function(){
        myScopes.type="open";
        myScopes.open=costum.scopeSelector;
        $.each(myScopes.open, function(e, v){
                myScopes.open[e].active=true;
        });
        $(".scope-order").addClass("active").find("i").removeClass("fa-circle-o").addClass("fa-check-circle");
        localStorage.setItem("myScopes",JSON.stringify(myScopes));
         if(location.hash.indexOf("#live") >= 0){
            startNewsSearch(true)
        }else{
            if(typeof searchObject.ranges != "undefined") searchAllEngine.initSearch();
            startSearch(0, indexStepInit); 
        }
    });
}
function initMeuseScopeSelection(key){
    selectedScope=costum.scopeSelector[key];
    $(".selectMeuseScope").html("<i class='fa fa-spin fa-spinner text-white' style='font-size:80px;margin-top:30px;'></i>"+
        "<br/><span class='textExplain margin-top-40'>Nous vous souhaitons la bienvenue sur "+selectedScope.name+" </span>");
    if(userId!=""){
        var params={
            id:userId,
            type:"citoyens",
            settings:"costum",
            value:[key],
            subName: costum.slug+".cities"
        };
        $.ajax({
              type: "POST",
              url: baseUrl+"/co2/element/updatesettings",
              data: params,
              success: function(data){
                if(data.result) {
                    location.reload();
                }
            }
            
        });
    }else{
        if(typeof localStorageCostum != "undefined"){
           if(typeof localStorageCostum[costum.slug] != "undefined"){ 
                if(typeof localStorageCostum[costum.slug].cities != "undefined")
                localStorageCostum[costum.slug].cities=[ key ];
            }else{
                localStorageCostum[costum.slug]={"cities" : [key]};
            }
        }else{
            localStorageCostum=new Object;
            localStorageCostum[costum.slug]={"cities" : [ key ]};
        } 
        //costumScopeActivated=localStorageCostum[costum.slug].cities[0];
        localStorage.setItem("costum",JSON.stringify(localStorageCostum));
        location.reload();
    }
}