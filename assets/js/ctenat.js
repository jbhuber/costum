  alert("bleu");
$('#menuApp').append('<a href="javascript:;" class="mapAppBtn lbh-menu-app btn btn-link pull-left btn-menu-to-app btn-menu-vertical col-xs-12 hidden-xs hidden-top link-submenu-header "><i class="fa fa-map"></i><span class="mapModSpan tooltips-menu-btn">La carte des territoires</span></a>');
var mapData = null;
$('#menuApp .mapAppBtn').parent().off().click(function() { 
    
  
  $(".search-loader").html("<i class='fa fa-spin fa-circle-o-notch'></i> "+trad.currentlyresearching+" ...");

  var searchParams = {
        "searchType" : [ "projects" ],
        "searchBy" : "ALL"  };
        
    var countData = 0;
    var oneElement = null;
    

    $.ajax({
      type: "POST",
          url: baseUrl+"/" + moduleId + "/search/globalautocomplete",
          data: searchParams,
          dataType: "json",
          error: function (data){
            mylog.log("error");
            mylog.dir(data);
            $(".search-loader").html("<i class='fa fa-ban'></i> "+trad.noresult);
          },
          success: function(data){
            mylog.log("success, try to load sig");
            mylog.dir(data);
            if(!data){
              toastr.error(data.content);
            }else{


            $.each(data, function(i, v) {
              if(v.length!=0){
                $.each(v, function(k, o){ countData++; });
              }
          });

          if(countData == 0){
            $(".search-loader").html("<i class='fa fa-ban'></i> "+trad.noresult);
          }else{
            $(".search-loader").html("<i class='fa fa-crosshairs'></i> Sélectionnez une commune ...");
            showMap(true);
            Sig.showMapElements(Sig.map, data);
            $(".logo-menutop")[0].remove(); 
            $("#input-search-map").parent().remove();
            $("#menu-map-btn-start-search").remove();

            mapData = data;
          }

          }
          
      }
    });
   })
