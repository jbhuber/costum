var CO3_ArticleBadge = class extends CO3_Article{
  constructor(pRequeteAjax,pPHDB_Obj){
    super(pRequeteAjax,pPHDB_Obj);
  } 

  ElementRenderHtml(params){
    var str = "<div class='badge_article'>";
    if(this.IsDefined(params.shortDescription)){
      str+="<div class='badge_article_titre'>"+params.name+"</div>";
    }
    if(this.IsDefined(params.shortDescription)){
      str+="<div class='badge_article_description'>"+params.shortDescription+"</div>";
    }
    str+="</div>";
    return str;
  }

  static getName(){
    return "CO3_ArticleBadge";
  }
}